---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: KDE lanza las Aplicaciones 15.04.1.
layout: application
title: KDE lanza las Aplicaciones de KDE 15.04.1
version: 15.04.1
---
Hoy, 12 de mayo de 2015, KDE ha lanzado la primera actualización de estabilización para las <a href='../15.04.0'>Aplicaciones 15.04</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 50 correcciones de errores registradas, se incluyen mejoras en kdelibs, kdepim, kdenlive, okular, marble y umbrello.

También se incluyen versiones de los Espacios de trabajo Plasma 4.11.19, de la Plataforma de desarrollo de KDE 4.14.8 y de la suite Kontact 4.14.8 que contarán con asistencia a largo plazo.
