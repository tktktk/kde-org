---
aliases:
- ../../kde-frameworks-5.13.0
date: 2015-08-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nuevas infraestructuras

- KFileMetadata: biblioteca de metadatos de archivo y extracción de los mismos
- Baloo: framework de indexación y búsqueda de archivos

### Cambios que afectan a todas las frameworks

- Se ha cambiado la versión mínima necesaria de Qt de 5.2 a 5.3
- Se ha adaptado la salida de depuración para que use categorías, para producir menos volumen de datos por omisión
- Se ha revisado y actualizado la documentación en formato docbook

### Integración con Frameworks

- Se ha corregido un cuelgue en el diálogo de archivos para directorios
- No confiar en options()-&gt;initialDirectory() para Qt &lt; 5.4

### Herramientas KDE Doxygen

- Añadir páginas de manual para guiones kapidox y actualizar la información sobre su encargado en setup.py

### KBookmarks

- KBookmarkManager: usar KDirWatch en lugar de QFileSystemWatcher para detectar la creación de user-places.xbel.

### KCompletion

- Correcciones de HiDPI para KLineEdit/KComboBox
- KLineEdit: no permitir que el usuario pueda borrar texto si está activado el modo de solo lectura

### KConfig

- No recomendar el uso de API obsoletas
- No generar código obsoleto

### KCoreAddons

- Añadir Kdelibs4Migration::kdeHome() para los casos no cubiertos por los recursos
- Corregir advertencia de tr()
- Corregir KCoreAddons compilado en Clang+ARM

### KDBusAddons

- KDBusService: documentar cómo activar la ventana activa en Activate()

### KDeclarative

- Corregir la llamada obsoleta KRun::run
- Mismo comportamiento del área del ratón para representar coordenadas de los eventos hijo filtrados
- Detección del aspecto inicial del icono que se está creando
- No refrescar la pantalla completa cuando se representa la impresora de gráficos (error 348385)
- añadir la propiedad de contexto «userPaths»
- Evitar el bloqueo ante un QIconItem vacío

### Soporte de KDELibs 4

- kconfig_compiler_kf5 se ha pasado a libexec; ahora se usa kreadconfig5 instead para la prueba findExe
- Documentar las sustituciones (no optimizadas) de KApplication::disableSessionManagement

### KDocTools

- Cambiar la frase sobre informar de fallos; aceptado por dfaure
- adaptar el «user.entities» del alemán a «en/user.entities»
- Actualizar general.entities: cambiar el marcado para frameworks y Plasma de aplicación a nombre de producto
- Actualización de en/user.entities
- Actualizar las plantillas de los libros y de las páginas de manual
- Usar CMAKE_MODULE_PATH en cmake_install.cmake
- Error: 350799 (error 350799)
- Actualización de general.entities
- Buscar los módulos perl necesarios.
- Espacio de nombres para macro asistente en el archivo de macros instalado.
- Traducciones de nombres clave adaptadas a las traducciones normalizadas proporcionadas por Termcat

### KEmoticons

- Instalación del tema Brisa
- Kemoticons: hacer que los emoticonos de Brisa sean los estándar en lugar de los de Cristal
- Paquete de emoticonos de Brisa hecho por Uri Herrera

### KHTML

- Hacer que KHtml se pueda usar sin buscar dependencias privadas

### KIconThemes

- Eliminar las asignaciones de memoria temporales.
- Eliminar la entrada de depuración de árbol de temas

### KIdleTime

- Cabeceras privadas para que se instalen complementos de plataforma.

### KIO

- Detener las envolventes QUrl que ya no son necesarias

### KItemModels

- Nuevo modelo de proxy: KExtraColumnsProxyModel. Admite la reordenación y el ocultado de columnas a partir del modelo de origen.

### KNotification

- Corregir la posición Y para menús emergentes alternativos
- Reducir las dependencias y moverlas al Nivel 2
- Capturar las entradas de notificaciones desconocidas (nullptr deref) (error 348414)
- Eliminar muchos mensajes de advertencia poco útiles

### Framework para paquetes

- hacer que los subtítulos sean subtítulos ;)
- kpackagetool: corregir la salida de texto no latino en la salida estándar

### KPeople

- Añadir AllPhoneNumbersProperty
- PersonsSortFilterProxyModel se puede usar ahora en QML

### Kross

- krosscore: instalar la cabecera «KrossConfig» de CamelCase
- Corregir las pruebas de Python2 para que funcionen con PyQt5

### KService

- Corregir kbuildsycoca --global
- KToolInvocation::invokeMailer: corregir los adjuntos cuando existe más de uno

### KTextEditor

- proteger el nivel de registro predeterminado para Qt &lt; 5.4.0; corregir el nombre del catálogo de registro
- añadir hl para Xonotic (fallo 342265)
- añadir Groovy HL (fallo 329320)
- actualizar el resaltado de J (error 346386)
- Compilar con MSVC2015.
- menos uso de iconloader, corregir más iconos pixelados
- activar/desactivar el botón de "Buscar todos" cuando se cambia el patrón
- Barra de búsqueda y sustitución mejorada
- Eliminar la regla que no tiene utilidad del modo de alimentación.
- Barra de búsqueda más fina.
- vi: corregir la lectura incorrecta del indicador markType01
- Usar la certificación correcta para invocar al método base.
- Eliminar comprobaciones; QMetaObject::invokeMethod ya se protege a sí mismo ante ello.
- corregir los problemas de HiDPI en los selectores de colores
- Limpieza de coe: QMetaObject::invokeMethod está a prueba de nullptr.
- más comentarios
- cambiar el modo en que las interfaces se protegen de null
- De manera predeterminada, solo las advertencias de salida y las que estén por encima.
- eliminar «por hacer» antiguos
- Utilizar QVarLengthArray para guardar la iteración de QVector de manera temporal.
- Mover el fragemento para sangrar las etiquetas al momento de la construcción.
- Corregir varios problemas serios del KateCompletionModel en modo árbol.
- Corregir el diseño de modelo dañado que confiaba en el comportamiento de Qt 4.
- obedecer las reglas «umask» cuando al guardar nuevos archivos (error 343158)
- añadir meson HL
- Como Varnish 4.x implica varios cambios de sintaxis en comparación con Varnish 3.x, he escrito varios archivos más de resaltado de sintaxis para Varnish 4 (varnish4.xml, varnishtest4.xml).
- corregir problemas de HiDPI
- vimode: no falla si el comando &lt;c-e&gt; se ejecuta al final de un documento (error 350299).
- Permitir el uso de cadenas QML multilínea.
- corregir la sintaxis de oors.xml
- añadir CartoCSS hl por Lukas Sommer (error 340756)
- Corregir el punto flotante HL, utilizar el Float integrado como en C (error 348843)
- dividir direcciones que estaban invertidas (error 348845)
- Error 348317: [PARCHE] El resaltado de sintaxis de Katepartdebería reconocer los caracteres de escape de estilo u0123 style para JavaScript (error 348317).
- añadir *.cljs (fallo 349844)
- Actualizar el archivo de resaltado de GLSL.
- se han corregido los colores por omisión para que se puedan distinguir mejor

### KTextWidgets

- Borrado del antiguo resaltador

### Framework KWallet

- Corregir compilación para Windows
- Mostrar una advertencia con código de error cuando PAM falla al abrir la cartera
- Devolver el código de error del motor en lugar de -1 cuando falla la apertura de una cartera
- Hacer que el error de «cifrado desconocido» del motor sea un código negativo
- Vigilar PAM_KWALLET5_LOGIN para KWallet5
- Corregir el error cuando falla MigrationAgent::isEmptyOldWallet()
- PAM puede desbloquear ahora KWallet usando el módulo «kwallet-pam»

### KWidgetsAddons

- Nueva API que usa parámetros de QIcon para definir los iconos en las barras de pestañas
- KCharSelect: corregir la categoría unicode y utilizar boundingRect para el cálculo de la anchura
- KCharSelect: corregir la anchura de la celda para que quepa el contenido
- Los márgenes de KMultiTabBar ahora son correctos en las pantallas HiDPI
- KRuler: dejar de usar KRuler::setFrameStyle() que no se ha implementado, limpiar los comentarios
- KEditListWidget: eliminar el margen para que se alinee mejor con otros elementos gráficos.

### KWindowSystem

- Reforzar la lectura de datos de NETWM (error 350173)
- Proteger las versiones antiguas de Qt como en kio-http
- Se han instalado cabeceras privadas para los complementos de plataforma.
- Partes específicas de la plataforma cargadas como complementos.

### KXMLGUI

- Corregir el comportamiento del método KShortcutsEditorPrivate::importConfiguration

### Framework de Plasma

- Ahora, con un gesto de pellizco se puede cambiar entre los diferentes niveles de escala del calendario.
- Comentario sobre la duplicación de código en icondialog
- El color del canal del deslizador estaba especificado en el código. Esto se ha modificado para que use la combinación de colores actual.
- Utilizar QBENCHMARK en lugar de un requisito estricto sobre el rendimiento del equipo
- La navegación del calendario se ha mejorado significativamente. Ahora se ofrece una vista de año y de década.
- PlasmaCore.Dialog ahora tiene una propiedad «Opacidad»
- Se ha hecho espacio para el selector de opción
- No mostrar el fondo circular si hay un menú
- Añadir la definición de X-Plasma-NotificationAreaCategory
- Configurar las notificaciones y la visualización en pantalla para que se muestren en todos los directorios
- Mostrar una advertencia útil cuando no se puede obtener un KPluginInfo válido
- Corregir la potencial recursión infinita en PlatformStatus::findLookAndFeelPackage()
- Cambiar el nombre de software-updates.svgz a software.svgz

### Sonnet

- Añadir a CMake los bits para activar la compilación del complemento Voikko.
- Implementar Sonnet::Client para la comprobación de ortografía de Voikko.
- Implementar la comprobación de ortografía basada en Voikko (Sonnet::SpellerPlugin)

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
