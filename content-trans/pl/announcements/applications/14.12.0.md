---
aliases:
- ../announce-applications-14.12.0
changelog: true
date: '2014-12-17'
description: KDE wydało Aplikacje 14.12.
layout: application
title: KDE wydało Aplikacje KDE 14.12
version: 14.12.0
---
17 Grudnia 2014. Dzisiaj KDE wydało Aplikacje KDE 14.12. Wydanie to wprowadza nowe możliwości i naprawia błędy w ponad stu aplikacjach. Większość z tych aplikacji jest oparta na Platformie Programistycznej KDE 4; niektóre zostały przystosowane do nowych <a href='https://dot.kde.org/2013/09/25/frameworks-5'>Szkieletów KDE 5</a>, zestawu modułowych bibliotek, które są oparte na Qt5, najnowszej wersji tego wieloplatformowego szkieletu aplikacji.

<a href='http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html'>Libkface</a> jest nowością w tym wydaniu; jest to  biblioteka umożliwiająca wykrywanie i rozpoznawanie twarzy na  zdjęciach.

To wydanie zawiera po raz pierwszy wydane Szkielety KDE oparte o wersję 5 <a href='http://www.kate-editor.org'>Kate</a> oraz <a href='https://www.kde.org/applications/utilities/kwrite/'>KWrite</a>, <a href='https://www.kde.org/applications/system/konsole/s'>Konsole</a>, <a href='https://www.kde.org/applications/graphics/gwenview/s'>Gwenview</a>, <a href='http://edu.kde.org/kalgebras'>KAlgebra</a>, <a href='http://edu.kde.org/kanagram'>Kanagram</a>, <a href='http://edu.kde.org/khangman'>KHangman</a>, <a href='http://edu.kde.org/kig'>Kig</a>, <a href='http://edu.kde.org/parley'>Parley</a>, <a href='https://www.kde.org/applications/development/kapptemplate/'>KApptemplate</a> jak również <a href='https://www.kde.org/applications/utilities/okteta/'>Okteta</a>. Niektóre biblioteki są gotowe na wersję 5 Szkieletów KDE, np. analitza i  libkeduvocdocument.

<a href='http://kontact.kde.org'>Pakiet Kontact</a> zyskał wsparcie długoterminowe w wersji 4.14, a jednocześnie programiści będą pracować nad przeniesieniem go do Szkieletów KDE 5

Nowości w tym wydaniu:

+ <a href='http://edu.kde.org/kalgebra'>KAlgebra</a> ma nową wersję na Androida, a dzięki Szkieletom KDE 5 może teraz <a href='http://www.proli.net/2014/09/18/touching-mathematics/'>drukować swoje wykresy w 3D</a>
+ <a href='http://edu.kde.org/kgeography'>KGeography</a> zawiera nową mapę dla Bihar.
+ Przeglądarka dokumentów <a href='http://okular.kde.org'>Okular</a> zyskała obsługę wyszukiwania odwrotnego latex-synctex w plikach dvi i kilka małych ulepszeń w obsłudze ePub.
+ <a href='http://umbrello.kde.org'>Umbrello</a> --modeller UML -- ma kilka nowych możliwości, zbyt licznych, aby wymienić je tutaj.

Kwietniowe wydanie Aplikacji KDE 15.04 będzie odznaczać się nowymi możliwościami, a także większą liczbą aplikacji opartych na zmodularyzowanych Szkieletach KDE 5.
