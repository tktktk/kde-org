---
aliases:
- ../../kde-frameworks-5.14.0
date: 2015-09-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### W wielu szkieletach

- Rename private classes to avoid exporting them accidentally

### Baloo

- Dodano interfejs org.kde.baloo to głównego obiektu dla wstecznej zgodności
- Zainstaluj fikcyjny org.kde.baloo.file.indexer.xml aby naprawić kompilację plasma-desktop 5.4
- Re-organize D-Bus interfaces
- Użyj metadanych json we wtyczce kded oraz popraw nazwę wtyczki
- Create one Database instance per process (bug 350247)
- Prevent baloo_file_extractor being killed while committing
- Generate xml interface file using qt5_generate_dbus_interface
- Naprawiono monitor Baloo
- Move file url export to main thread
- Make sure cascaded configs are taken into account
- Do not install namelink for private library
- Install translations, spotted by Hrvoje Senjan.

### BluezQt

- Nie przekazuj sygnału deviceChanged dalej po usunięciu urządzenia (błąd 351051)
- Respect -DBUILD_TESTING=OFF

### Dodatkowe moduły CMake

- Dodano macro do tworzenia dzienników deklaracji kategorii dla Qt5.
- ecm_generate_headers: Dodano opcję COMMON_HEADER oraz opcję wielu nagłówków
- Dodano -pedantic dla kodu KF5 (przy użyciu gcc lub clang)
- KDEFrameworkCompilerSettings: only enable strict iterators in debug mode
- Also set the default visibility for C code to hidden.

### Integracja Szkieletów

- Also propagate window titles for folder-only file dialogs.

### KAktywności

- Only spawn one action loader (thread) when the actions of the FileItemLinkingPlugin are not initialized (bug 351585)
- Usunięto problem budowania wprowadzony przez zmienienie nazwy prywatnych klas (11030ffc0)
- Dodano brakującą ścieżkę plików dołączany służących do budowania na OS X
- Setting the shortcuts moved to activity settings
- Setting the private activity mode works
- Refactor of the settings UI
- Basic activity methods are functional
- UI for the activity configuration and deletion pop-ups
- Basic UI for the activities creation/deletion/configuration section in KCM
- Increased the chunk size for loading the results
- Dodano brakujący plik dołączany std::set

### Narzędzia KDE Doxygen

- poprawka do Windowsa: usuwaj istniejące pliki przed zastąpieniem ich przy użyciu os.rename.
- Używaj natywnych ścieżek przy wywoływaniu pythona, aby naprawić budowanie na Windowsie

### KCompletion

- Naprawiono złe zachowanie / uruchamianie OOM na Windowsie (błąd 345860)

### KConfig

- Optimize readEntryGui
- Avoid QString::fromLatin1() in generated code
- Minimize calls to expensive QStandardPaths::locateAll()
- Ukończono przeniesienie na QCommandLineParser (od teraz ma ono addPositionalArgument)

### Obsługa KDELibs 4

- Port solid-networkstatus kded plugin to json metadata
- KPixmapCache: create dir if it doesn't exist

### KDocTools

- Sync Catalan user.entities with English (en) version.
- Dodano obiekty dla sebas oraz plasma-pa

### KEmotikony

- Performance: cache a KEmoticons instance here, not a KEmoticonsTheme.

### KFileMetaData

- PlainTextExtractor: enable O_NOATIME branch on GNU libc platforms
- PlainTextExtractor: make the Linux branch work also without O_NOATIME
- PlainTextExtractor: naprawiono sprawdzane błędów przy porażce open(O_NOATIME)

### KGlobalAccel

- Only start kglobalaccel5 if needed.

### KI18n

- Gracefully handle no newline at end of pmap file

### KIconThemes

- KIconLoader: naprawiono reconfigure() które zapominało odziedziczone wystroje i katalogi aplikacji
- Adhere better to the icon loading spec

### KImageFormats

- eps: naprawiono pliki dołączane związane z rejestrowaniem kategoriami Qt

### KIO

- Use Q_OS_WIN instead of Q_OS_WINDOWS
- Make KDE_FORK_SLAVES work under Windows
- Disable installation of desktop file for ProxyScout kded module
- Provide deterministic sort order for KDirSortFilterProxyModelPrivate::compare
- Show custom folder icons again (bug 350612)
- Move kpasswdserver from kded to kiod
- Usunięto błędy przenoszenia w kpasswdserver
- Usunięto przestrzały kod, który wykazywał zachowanie bardzo, bardzo starych wersji kpasswdserver.
- KDirListerTest: używaj QTRY_COMPARE w obu zdaniach, aby naprawić warunek race pokazywany przez CI
- KFilePlacesModel: wykonano stare zadanie polegające na użyciu trashrc zamiast w pełnofunkcjonalnego KDirLister.

### KItemModels

- New proxymodel: KConcatenateRowsProxyModel
- KConcatenateRowsProxyModelPrivate: naprawiono obsługę layoutChanged.
- More checking on the selection after sorting.
- KExtraColumnsProxyModel: naprawiono błąd w sibling() co wywoływało problemy np. z zaznaczaniem

### Pakiety Szkieletów

- kpackagetool can uninstall a package from a package file
- kpackagetool is now smarter about finding the right servicetype

### KService

- KSycoca: sprawdź znaczniki czasu i uruchom kbuildsycoca, gdy potrzeba. Zależność kded jest już niepotrzebna.
- Nie zamykaj ksycoca zaraz po jego otwarciu.
- KPluginInfo now correctly handles FormFactor metadata

### KTextEditor

- Merge allocation of TextLineData and ref count block.
- Change default keyboard shortcut for "go to previous editing line"
- Naprawiono podświetlanie składni w komentarzach Haskella
- Speed up code-completion pop-up appearance
- minimap: Attempt to improve the look and feel (bug 309553)
- nested comments in Haskell syntax highlighting
- Naprawiono problem ze złym usuwaniem wcięć dla pythona (błąd 351190)

### KWidgetsAddons

- KPasswordDialog: let the user change the password visibility (bug 224686)

### KXMLGUI

- Naprawiono KSwitchLanguageDialog, który nie pokazywał większości języków

### KXmlRpcClient

- Unikaj QLatin1String przy przydzielaniu pamięci stosu

### ZarządzanieModememQt

- Naprawiono sprzeczność metatype z najnowszą zmianą nm-qt

### ZarządzanieSieciąQt

- Dodano nowe właściwości z najświeższych wydań NM

### Szkielety Plazmy

- przemianowanie do flickable, jeśli możliwe
- Naprawiono wyszczególnienie zawartości pakietu
- plasma: Naprawiono występowanie działań apletów jako nullptr (błąd 351777)
- Sygnał onClicked funkcji PlasmaComponents.ModelContextMenu od teraz działa poprawnie
- PlasmaComponents ModelContextMenu od teraz tworzy sekcje menu
- Przeniesiono wtyczkę platformstatus kded na metadane json...
- Obsługa nieprawidłowych metadanych w PluginLoader
- Pozwól RowLayout wykryć rozmiar etykiety
- zawsze pokazuj menu edycji, gdy kursor jest widoczny
- Usunięto pętlę w ButtonStyle
- Nie zmieniaj płaskości przycisku po jego naciśnięciu
- na ekranach dotykowych paski przewijania są przejściowe
- dostosowano szybkość przewijania do dpi
- własny kursor tylko na urządzeniach mobilnych
- kursor tekstowy przyjazny dotykowi
- naprawiono zasady okien podrzędnych i wyskakujących
- zadeklarowano __editMenu
- dodano brakujących delegatów uchwytów kursora
- przepisano implementację EditMenu
- używaj menu urządzeń mobilnych tylko warunkowo
- przemianuj menu dla roota

Możesz przedyskutować i podzielić się pomysłami dotyczącymi tego wydania w dziale komentarzy <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artykułu dot</a>.
