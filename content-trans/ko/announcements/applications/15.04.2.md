---
aliases:
- ../announce-applications-15.04.2
changelog: true
date: 2015-06-02
description: KDE에서 KDE 프로그램 15.04.2 출시
layout: application
title: KDE에서 KDE 프로그램 15.04.2 출시
version: 15.04.2
---
June 2, 2015. Today KDE released the second stability update for <a href='../15.04.0'>KDE Applications 15.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Gwenview, Kate, Kdenlive, kdepim, Konsole, Marble, KGpg, Kig, ktp-call-ui 및 Umbrello 등에 30개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.20, KDE Development Platform 4.14.9 and the Kontact Suite 4.14.9.
