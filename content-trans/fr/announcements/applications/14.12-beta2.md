---
aliases:
- ../announce-applications-14.12-beta2
custom_spread_install: true
date: '2014-11-13'
description: KDE publie la version 14.12 « bêta » 2 des applications.
layout: application
title: KDE publie la deuxième version « Beta » pour les les applications en version
  14.12.
---
13 Novembre 2014. Aujourd'hui, KDE publie la version « bêta » des applications de KDE. Les dépendances et les fonctionnalités sont stabilisées et l'équipe KDE se concentre à présent sur la correction des bogues et sur les dernières finitions.

Avec de nombreuses applications reposant sur l'environnement de développement version 5, les mises à jour des applications 14.12 ont besoin de tests intensifs pour maintenir et améliorer la qualité et l'interface utilisateur. Les utilisateurs actuels sont très importants pour maintenir la grande qualité de KDE. En effet, les développeurs ne peuvent tester toutes les configurations possibles. Votre aide est nécessaire pour aider à trouver les bogues suffisamment tôt pour qu'ils puissent être corrigés avant la version finale. Veuillez contribuer à l'équipe en installant la version « bêta » et <a href='https://bugs.kde.org/'>en signalant tout bogue</a>.

#### Installation des paquets binaires des applications KDE 14.12 Beta 2

<em>Paquets</em>Quelques fournisseurs de systèmes d'exploitation Linux / Unix fournissent gracieusement pour certaines versions de leurs distributions, des paquets binaires pour les applications 14.12 « bêta » 2 (en interne 14.11.90). Dans d'autres cas, des bénévoles de la communauté le font aussi. Des paquets binaires supplémentaires, et également des mises à jour des paquets actuels, seront éventuellement mis à disposition dans les prochaines semaines.

<em>Emplacements des paquets</em>. Pour obtenir une liste à jour des paquets binaires disponibles, connus par l'équipe de publication KDE, veuillez visiter le <a href='http://community.kde.org/KDE_SC/Binary_Packages'> Wiki de la communauté</a>.

#### Compilation des applications KDE 14.12 Beta 2

Le code source complet des applications 14.12 « Bêta 2 » de KDE peut être <a href='http://download.kde.org/unstable/applications/14.11.90/src/'>librement téléchargé</a>. Toutes les instructions de compilation et d'installation sont disponibles sur la <a href='/info/applications/applications-14.11.90'>page d'informations des applications KDE « Bêta 2 »</a>.
