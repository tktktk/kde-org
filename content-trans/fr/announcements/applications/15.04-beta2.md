---
aliases:
- ../announce-applications-15.04-beta2
date: '2015-03-12'
description: KDE publie la version 15.04 « bêta » 2 des applications de KDE.
layout: applications
title: KDE publie la deuxième version « Beta » pour les les applications de KDE en
  version 15.04.
---
12 mars 2015. Aujourd'hui, KDE publie la seconde version « bêta » des applications KDE. Les dépendances et les fonctionnalités sont stabilisées et l'équipe KDE se concentre à présent sur la correction des bogues et sur les dernières finitions.

Avec toutes les applications reposant sur KDE Frameworks 5, les publications de KDE Applications 15.04 nécessitent des tests poussés pour maintenir et améliorer la qualité ainsi que l'expérience utilisateur. Les utilisateurs ont un grand rôle à jouer dans la qualité de KDE car les développeurs ne peuvent tout simplement pas tester toutes les configurations possibles. Nous comptons sur vous pour nous aider à détecter les bogues suffisamment tôt pour pouvoir les corriger avant la livraison finale. N'hésitez pas à rejoindre l'équipe en installant la beta <a href='https://bugs.kde.org/'>et en signalant les bogues</a>.
