---
aliases:
- ../../kde-frameworks-5.59.0
date: 2019-06-08
layout: framework
libCount: 70
---
### Baloo

- Försök inte indexera SQL-databasdumpar
- Undanta .gcode och virtuell maskin-filer från indexeringsöverväganden

### BluezQt

- Lägg till Bluez programmeringsgränssnitt till DBus XML-tolk/generator

### Breeze-ikoner

- gcompris-qt också
- Gör falkon ikon till en riktig SVG
- lägg till saknade ikoner från programmen, för att göras om https://bugs.kde.org/show_bug.cgi?id=407527
- lägg till ikon för kfourinline från program, behöver också uppdateras
- lägg till kigo ikon https://bugs.kde.org/show_bug.cgi?id=407527
- lägg till kwave-ikon från kwave, för att göras om av breeze-stil
- Skapa symboliska länkar arrow-_-double till go-_-skip, lägg till 24 bildpunkters go-*-skip
- Ändra input-* enheternas ikonstilar, lägg till 16 bildpunkters ikoner
- Lägg till mörk version för ny ikon av Knights som kom undan från min tidigare incheckning
- Skapa ny ikon för Knights baserad på Anjutas ikon (fel 407527)
- lägg till ikoner för program som saknar dem i breeze, de ska uppdateras för att vara mer breeze-liknande, men de behövs för den nya kde.org/applications för tillfället
- kxstitch ikon från kde:kxstitch, att uppdatera
- använd inte glob för allt under solen
- säkerställ att också ScaledDirectories sätts på

### Extra CMake-moduler

- Skapa specifik katalog för Qt-loggningskategorier fil
- Aktivera inte QT_STRICT_ITERATORS på Windows

### Integrering med ramverk

- säkerställ att också söka på föråldrad plats
- sök på den nya platsen efter knsrc-filer

### KArchive

- Testa läsning och sökning i KCompressionDevice
- KCompressionDevice: Ta bort bIgnoreData
- KAr: rätta läsning utanför gränser (vid felaktig inmatning) genom att konvertera till QByteArray
- KAr: rätta tolkning av långa filnamn med Qt-5.10
- KAr: rättigheterna är oktala, inte decimala
- KAr::openArchive: Kontrollera också att ar_longnamesIndex inte är &lt; 0
- KAr::openArchive: Rätta felaktig minnesåtkomst för felaktiga filer
- KAr::openArchive: Skydda mot Heap-buffer-overflow i felaktiga filer
- KTar::KTarPrivate::readLonglink: Rätta krasch i felformade filer

### KAuth

- Hårdkoda installationskatalog för dbus-policy

### KConfigWidgets

- Använd lokal valuta för bidragsikon

### KCoreAddons

- Rätta kompilering av python-bindningar (fel 407306)
- Lägg till GetProcessList för att hämta listan över processer som för närvarande är aktiva

### KDeclarative

- Rätta qmldir-filer

### Stöd för KDELibs 4

- Ta bort QApplication::setColorSpec (tom metod)

### KFileMetaData

- Visa tre signifikanta siffror när dubbla flyttal visas (fel 343273)

### KIO

- Hantera byte istället för tecken
- Rätta att I/O-slav körbara filer aldrig avslutas, när KDE_FORK_SLAVES anges
- Rätta skrivbordslänk till fil eller katalog (fel 357171)
- Testa aktuellt filter innan ett nytt tilldelas (fel 407642)
- [kioslave/file] Lägg till en kodare för föråldrade filnamn (fel 165044)
- Lita på QSysInfo för att hämta systeminformation
- Lägg till dokument i den förvalda listan med platser
- kioslave: bevara argv[0], för att rätta applicationDirPath() på annat än Linux
- Tillåt att släppa en fil eller en katalog på KDirOperator (fel 45154)
- Avkorta långa filnamn innan en länk skapas (fel 342247)

### Kirigami

- [ActionTextField] Gör QML-verktygstips konsekventa
- basera på höjd för objekt som ska ha en övre vaddering (fel 405614)
- Prestanda: komprimera färgändringar utan en QTimer
- [FormLayout] Använd jämn övre och undre mellanrum för avskiljare (fel 405614)
- ScrollablePage: Säkerställ att den rullade vyn får fokus när den ställs in (fel 389510)
- Förbättra användning av verktygsraden med bara tangentbord (fel 403711)
- gör återvinnaren en FocusScope

### KNotification

- Hantera program som ställer in egenskapen desktopFileName med filnamnssuffix

### KService

- Rätta assert (hash != 0) ibland när en fil tas bort av en annan process
- Rätta en annan assert när filen försvinner under oss: ASSERT: "ctime != 0"

### KTextEditor

- Ta inte bort hela föregående rad genom att göra baksteg på pos 0 (fel 408016)
- Använd inbyggd dialogrutas överskrivningskontroll
- Lägg till åtgärd för att återställa teckenstorlek
- visa alltid statisk ordbrytningsmarkör på begäran
- Säkerställ färglagda start/slutmarkörer för intervall after utvikning
- Rättning: återställ inte färgläggning när vissa filer sparas (fel 407763)
- Automatisk indentering: Använd std::vector istället för QList
- Rättning: Använd förvalt indenteringsläge för nya filer (fel 375502)
- förbättra duplicerad tilldelning
- ta hänsyn till inställningen auto-bracket för balanskontroll
- Förbättra ogiltig teckenkontroll vid inläsning (fel 406571)
- Ny meny för syntaxfärgläggning i statusraden
- Undvik oändlig snurra i åtgärden "Toggle Contained Nodes"

### Kwayland

- Tillåt sammansättningar att skicka diskreta axelvärden (fel 404152)
- Implementera set_window_geometry
- Implementera wl_surface::damage_buffer

### KWidgetsAddons

- KNewPasswordDialog: lägg till punkter i meddelandekomponenter

### NetworkManagerQt

- Hämta inte enhetsstatistik vid konstruktion

### Plasma ramverk

- Gör så att Breeze ljus/mörk använder fler systemfärger
- Exportera SortFilterModel sorteringskolumn till QML
- plasmacore: rätta qmldir, ToolTip.qml är inte längre en del av modul
- signalera availableScreenRectChanged för alla miniprogram
- Använd helt enkelt configure_file för att generera filen plasmacomponents3
- Uppdatera *.qmltypes till aktuellt programmeringsgränssnitt för QML-moduler
- FrameSvg: rensa också maskcache vid clearCache()
- FrameSvg: gör så att hasElementPrefix() också hanterar prefix med efterföljande -
- FrameSvgPrivate::generateBackground: generera också bakgrund om reqp != p
- FrameSvgItem: skicka också maskChanged från geometryChanged()
- FrameSvg: förhindra krasch när mask() anropas när ingen ram ännu har skapats
- FrameSvgItem: skicka alltid maskChanged från doUpdate()
- Dokumentation av programmeringsgränssnitt: notera efterföljande '-' för FrameSvg::prefix()/actualPrefix()
- Dokumentation av programmeringsgränssnitt: peka på Plasma5 versioner på teknikbasen om tillgängliga
- FrameSvg: v &amp; h kanter eller t &amp; b behöver inte ha samma höjd respektive bredd

### Syfte

- [JobDialog] Signalera också avbryt när fönstret stängs av användaren
- Rapportera att avbryta en inställning som är avslutat med ett fel (fel 407356)

### QQC2StyleBridge

- Ta bort animering av DefaultListItemBackground och MenuItem
- [QQC2 Slider Style] Rätta felaktig grepposition när initialvärdet är 1 (fel 405471)
- ScrollBar: Få den att också fungera som en horisontell rullningslist (fel 390351)

### Solid

- Omstrukturera sättet som enhetsgränssnitt byggs och registreras
- [Fstab] Använd ikonen folder-decrypted för att krypterande fuse-monteringar

### Syntaxfärgläggning

- YAML: bara kommentarer efter mellanslag och andra förbättringar/rättningar (fel 407060)
- Markdown: använd includeAttrib i kodblock
- Rätta färgläggning av "0" i C-läge
- Tcsh: rätta operatorer och nyckelord
- Lägg till syntaxdefinition för Common Intermediate Language
- SyntaxHighlighter: Rätta förgrundsfärg för text utan speciell färgläggning (fel 406816)
- Lägg till exempelprogram för att skriva ut färglagd text i PDF
- Markdown: Använd färger med större kontrast för listor (fel 405824)
- Ta bort filändelsen .conf från "INI-filer" hl, för att bestämma färgläggning genom att använda Mime-typ (fel 400290)
- Perl: rätta operatorn // (fel 407327)
- rätta skiftläge för UInt* typer i Julia hl (fel 407611)

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
