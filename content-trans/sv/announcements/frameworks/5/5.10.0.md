---
aliases:
- ../../kde-frameworks-5.10.0
date: '2015-05-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KActivities

- (ingen ändringslogg tillhandahållen)

### KConfig

- Skapa QML-säkra klasser med användning av kconfigcompiler

### KCoreAddons

- Nytt cmake-makro kcoreaddons_add_plugin för att enklare skapa insticksmoduler baserade på KPluginLoader.

### KDeclarative

- Rätta krasch i strukturcache.
- och andra rättningar

### KGlobalAccel

- Lägg till ny metod globalShortcut, som hämtar genvägen som den är definierad i de globala inställningarna.

### KIdleTime

- Förhindra att kidletime kraschar på wayland-plattform

### KIO

- Tillägg av KPropertiesDialog::KPropertiesDialog(urls) och KPropertiesDialog::showDialog(urls).
- Asynkron datahämtning baserad på QIODevice för KIO::storedPut och KIO::AccessManager::put.
- Rätta villkor med returvärde från QFile::rename (fel 343329)
- Rättade KIO::suggestName för att föreslå bättre namn (fel 341773)
- kioexec: Rättade sökväg till skrivbar plats för kurl (fel 343329)
- Lagra bara bokmärken user-places.xbel (fel 345174)
- Duplicera RecentDocuments-posten om två olika filer har samma namn
- Bättre felmeddelande om en enda fil är för stor för papperskorgen (fel 332692)
- Rätta krasch i KDirLister vid omdirigering när openURL anropas

### KNewStuff

- Ny klassuppsättning, benämnda KMoreTools och relaterade. KMoreTools hjälper till att lägga till tips om externa verktyg som potentiellt ännu inte är installerade. Dessutom gör det långa menyer kortare genom att tillhandahålla en huvuddel och fler sektioner, som också kan ställas in av användaren.

### KNotifications

- Rätta KNotifications när det används med Ubuntus NotifyOSD (fel 345973)
- Utlös inte uppdateringar av underrättelser när samma egenskaper tilldelas (fel 345973)
- Introduktion av flaggan LoopSound som tillåter underrättelser att spela upp repeterat ljud om det behövs (fel 346148)
- Krascha inte om underrättelse inte har en grafisk komponent

### KPackage

- Tillägg av funktionen KPackage::findPackages som liknar KPluginLoader::findPlugins

### KPeople

- Använd KPluginFactory för instansiering av insticksprogram, istället för KService (behålls för kompatibilitet).

### KService

- Rätta felaktig delning av sökvägspost (fel 344614)

### KWallet

- Konverteringsmodulen kontrollerar nu också om den gamla plånboken är tom innan den börjar (fel 346498)

### KWidgetsAddons

- KDateTimeEdit: Rättning så att användarinmatning faktiskt registreras. Rättning av dubbla marginaler.
- KFontRequester: Rättning för att bara välja teckensnitt med fast breddsteg

### KWindowSystem

- Bero inte på QX11Info i KXUtils::createPixmapFromHandle (fel 346496)
- ny metod NETWinInfo::xcbConnection() -&gt; xcb_connection_t*

### KXmlGui

- Rättning av genvägar när sekundär genväg inställd (fel 345411)
- Uppdatera lista över produkter eller komponenter i Bugzilla för felrapportering (fel 346559)
- Globala genvägar: Tillåt också inställning av alternativ genväg

### NetworkManagerQt

- De installerade deklarationsfilerna är nu organiserade som i alla andra ramverk.

### Plasma ramverk

- PlasmaComponents.Menu stöder nu sektioner
- Använd KPluginLoader istället för ksycoca för att ladda C++ datagränssnitt
- Ta hänsyn till rotation av visualParent i popupPosition (fel 345787)

### Sonnet

- Försök inte markera om ingen stavningskontroll hittas. Det skulle leda till en oändlig snurra med konstant utlösning av timer rehighlighRequest.

### Frameworkintegration

- Rättning av inbyggda fildialogrutor från grafisk komponent QFileDialog: ** Fildialogrutor öppnade med exec() utan överliggande komponent öppnades, men all användarinteraktion var blockerad på ett sätt så att inga filer kunde väljas, inte heller kunde dialogrutan stängas. ** Fildialogrutor öppnade med open() eller show() med överliggande komponent öppnades inte alls.

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
