---
aliases:
- ../announce-applications-18.12.0
changelog: true
date: 2018-12-13
description: KDE Ships Applications 18.12.
layout: application
release: applications-18.12.0
title: KDE Ships KDE Applications 18.12.0
version: 18.12.0
---
{{% i18n_date %}}

KDE Applications 18.12 are now released.

{{%youtube id="ALNRQiQnjpo"%}}

Me jätkame visalt tööd meie KDE rakendustesse kaasatud tarkvara parandamisel ja täiustamisel ning loodame, et kõik uued omadused ja veaparandused tulevad kasutajatele kasuks!

## What's new in KDE Applications 18.12

Üle 140 vea parandati sellistes rakendustes, nagu Kontacti komplekt, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsool, Lokalize, Okular, Spectacle, Umbrello jt!

### Failihaldus

{{<figure src="/announcements/applications/18.12.0/app1812_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager:

- Uus MTP teostus, mis muuda selle loometöös täiesti kasutuskõlblikuks
- Tohutu jõudluse kasv failide lugemisel SFTP protokolli vahendusel
- Pisipiltide eelvaatlusel näidatakse raame ja varju ainult sellistel pildifailidel, mis ei ole läbipaistvad, mis parandab ikoonide esitust
- Uus pisipildina eelvaatlus LibreOffice' dokumentidele ja AppImage rakendustele
- Suuremaid kui 5 MB videofaile näidatakse nüüd kataloogi pisipildina, kui kataloogide pisipiltide näitamine on lubatud
- Audio-CD-de lugemisel suudab Dolphin nüüd muuta MP3 dekoodri konstantset bitikiirust (CBR) ja parandada FLAC-i korral ajatemplit
- Dolphini juhtimismenüü näitab nüüd "Loo uus ..." kirjeid ja sisaldab uut "Näita peidetud asukohti" kirjet
- Dolphin sulgub nüüd, kui avatud on ainult üks kaart ka vajutatakse standardset "Sulge kaart" kiirklahvi (Ctrl+w)
- Ketta lahutamise järel Asukohtade paneelil on nüüd võimalik see uuesti haakida
- Viimati kasutatud dokumentide vaade (mida saab näha Dolphinis asukohta recentdocuments:/ avades) näitab nüüd ainult tegelikke dokumente ja filtreerib automaatselt välja veebi-URL-id
- Dolphin näitab nüüd hoiatust, enne kui laseb faili või kataloogi nime muuta moel, mis muudaks selle otsekohe peidetuks
- Enam ei ole Asukohtade paneelil lahutada kettaid sinu aktiivse operatsioonisüsteemiga või kodukataloogiga

<a href='https://www.kde.org/applications/utilities/kfind'>KFind</a>, KDE's traditional file search, now has a metadata search method based on KFileMetaData.

### Kontoritöö

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client:

- KMail võib nüüd näidata ühendatud sissetulevate kirjade kausta
- Uus plugin HTML-kirja loomiseks Markdowni keele põhjal
- Purpose kasutamine teksti jagamiseks (e-kirjana)
- HTML-kirjad on nüüd loetavad sõltumata kasutatavast värviskeemist

{{<figure src="/announcements/applications/18.12.0/app1812_okular01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, KDE's versatile document viewer:

- Uus "masinakirjas" annoteerimise tööriist teksti kirjutamiseks, kuhu vaja
- Hierarhilist sisukorda saab nüüd kokku ja lahti kerida nii tervikuna kui ka konkreetsete osade kaupa
- Reasiseste annotatsioonide reamurdmise käitumise täiustamine
- Hiirekursoriga lingi kohal viibides näidatakse nüüd URL-i alati, kui seda saab klõpsata. mitte ainult sirvimisrežiimis
- ePub-faile ressurssidega, mille URL sisaldab tühikuid, näidatakse nüüd korrektselt

### Arendus

{{<figure src="/announcements/applications/18.12.0/app1812_kate01.png" width="600px" >}}

<a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, KDE's advanced text editor:

- Põimitud terminali kasutamisel sünkroonitakse nüüd aktiivne kataloog automaatselt aktiivse dokumendi asukohaga kettal
- Kate põimitud terminali saab nüüd fookusse tuua ja fookusest välja viia kiirklahviga F4
- Kate kaardivahetaja näitab nüüd sarnaste nimedega failide täielikku asukohta
- Reanumbreid näidatakse nüüd vaikimisi
- Uskumatult võimas ja kasulik tekstifiltri plugin on nüüd vaikimisi sisse lülitatud ja paremini üles leitav
- Juba avatud dokumendi avamine kiiravamise võimalusega lülitab nüüd dokumendile tagasi
- Kiiravamise võimalus ei näita enam topeltkirjeid
- Mitme tegevuse kasutamise korral avatakse failid nüüd õiges tegevuses
- Kate näitab nüüd kõiki õigeid ikoone rakenduse kasutamisel GNOMEs GNOME ikooniteema abil

{{<figure src="/announcements/applications/18.12.0/app1812_konsole.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator:

- Konsool toetab nüüd täies mahus emoji sümboleid
- Mitteaktiivsete kaartide ikoonid tõstetakse nüüd esile, kui need saavad helisignaali
- Lõpus olevaid kooloneid ei peeta enam sõna osaks topeltklõpsuga valimisel, mis muudab hõlpsamaks valida asukohti ja "greppida" väljundit
- Kui ühendatakse edasi-tagasi nuppudega hiir, võib Konsool nüüd tarvitada neid nuppe kaartide vahel liikumiseks
- Konsoolil on nüüd menüükirje fondisuuruse lähestamiseks profiili vaikeväärtusele, kui seda on suurendatud või vähendatud
- Kaarte on nüüd raskem juhuslikult lahti ühendada, see-eest aga on nende järjekorra muutmine kiirem
- Tõstuklahvi ja klõpsuga valimise käitumise täiustamine
- Parandati ära topetklõpsamine tekstireal, mis ületab akna laiust
- Otsinguriba saab taas sulgeda klahvile Escape vajutamisega

<a href='https://www.kde.org/applications/development/lokalize/'>Lokalize</a>, KDE's translation tool:

- Tõlgitud failide peitmine projektikaardil
- Süntaksi ja sõnastiku kontrollimise süsteemi pology esialgne toetus
- Liikumise lihtsustamine kaarte järjekorda seades ja korraga mitut kaarti avades
- Segmendivigade parandamine üheaegse juurdepääsi korral andmebaasi objektidele
- Lohistamise ebakõlade parandamine
- Redaktorite sama ülesandega erinevate kiirklahvide tõrke parandamine
- Otsingukäitumise täiustamine (otsing leiab nüüd ja näitab mitmusevorme)
- Ühilduvuse taastamine Windowsiga tänu Crafti ehitussüsteemile

### Tööriistad

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer:

- Punasilmsuse vähendamise tööriist sai mitmeid kasutamist hõlbustavaid täiustusi
- Gwenview näitab nüüd menüüriba peitmisel hoiatusdialoogi, mis ütleb, kuidas menüüriba tagasi saada

{{<figure src="/announcements/applications/18.12.0/app1812_spectacle01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's screenshot utility:

- Spectacle võib nüüd anda ekraanipildifailidele järjestikused numbrid ning kasutabki seda nimeandmisskeemi, kui puhastada failinime tekstiväli.
- Parandati piltide salvestane muusse kui .png-vormingusse käsu Salvesta Kui... tarvitamisel
- Kui kasutada Spectacle'i ekraanipildi avamiseks välises rakenduses, saab nüüd pilti muuta ja salvestada ka pärast seda
- Spectacle avab nüüd õige kataloogi, kui valida Tööriistad > Ekraanipiltide kataloogi avamine
- Ekraanipiltide ajatemplid kajastavad nüüd pildi loomise, mitte salvestamise aega
- Kõik salvestamisvalikud leiab nüüd salvestamise leheküljelt

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, KDE's archive manager:

- Lisati Zstandardi vormingu (tar.zst arhiivid) toetus
- Parandati Arki kalduvus näidata teatavate failide (nt Open Document vormingus) eelvaatlust arhiivina, mitte avada neid sobivad rakenduses

### Matemaatika

<a href='https://www.kde.org/applications/utilities/kcalc/'>KCalc</a>, KDE's simple calculator, now has a setting to repeat the last calculation multiple times.

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, KDE's mathematical frontend:

- Lisati Markdowni kirjetüüp
- Parajasti arvutatava käsukirje animeeritud esiletõstmine
- Ootel käsukirjete (järjekorras, aga veel arvutamata) visualiseerimine
- Käsukirjete vormindamise lubamine (taustavärv, esiplaani värv, fondi omadused)
- Uute käsukirjete lisamise lubamine töölehe suvalisse kohta lihtsalt kursorit vajalikku kohta asetades ja kirjutama hakates
- Mitut käsku sisaldavate avaldiste korral tulemuste näitamine töölehel iseseisvate tulemusobjektidena
- Lisati töölehtede avamise toetus konsoolist suhtelise asukoha järgi
- Lisati mitme faili avamise toetus ühes Cantori kestas
- Värvi ja fondi muutmine lisateabe pärimisel paremaks eristamiseks tavalisest käsukirje sisendist
- Lisati kiirklahvid töölehtede vahel liikumiseks (Ctrl+PageUp, Ctrl+Pagedown)
- Vaate alammenüüsse lisati kirje suurenduse lähtestamiseks
- Cantori projektide allalaadimise lubamine saidilt store.kde.org (üles laadida saab praegu ainult veebilehelt)
- Töölehe avamine kirjutuskaitstuna, kui süsteemis puudub vajalik taustaprogramm

<a href='https://www.kde.org/applications/education/kmplot/'>KmPlot</a>, KDE's function plotter, fixed many issues:

- Tuletiste ja integraalide diagrammide valede nimede parandamine tavapärases tähistuses
- SVG eksport töötab KmPlotis nüüd korralikult
- Esimesel tuletisel ei ole priimimärki
- Fookuseta funktsiooni märkimise eemaldamine peidab nüüd ka selle diagrammi:
- Kõrvaldati Kmploti krahh, kui "Muuda konstante" avati funktsiooniredaktoris rekursiivselt
- Kõrvaldati Kmploti krahh pärast funktsiooni kustutamist, mida hiirekursor diagrammis järgib
- Kujutatud andmed saab nüüd eksportida mis tahes pildivormingusse
