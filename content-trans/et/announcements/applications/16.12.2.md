---
aliases:
- ../announce-applications-16.12.2
changelog: true
date: 2017-02-09
description: KDE toob välja KDE rakendused 16.12.2
layout: application
title: KDE toob välja KDE rakendused 16.12.2
version: 16.12.2
---
February 9, 2017. Today KDE released the second stability update for <a href='../16.12.0'>KDE Applications 16.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 20 teadaoleva veaparanduse hulka kuuluvad Kdepimi, Dolphini, Kate, Kdenlive'i, Ktouchi, Okulari ja teiste rakenduste täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.29.
