---
aliases:
- ../announce-applications-17.04.0
changelog: true
date: 2017-04-20
description: KDE toob välja KDE rakendused 17.04.0
layout: application
title: KDE toob välja KDE rakendused 17.04.0
version: 17.04.0
---
20. aprill 2017. KDE rakendused 17.04 on kohal. Üldiselt püüdsime muuta nii rakendused kui ka teegid stabiilsemaks ja kasutuskindlamaks. Ebakõlasid eemaldades ja kõigi kasutajate tagasisidet kuulates oleme loodetavasti suutnud teha KDE rakendused viimitletumaks ja mugavamaks.

Nautige uusi rakendusi!

#### <a href="https://edu.kde.org/kalgebra/">KAlgebra</a>

{{<figure src="/announcements/applications/17.04.0/kalgebra1704.jpg" width="600px" >}}

KAlgebra arendajad on valinud eripärase tee teistega samale järjele jõuda, portides kogu õpirakenduse mobiilse versiooni Kirigami 2.0 peale, mis ongi eelistatud raamistik KDE rakenduste lõimimiseks töölaua- ja mobiiliplatvormil.

Lisaks on töölauaversioon üle viidud GLES-i 3D taustaprogrammi peale, mis lubab rakendusel renderdada 3D funktsioone nii töölaual kui ka mobiilseadmetes. Ühtlasi on sel moel koodi lihtsam ja kergem hooldada.

#### <a href="http://kdenlive.org/">Kdenlive</a>

{{<figure src="/announcements/applications/17.04.0/kdenlive1704.png" width="600px" >}}

KDE videoredaktor muutub iga väljalaskega aina stabiilsemaks ja omadusterohkemaks. Seekord on arendajad ümber kujundanud profiili valimise dialoogi, mis võimaldab hõlpsamalt paika panna oma filmi ekraanisuurust, kaadrisagedust ja teisi parameetreid.

Samuti saab nüüd video esitada otse märguandest, kui renderdamine on lõpetatud. Mõned krahhid, mis juhtusid klippide liigutamisel ajateljel, on ära parandatud ning DVD nõustajat täiustatud.

#### <a href="https://userbase.kde.org/Dolphin">Dolphin</a>

{{<figure src="/announcements/applications/17.04.0/dolphin1704.png" width="600px" >}}

Meie lemmikust failiuudistaja ja portaal kuhu iganes (võib-olla siiski mitte allilma) on saanud mitmel pool lihvi ja kasutamist hõlbustavaid täiendusi, nii et see on nüüd varasemast veel võimsam.

<i>Asukohtade</i> paneeli (see paikneb vaikimisi põhivaatealast vasakus servas) kontekstimenüüsid on korralikult puhastatud ning nüüd on võimalik interaktiivselt tarvitada metaandmete vidinaid kohtspikrites. Kohtspikrid ise, muide, töötavad nüüd ka Waylandi peal.

#### <a href="https://www.kde.org/applications/utilities/ark/">Ark</a>

{{<figure src="/announcements/applications/17.04.0/ark1704.png" width="600px" >}}

Populaarne graafiline rakendus failide ja kataloogide tihendatud arhiivide loomiseks, lahtipakkimiseks ja haldamiseks pakub nüüd <i>otsinguvõimalust</i>, mis lubab faile kiiresti üles leida ka suurtes arhiivides.

Samuti saab otse <i>seadistamisdialoogis</i> pluginaid sisse ja välja lülitada. Kui pluginatest rääkida, siis uus Libzipi plugin parandab zip-arhiivide toetust.

#### <a href="https://minuet.kde.org/">Minuet</a>

{{<figure src="/announcements/applications/17.04.0/minuet1704.png" width="600px" >}}

Kui õpetad või õpid muusikat ja selle esitamist, tasuks silm peal hoida Minuetil. Uus versioon pakub rohkem heliridade ja kuulamisharjutusi bebopi, harmoonilise minoori ja mažoori, pentatoonilise ja sümmeetriliste heliridade kohta.

You can also set or take tests using the new <i>Test mode</i> for answering exercises. You can monitor your progress by running a sequence of 10 exercises and you'll get hit ratio statistics when finished.

### Ja veel palju rohkem!

<a href='https://okular.kde.org/'>Okular</a>, KDE's document viewer, has had at least half a dozen changes that add features and crank up its usability on touchscreens. <a href='https://userbase.kde.org/Akonadi'>Akonadi</a> and several other apps that make up <a href='https://www.kde.org/applications/office/kontact/'>Kontact</a> (KDE's email/calendar/groupware suite) have been revised, debugged and optimized to help you become more productive.

<a href='https://www.kde.org/applications/games/kajongg'>Kajongg</a>, <a href='https://www.kde.org/applications/development/kcachegrind/'>KCachegrind</a> and more (<a href='https://community.kde.org/Applications/17.04_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Release Notes</a>) have now been ported to KDE Frameworks 5. We look forward to your feedback and insight into the newest features introduced with this release.

<a href='https://userbase.kde.org/K3b'>K3b</a> has joined the KDE Applications release.

### Vigadest vabaks

Rakendustes on lahenduse leidnud üle 95 vea, sealhulgas Kopetes, KDE turvalaekas, Marble'is, Spectacle'is ja teistes!

### Täielik muudatuste logi
