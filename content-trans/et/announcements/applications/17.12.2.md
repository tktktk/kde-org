---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDE toob välja KDE rakendused 17.12.2
layout: application
title: KDE toob välja KDE rakendused 17.12.2
version: 17.12.2
---
February 8, 2018. Today KDE released the second stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Umbes 20 teadaoleva veaparanduse hulka kuuluvad Kontacti, Dolphini, Gwenview, KGeti, Okulari ja teiste rakenduste täiustused.
