---
aliases:
- ../../kde-frameworks-5.19.0
date: 2016-02-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- attica plugina välimuse ja initsialiseerimise lihtsustamine

### Breeze'i ikoonid

- Palju uusi ikoone
- Puuduvate MIME tüüpide ikoonide lisamine Oxygeni ikoonikomplektist

### CMake'i lisamoodulid

- ECMAddAppIcon: ikoonide käitlemisel absoluutse asukoha kasutamine
- Tagamine, et Androidis järgitakse prefiksit
- FindPoppler'i mooduli lisamine
- PATH_SUFFIXES kasutamine ecm_find_package_handle_library_components()-s

### KActivities

- QML-ile ei esitata exec() väljakutset (veateade 357435)
- KActivitiesStats'i teek asub nüüd eraldi hoidlas

### KAuth

- preAuthAction'i lubamine ka AuthorizeFromHelperCapability'ga taustaprogrammides
- Polkiti agendi DBus-teenuse nime parandus

### KCMUtils

- HiDPI probleemi parandus KCMUtils'is

### KCompletion

- KLineEdit::meetodit setUrlDropsEnabled ei saa märkida iganenuks

### KConfigWidgets

- "Täiendava" värviskeemi lisamine kcolorscheme'ile

### KCrash

- KCrash::initialize dokumentatsiooni uuendamine. Rakenduste arendajatel soovitatakse seda otse välja kutsuda.

### KDeclarative

- KDeclarative/QuickAddons'i sõltuvuste puhastamine
- [KWindowSystemProxy] setter'i lisamine showingDesktop'ile
- DropArea: dragEnter'i sündmuse korrektse eiramise parandus preventStealing'i abil
- DragArea: delegaadi elemendi hõivamise lubamine
- DragDropEvent: funktsiooni ignore() lisamine

### KDED

- BlockingQueuedConnection'i hädalahenduse tühistamine, Qt 5.6 pakub paremat lahendust
- kded sundimine registreerima kded moodulite määratud aliastega

### KDELibs 4 toetus

- kdelibs4support nõuab kded'd (kdedmodule.desktop'i jaoks)

### KFileMetaData

- Failide algse URL-i päringu lubamine

### KGlobalAccel

- Krahhi vältimine, kui dbus pole saadaval

### KDE GUI Addons

- Saadaolevate palettide loendi parandus värvidialoogis

### KHTML

- Ikoonilingi tüübi tuvastamise parandus

### KI18n

- gettext'i API kasutuse vähendamine

### KImageFormats

- kra ja ora imageio (kirjutuskaitstud) pluginate lisamine

### KInit

- Ignore viewport on current desktop on init startup information
- klauncher'i portimine xcb peale
- xcb kasutamine koostöös KStartupInfo'ga

### KIO

- Uus klass FavIconRequestJob uues teegis KIOGui lemmikikoonide tõmbamiseks
- KDirListerCache'i krahhi vältimine kahe loendaja korral puhvri tühjas kataloogis (veateade 278431)
- KIO::stat'i Windowsi variant teatab nüüd protokolli file:/ veast, kui faili ei ole olemas
- Ei eeldata, et kirjutuskaitstud kataloogis paiknevaid faile ei saa Windowsis kustutada
- KIOWidets'i .pri-faili parandus: see sõltub KIOCore'st, mitte iseendast
- kcookiejar'i automaatse laadimise parandamine: väärtused läksid sissekandega 6db255388532a4 vahetusse
- kcookiejar'i muutmine juurdepääsetavaks dbus'i teenuse nime org.kde.kcookiejar5 all
- kssld: iDBus'i tenusefaili org.kde.kssld5 paigaldamine
- DBus'i steenusefaili org.kde.kpasswdserver lisamine
- [kio_ftp] faili/kataloogi muutmise kuupäeva/kellaaja esitamise parandus (veateade 354597)
- [kio_help] rämpsu saatmise parandus staatiliste failide teenindamisel
- [kio_http] NTLMv2 autentimise proovimine, kui servel lükkab NTLMv1 tagasi
- [kio_http] puhverdamise tuksi keeranud portimisvigade parandus
- [kio_http] NTLMv2 staadium 3 vastuse loomise parandus
- [kio_http] ootamise, kuni puhvripuhastaja jälgib pesa, parandus
- kio_http_cache_cleaner: käivitamisel ei lõpetata tööd, kui puhvrikataloogi veel ei ole
- kio_http_cache_cleaner'i DBus'i nime muutmine, et see ei lõpetaks tööd, kui töötab ka kde4 versioon

### KItemModels

- KRecursiveFilterProxyModel::match: krahhi likvideerimine

### KJobWidgets

- Krahhi kõrvaldamine KJob'i dialoogides (veateade 346215)

### Paketiraamistik

- Sama paketi eri asukohtadest mitmekordse leidmise vältimine

### KParts

- PartManager: vidina jälgimise lõpetamine, isegi kui see pole enam tipptasemel (veateade 355711)

### KTextEditor

- Automaatsulgude võimaluse "ümbritse sulgudega" parem käitumine
- Valikuvõtme muutmine uue vaikeväärtuse jõustamiseks, reavahetus faili lõpus = true
- Mõne kahtlasevõitu setUpdatesEnabled väljakutse eemaldamine (veateade 353088)
- verticalScrollPositionChanged'i väljastamisega viivitamine, kuni kõik on voltimisega kooskõlas (veateade 342512)
- Sildi asendamist uuendav paik (veateade 336634)
- Paleti ainult ühekordne uuendamine, kui muudetakse qApp'ile kuuluvat sündmust (veateade 358526)
- Reavahetuse vaikimisi lisamine faili lõppu
- NSIS süntaksi esiletõstmise faili lisamine

### KWalleti raamistik

- Faili deskriptori kloonimine faili avamisel keskkonna lugemiseks

### KWidgetsAddons

- Sõltlasvidinate töö parandus KFontRequester'iga
- KNewPasswordDialog: KMessageWidget'i kasutamine
- Krahhi vältimine tõõ lõpetamisel KSelectAction::~KSelectAction'is

### KWindowSystem

- Litsentsipäis "Library GPL 2 or later" asendati päisega "Lesser GPL 2.1 or later"
- Krahhi vältimine, kui KWindowSystem::mapViewport kutsutakse välja ilma QCoreApplication'ita
- QX11Info::appRootWindow puhverdamine eventFilter'ist (veateade 356479)
- QApplication'i sõltuvusest vabanemine (veateade 354811)

### KXMLGUI

- Valiku lisamine KGlobalAccel'i keelamiseks kompileerimise ajal
- Rakenduse kiirklahviskeemi asukoha parandamine
- Kiirklahvifailide loendi parandus (QDir'i väär kasutus)

### NetworkManagerQt

- Ühenduse oleku ja teiste omaduste ülekontrollimine kindlustamaks, et need on tõelised (versioon 2) (veateade 352326)

### Oxygeni ikoonid

- Vigaste lingitud failide eemaldamine
- Rakenduseikoonide lisamine kde rakendustele
- Breeze'i asukohaikoonide lisamine oxygen'i
- Oxygen'i MIME tüüpide ikoonide sünkroonimine Breeze'i MIME tüüpide ikoonidega

### Plasma raamistik

- Omaduse separatorVisible lisamine
- Veel otsesem eemaldamine m_appletInterfaces'ist (veateade 358551)
- KColorScheme'i complementaryColorScheme'i kasutamine
- AppletQuickItem: algsuurust ei määrata suuremaks eellase suurusest (veateade 358200)
- IconItem: omaduse usesPlasmaTheme lisamine
- Tööriistakast laaditakse ainult töölaua või paneeli korral
- IconItem: QIcon::fromTheme ikoonid püütakse laadida svg-na (veateade 353358)
- Kontrolli eiramine, kui compactRepresentationCheck'is on kas või üks osa suurusest null (veateade 358039)
- [Ühikud] kestuse korral vähemalt 1ms tagastamine (veateade 357532)
- clearActions() lisamine kõigi apletiliideste toimingute eemaldamiseks
- [plasmaquick/dialog] KWindowEffects'i ei kasutada märguandeakna tüübis
- Applet::loadPlasmoid() märkimine iganenuks
- [PlasmaCore DataModel] allika eemaldamisel mudelit ei lähtestata
- Veerisevihjete parandus läbipaistmatu paneelitausta SVG-s
- IconItem: omaduse animated lisamine
- [Unity] töölauaikooni suuruse skaleerimine
- the button is compose-over-borders
- paintedWidth/paintedheight IconItem'i tarvis

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
