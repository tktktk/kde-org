---
aliases:
- ../../kde-frameworks-5.44.0
date: 2018-03-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl: Retirar a opción checkDb (fallo 380465)
- indexerconfig: Describir algunhas funcións
- indexerconfig: expoñer a función canBeSearched (fallo 388656)
- balooctl monitor: Agardar pola interface de D-Bus
- fileindexerconfig: Introducir canBeSearched() (fallo 388656)

### Iconas de Breeze

- retirar view-media-playlist das iconas da configuración
- engadir unha icona de media-album-cover de 24 px
- engadir compatibilidade con QML en Babe (22px)
- actualizar as iconas «handle-» en Kirigami
- engadir iconas de son e vídeo de 64 px para Elisa

### Módulos adicionais de CMake

- Define **ANDROID_API**
- Corrixir o nome da orde readelf en x86
- Cadea de ferramentas de Android: engadir a variábel ANDROID_COMPILER_PREFIX, corrixir a ruta de inclusión dos destinos de x86, estender a ruta de busca das dependencias de NDK

### Ferramentas de Doxygen de KDE

- Saír en caso de erro se o directorio de saída non está baleiro (fallo 390904)

### KConfig

- Aforrar algunhas asignacións de memoria usando a API correcta
- Exportar kconf_update con ferramentas

### KConfigWidgets

- Mellorar KLanguageButton::insertLanguage cando non se pasa un nome
- Engadir iconas para «Cancelar a selección» e «Substituír» en KStandardActions

### KCoreAddons

- Limpar m_inotify_wd_to_entry antes de invalidar os punteiros de Entry (fallo 390214)
- kcoreaddons_add_plugin: retirar un OBJECT_DEPENDS sen efecto dun ficheiro JSON
- Axudar a automoc a atopar ficheiros JSON de metadatos aos que se fai referencia no código
- kcoreaddons_desktop_to_json: deixar unha nota sobre o ficheiro xerado no rexistro de construción
- Aumentar shared-mime-info a 1.3
- Introducir K_PLUGIN_CLASS_WITH_JSON

### KDeclarative

- Corrixir o fallo da construción en armhf e aarch64
- Matar QmlObjectIncubationController
- desconectar render() no cambio de xanela (fallo 343576)

### KHolidays

- Allen Winter é oficialmente o novo mantedor de KHolidays

### KI18n

- Documentación de API: engadir unha nota sobre chamar a setApplicationDomain tras a creación de QApp

### KIconThemes

- [KIconLoader] Ter en conta devicePixelRatio para as capas

### KIO

- Non asumir a disposición de msghdr nin a estrutura de iovec (fallo 391367)
- Corrixir a selección de protocolo en KUrlNavigator
- Cambiar qSort por std::sort
- [KUrlNavigatorPlacesSelector] Usar KFilePlacesModel::convertedUrl
- [Traballo de soltar] Crear un ficheiro de lixo axeitado ao ligar
- Corrixir unha activación accidental de elemento de menú de ronsel (fallo 380287)
- [KFileWidget] Ocultar o marco e a cabeceira dos lugares
- [KUrlNavigatorPlacesSelector] Poñer as categorías en menús subordinados (fallo 389635)
- Usar a cabeceira de asistencia de probas estándar de KIO
- Engadir Ctrl+H á lista de atallos de «mostrar ou agochar ficheiros agochados» (fallo 390527)
- Engadir compatibilidade con semánticas de movemento en KIO::UDSEntry
- Corrixir o problema de «atallo ambiguo» introducido en D10314
- Meter a caixa de mensaxe «Non se puido atopar o executábel» nunha lambda encolada (fallo 385942)
- Mellorar a facilidade de uso do diálogo de «Abrir con» engadindo a opción para filtrar a árbore de aplicacións
- [KNewFileMenu] KDirNotify::emitFilesAdded tras storedPut (fallo 388887)
- Corrixir unha aserción ao cancelar o diálogo rebuild-ksycoca (fallo 389595)
- Corrixir o fallo #382437 «Unha regresión en KDialog causa unha extensión de ficheiro incorrecta» (fallo 382437)
- Inicio máis rápido de simplejob
- Reparar a copia de ficheiros a VFAT sen avisos
- kio_file: saltar a xestión de erros para permisos iniciais durante a copia de ficheiros
- Permitir que a semántica de mover se xere desde KFileItem. Agora o construtor, o destrutor e o operador de asignación de copia existentes tamén os xera o compilador
- Non chamar stat(/etc/localtime) entre read() e write() ao copiar ficheiros (fallo 384561)
- remoto: non crear entradas con nomes baleiros
- Engadir a funcionalidade supportedSchemes
- Usar F11 como atallo para conmutar a vista previa lateral
- [KFilePlacesModel] Agrupar as comparticións de rede na categoría «Remoto»

### Kirigami

- Mostrar o botón de ferramenta como marcado mentres se mostra o menú
- indicadores de desprazamento non interactivos en móbiles
- Corrixir o menú subordinado de accións
- Permitir usar QQC2.Action
- Permitir grupos de accións exclusivos (fallo 391144)
- Mostrar o texto canda os botóns de ferramenta de acción de páxina
- Permitir que as accións mostren menús subordinados
- Non ter unha posición de compoñente específica no seu pai
- Non causar as accións de SwipeListItem se non están expostas
- Engadir unha comprobación isNull() antes de definir se QIcon é unha máscara
- Engadir FormLayout.qml a kirigami.qrc
- corrixir as cores de swipelistitem
- mellor comportamento para cabeceiras e pés
- Mellorar o comportamento de completado pola esquerda e elidir de ToolBarApplicationHeader
- Asegurarse de que os botóns de navegación non van baixo a acción
- compatibilidade coas propiedades header e footer en overlaysheet
- Eliminar o recheo inferior innecesario en OverlaySheets (fallo 390032)
- Pulir a aparencia de ToolBarApplicationHeader
- mostrar un botón de pechar en escritorios (fallo 387815)
- non se pode pechar a folla coa roda do rato
- Só multiplicar o tamaño da icona se Qt non o fixo xa (fallo 390076)
- ter en conta o pé global para a posición das asas
- comprimir os eventos da creación e destrución de barras de desprazamento
- ScrollView: Publicar a política da barra de desprazamento e corrixila

### KNewStuff

- Engadir vokoscreen a KMoreTools e engadilo ao agrupamento «screenrecorder»

### KNotification

- Usar QWidget para ver se a xanela está dispoñíbel

### Infraestrutura KPackage

- Axudar a automoc a atopar ficheiros JSON de metadatos aos que se fai referencia no código

### KParts

- Limpar código vello inútil

### KRunner

- Actualizar o modelo de complemento de KRunner

### KTextEditor

- Engadir iconas de exportar documento, retirar marcador e formatar texto en maiúsculas, minúsculas e maiúsculas iniciais a KTextEditor

### KWayland

- Permitir liberar saída liberada polo cliente
- [servidor] Xestionar correctamente a situación na que o DataSource dun arrastre se destrúe (fallo 389221)
- [servidor] Non quebrar cando unha superficie subordinada se remite e a superficie superior se destruíu (fallo 389231)

### KXMLGUI

- Restabelecer os datos internos de QLocale cando temos un idioma de aplicación personalizado
- Non permitir configurar accións de separador mediante o menú de contexto
- Non mostrar o menú contextual ao facer clic dereito fóra (fallo 373653)
- Mellorar KSwitchLanguageDialogPrivate::fillApplicationLanguages

### Iconas de Oxygen

- engadir unha icona para Artikulate (fallo 317527)
- engadir a icona folder-games (fallo 318993)
- corrixir unha icona de 48 px incorrecta de calc.template (fallo 299504)
- engadir as iconas media-playlist-repeat e shuffle (fallo 339666)
- Oxygen: engadir iconas de etiqueta como en Breeze (fallo 332210)
- ligar emblem-mount a media-mount (fallo 373654)
- engadir iconas de rede que están dispoñíbeis en breeze-icons (fallo 374673)
- sincronizar as iconas de Oxygen coas de Breeze: engadir iconas para o plasmoide de son
- Engadir edit-select-none a Oxygen para Krusader (fallo 388691)
- Engadir a icona rating-unrated (fallo 339863)

### Infraestrutura de Plasma

- usar o novo valor para largeSpacing en Kirigami
- Reducir a visibilidade do texto de substitución do TextField de PC3
- Tampouco facer os títulos un 20% transparentes
- [PackageUrlInterceptor] Non reescribir «inline»
- Non facer as cabeceiras un 20% transparentes, para coincidir con Kirigami
- non poñer fullrep na xanela emerxente se non está colapsada
- Axudar a automoc a atopar ficheiros JSON de metadatos aos que se fai referencia no código
- [AppletQuickItem] Cargar previamente o expansor de miniaplicativo só se non se expandiu xa
- outras optimizacións microscópicas de carga anticipada
- Usar smooth=true de maneira predeterminada en IconItem
- cargar tamén o expansor (o diálogo) de maneira preventiva
- [AppletQuickItem] Corrixir a definición da política de carga previa predeterminada se non hai ningunha variábel de contorno definida
- corrixir a aparencia de RTL en ComboBox (fallo https://bugreports.qt.io/browse/QTBUG-66446)
- intentar cargar previamente certos miniaplicativos de maneira intelixente
- [Elemento de icona] Definir o filtrado na textura FadingNode
- Inicializar m_actualGroup como NormalColorGroup
- Asegurarse de que as instancias de FrameSvg e Svg teñen o devicePixelRatio correcto

### Prison

- Actualizar ligazóns a dependencias e marcar Android como oficialmente compatíbel
- Facer opcional a dependencia de DMTX
- Engadir compatibilidade con QML para Prison
- Definir o tamaño mínimo tamén en códigos de barra 1D

### Purpose

- Corrixir a capa, axustar a KIO

### QQC2StyleBridge

- Corrixir un erro de sintaxe da remisión anterior, detectado ao iniciar ruqola
- Mostrar un botón redondo ao mostrar un botón exclusivo (fallo 391144)
- crear MenuBarItem
- crear DelayButton
- Novo compoñente: botón arredondado
- ter en conta a posición da barra de ferramentas
- permitir cores para as iconas nos botóns
- permitir --reverse
- facer que as iconas de Menu funcionen completamente
- sombras consistentes co novo estilo Breeze
- Algúns QStyles non parece devolver pixelmetrics sensatas
- compatibilidade inicial incompleta con iconas
- non rodear coa roda do rato

### Solid

- corrixir unha fuga e unha comprobación de punteiro nulo incorrecta en DADictionary
- [UDisks] Corrixir unha regresión de montaxe automática (fallo 389479)
- [UDisksDeviceBackend] Evitar varias buscas
- Infraestrutura de Mac e IOKit: compatibilidade con unidades, discos e volumes

### Sonnet

- Usar Locale::name() en vez de Locale::bcp47Name()
- Atopar a construción de libhunspell en MSVC

### Realce da sintaxe

- Compatibilidade básica con bloques de código delimitados de PHP e Python en Markdown
- Permitir WordDetect que ignore maiúsculas
- Salientado de esquemas: retirar cores definidas a man
- Engadir salientado de sintaxe para políticas e contextos de ficheiro de CIL de SELinux
- Engadir a extensión de ficheiro ctp ao realce de sintaxe de PHP
- Yacc e Bison: Corrixir o símbolo $ e actualizar a sintaxe de Bison
- awk.xml: engadir palabras clave de extensión de gawk (fallo 389590)
- Engadir APKBUILD para salientar como un ficheiro de Bash
- Reverter «Engadir APKBUILD para salientar como un ficheiro de Bash»
- Engadir APKBUILD para salientar como un ficheiro de Bash

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
