---
aliases:
- ../../kde-frameworks-5.37.0
date: 2017-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nova infraestrutura: Kirigami, un conxunto de complementos de QtQuick para construír interfaces de usuario baseadas nas directrices de experiencia de usuario de KDE

### Iconas de Breeze

- actualizar as cores de .h e .h++ (fallo 376680)
- retirar unha icona monocroma pequena de KTorrent (fallo 381370)
- bookmarks é unha icona de acción, non de cartafol (fallo 381383)
- actualizar utilities-system-monitor (fallo 381420)

### Módulos adicionais de CMake

- Engadir --gradle a androiddeployqt
- Corrixir o destino de instalación de APK
- Corrixir o uso de query_qmake: distinguir entre as chamadas que esperan qmake e as que non
- Engadir documentación de API para KDE_INSTALL_USE_QT_SYS_PATHS de KDEInstallDirs
- Engadir un metainfo.yaml para converter ECM nunha infraestrutura axeitada
- Android: buscar ficheiros QML no directorio de orixe, non no directorio de instalación

### KActivities

- emitir runningActivityListChanged ao crear unha actividade

### Ferramentas de Doxygen de KDE

- Escapar o HTML da consulta de busca

### KArchive

- Engadir ficheiros de Conan, como un primeiro experimento de compatibilidade con Conan

### KConfig

- Permitir construír KConfig sen Qt5Gui
- Atallos de teclado estándar: usar Ctrl+RePáx/AvPáx para o separador anterior e posterior

### KCoreAddons

- Retirar a declaración sen usar de init() de K_PLUGIN_FACTORY_DECLARATION_WITH_BASEFACTORY_SKEL
- Nova API de spdx en KAboutLicense para obter as expresións de licenza de SPDX
- kdirwatch: Evitar unha quebra potencial se o punteiro d se destrúe antes que KDirWatch (fallo 381583)
- Corrixir a visualización de formatDuration con arredondamento (fallo 382069)

### KDeclarative

- corrixir que plasmashell retirase a definición de QSG_RENDER_LOOP

### Compatibilidade coa versión 4 de KDELibs

- Corrixir «Unha nota sobre a obsolescencia de KUrl::path() é incorrecta en Windows» (fallo 382242)
- Actualizar kdelibs4support para usar a compatibilidade segundo o destino que fornece kdewin
- Marcar os construtores tamén como obsoletos
- Sincronizar co KDE4Defaults.cmake de kdelibs

### KDesignerPlugin

- Engadir compatibilidade co novo trebello kpasswordlineedit

### KHTML

- Permitir tamén SVG (fallo 355872)

### KI18n

- Permitir cargar catálogos de internacionalización de calquera lugar
- Asegurarse de que se xera o destino tsfiles

### KIdleTime

- Só requirir Qt5X11Extras cando de verdade o necesitamos

### KInit

- Usar a marca de funcionalidade axeitada para incluír kill(2)

### KIO

- Engadir o novo método urlSelectionRequested a KUrlNavigator
- KUrlNavigator: expoñer o KUrlNavigatorButton que recibiu un evento de soltar
- Poñer en reserva sen preguntar ao usuario cunha xanela emerxente de copiar ou cancelar
- Asegurarse de que KDirLister actualizar os elementos aos que lle cambiase o URL de destino (fallo 382341)
- Permitir pregar as opcións avanzadas de «Abrir con» e ocultalas de maneira predeterminada (fallo 359233)

### KNewStuff

- Dar un pai aos menús de KMoreToolsMenuFactory
- Ao solicitar da caché, informar de todas as entradas en lote

### Infraestrutura KPackage

- kpackagetool agora pode enviar datos de AppStream a un ficheiro
- adoptar o novo KAboutLicense::spdx

### KParts

- Restabelecer url en closeUrl()
- Engadir un modelo para unha aplicación simple baseada en KPart
- Deixar de usar KDE_DEFAULT_WINDOWFLAGS

### KTextEditor

- Xestionar o evento preciso de roda ao ampliar
- Engadir un modelo para un complemento de ktexteditor
- copiar os permisos do ficheiro orixinal ao gardar a copia (fallo 377373)
- evitar quizais unha quebra de stringbuild (fallo 339627)
- corrixir un problema con que se engadise * a liñas fóra de comentarios (fallo 360456)
- corrixir gardar como copia, faltáballe permitir sobrescribir o ficheiro de destino (fallo 368145)
- Orde «set-highlight»: unir os argumentos con espazo
- corrixir unha quebra na destrución da vista por mor dunha limpeza non determinista de obxectos
- Emitir sinais do bordo das iconas cando non se prema ningunha marca
- Corrixir unha quebra no modo de entrada de vi (secuencia: «o» «Esc» «O» «Esc» «.») (fallo 377852)
- Usar un grupo mutuamente excluínte no tipo de marca predeterminado

### KUnitConversion

- Marcar MPa e PSI como unidades comúns

### Infraestrutura de KWallet

- Usar CMAKE_INSTALL_BINDIR para a xeración de servizo de D-Bus

### KWayland

- Destruír todos os obxectos de KWayland creador polo rexistro cando se destrúe
- Emitir connectionDied se se destrúe a QPA
- [cliente] Seguir todos os ConnectionThreads creados e engadir unha API para acceder a eles
- [servidor] Enviar a saída da entrada de texto se a superficie co foco deixa de estar asociada
- [servidor] Enviar a saída do punteiro se a superficie co foco deixa de estar asociada
- [cliente] Facer un seguimento axeitado de enteredSurface en Keyboard
- [servidor] Enviar a saída do teclado cando o cliente destrúe a superficie co foco (fallo 382280)
- comprobar a validez de Buffer (fallo 381953)

### KWidgetsAddons

- Extraer un trebello de contrasinal do editor de liña → nova clase KPasswordLineEdit
- Corrixiuse unha quebra ao buscar coas funcionalidades de accesibilidade activadas (fallo 374933)
- [KPageListViewDelegate] Pasar o trebello a drawPrimitive en drawFocus

### KWindowSystem

- Retirar a dependencia de QWidget da cabeceira

### KXMLGUI

- Deixar de usar KDE_DEFAULT_WINDOWFLAGS

### NetworkManagerQt

- Engadir compatibilidade con ipv*.route-metric
- Corrixíronse as enumeracións sen definir de NM_SETTING_WIRELESS_POWERSAVE_FOO (fallo 382051)

### Infraestrutura de Plasma

- [Interface de Containment] emitir sempre contextualActionsAboutToShow para containment
- Tratar as etiquetas de Button e ToolButton como texto plano
- Non realizar correccións específicas de Wayland ao estar en X (fallo 381130)
- Engadir KF5WindowSystem á interface de ligazón
- Declarar AppManager.js como biblioteca de pragma
- [PlasmaComponents] Retirar Config.js
- usar texto simple de maneira predeterminada nas etiquetas
- Cargar as traducións dos ficheiros de KPackage se está empaquetado (fallo 374825)
- [PlasmaComponents Menu] Non quebrar ante accións nulas
- [Plasma Dialog] Corrixir condicións de marcas
- actualizar a icona da área de notificacións para Akregator (fallo 379861)
- [Contedor Interface] Manter o contedor en RequiresAttentionStatus mentres o menú contextual está aberto (fallo 351823)
- Corrixir a xestión de teclas da disposición da barra de separadores en RTL (fallo 379894)

### Sonnet

- Permitir construír Sonnet sen Qt5Widgets
- cmake: reescribir FindHUNSPELL.cmake para usar pkg-config

### Realce da sintaxe

- Permitir construír KSyntaxHighlighter sen Qt5Gui
- Engadir a posibilidade de compilación cruzada do indexador de realce
- Temas: retirar todos os metadatos que non se usan (licenza, autor, só lectura)
- Tema: retirar os campos de licenza e autor
- Tema: derivar a marca de só lectura do ficheiro en disco
- Engadir salientado de sintaxe para a linguaxe de modelaxe de datos YANG
- PHP: Engadir palabras clave de PHP 7 (fallo 356383)
- PHP: Limpar a información de PHP 5
- corrixir gnuplot, facer fatais os espazos sobrantes
- corrixir a detección de «else if», temos que cambiar de contexto, engadir unha regra adicional
- comprobacións de indexador para os espazos iniciais e finais no realce de XML
- Doxygen: Engadir salientado para Doxyfile
- engadir ao realce de C tipos estándar que faltaban e actualizar a C11 (fallo 367798)
- Q_PI D → Q_PID
- PHP: mellorar o realce de variábeis entre chaves entre comiñas duplas (fallo 382527)
- Engadir salientado de PowerShell
- Haskell: engadir a extensión de ficheiro .hs-boot (módulo de preparación) (fallo 354629)
- Corrixir replaceCaptures() para que funcione con máis de 9 capturas
- Ruby: Usar WordDetect en vez de StringDetect para o cumprimento de palabras completas
- Corrixir un realce incorrecto de BEGIN e END en palabras como «EXTENDED» (fallo 350709)
- PHP: Retirar mime_content_type() da lista de funcións obsoletas (fallo 371973)
- XML: Engadir a extensión e tipo MIME XBEL ao realce de XML (fallo 374573)
- Bash: Corrixir un realce incorrecto para as opcións de orde (fallo 375245)
- Perl: Corrixir o realce de heredoc con espazos antes do delimitador (fallo 379298)
- Actualizar o ficheiro de sintaxe de SQL de Oracle (fallo 368755)
- C++: Corrixir que «-» non sexa parte das cadeas UDL (fallo 380408)
- C++: indicadores de formato de printf: engadir «n» e «p», retirar «P» (fallo 380409)
- C++: Corrixir que o valor do carácter teña a cor das cadeas (fallo 380489)
- VHDL: corrixir un erro de realce ao usar corchetes e atributos (fallo 368897)
- realce de zsh: corrixir unha expresión matemática nunha expresión de cadeas subordinadas (fallo 380229)
- Realce de JavaScript: engadir compatibilidade coa extensión E4X de XML (fallo 373713)
- Retirar a regra da extensión «*.conf»
- Sintaxe de Pug e Jade

### ThreadWeaver

- Engadir unha exportación que faltaba en QueueSignals

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
