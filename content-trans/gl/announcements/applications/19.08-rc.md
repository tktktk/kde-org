---
aliases:
- ../announce-applications-19.08-rc
date: 2019-08-02
description: KDE Ships Applications 19.08 Release Candidate.
layout: application
release: applications-19.07.90
title: KDE publica a candidata a versión final da versión 19.08 das aplicacións de
  KDE
version_number: 19.07.90
version_text: 19.08 Release Candidate
---
August 02, 2019. Today KDE released the Release Candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

Check the <a href='https://community.kde.org/Applications/19.08_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release.

Hai que probar ben a versión 19.08 das aplicacións de KDE para manter e mellorar a calidade e a experiencia de usuario. Os usuarios reais son críticos para manter unha alta calidade en KDE, porque os desenvolvedores simplemente non poden probar todas as configuracións posíbeis. Contamos con vostede para axudarnos a atopar calquera fallo canto antes para poder solucionalo antes da versión final. Considere unirse ao equipo instalando a candidata a versión final <a href='https://bugs.kde.org/'>e informando de calquera fallo</a>.
