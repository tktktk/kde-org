---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: KDE rilascia KDE Applications 15.04.3
layout: application
title: KDE rilascia KDE Applications 15.04.3
version: 15.04.3
---
1 luglio 2015. Oggi KDE ha rilasciato il terzo aggiornamento di stabilizzazione per <a href='../15.04.0'>KDE Applications 15.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 20 errori corretti includono miglioramenti a Kdenlive, kdepim, Kopete, ktp-contact-list, Marble, Okteta e Umbrello.

Questo rilascio include inoltre le versioni con supporto a lungo termine (Long Term Support) di Plasma Workspaces 4.11.21, KDE Development Platform 4.14.10 e della suite Kontact 4.14.10.
