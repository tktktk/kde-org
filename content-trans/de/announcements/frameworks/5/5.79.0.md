---
aliases:
- ../../kde-frameworks-5.79.0
date: 2021-02-13
layout: framework
libCount: 83
qtversion: 5.14
---
###  Attica

* Port from QNetworkRequest::FollowRedirectsAttribute to QNetworkRequest::RedirectPolicyAttribute

### Baloo

* [SearchStore] Remove filesystem dependencies from includeFolder prop
* [FileIndexerConfig] Fix hidden check for explicitly included directories
* [FilteredDirIterator] Request hidden files from QDirIterator

### Breeze Icons

* new telegram-panel icons
* Use correct style for align-horizontal-left-out icons (bug 432273)
* Add new kickoff icons (bug 431883)
* Add rating-half, 100% opacity for rating-unrated, Text color for rating in dark theme
* Remove KeePassXC icons (bug 431593)
* Fix @3x icons (bug 431475)
* add neochat icon

### Extra CMake-Module

* Only enable GNU_TAR_FOUND when --sort=name is available
* Remove fastlane metadata generation from a given APK
* KDEFrameworksCompilerSettings: define -DQT_NO_KEYWORDS and -DQT_NO_FOREACH by default
* [KDEGitCommitHooks] Create copy of scripts in source dir
* Support the new Appstream file extension as well
* fetch-translations: Resolve the URL before passing it to fetchpo.rb
* Consider Appstream donation URLs for creating F-Droid metadata
* Fix permissions for scripts (bug 431768)
* ECMQtDeclareLoggingCategory: create .categories files in build, not configure
* Add cmake function to configure git pre-commit hooks

### Framework-Integration

* Fix window decorations not being uninstallable (bug 414570)

### KDE Doxygen Tools

* Make sure we don't use default doxygen font
* Improve QDoc rendering and fix a few bug in the dark theme
* Update maintainership information
* Brand new theme consistent with develop.kde.org/docs

### KCalendarCore

* Unregister MemoryCalendar as an observer when deleting an incidence
* Use the recurrenceId to delete the right occurrence
* Also clear notebook associations when closing a MemoryCalendar

### KCMUtils

* Ensure single column mode

### KCodecs

* Remove the usage of non-UTF-8 string literals

### KCompletion

* Fix regression caused due to porting from operator+ to operator|

### KConfig

* Refactor window geometry save/restore code to be less fragile
* Fix restoring window size when closed while maximized (bug 430521)
* KConfig: preserve the milliseconds component of QDateTime

### KCoreAddons

* Add KFuzzyMatcher for fuzzy filtering of strings
* KJob::infoMessage: document that the richtext argument will be removed, unused
* KJobUiDelegate::showErrorMessage: implement with qWarning()
* Deprecate X-KDE-PluginInfo-Depends related methods
* Drop X-KDE-PluginInfo-Depends keys

### KDeclarative

* Allow single column items
* KeySequenceItem: Assign empty string on clear instead of undefined (bug 432106)
* Disambiguate selected vs hovered states for GridDelegate (bug 406914)
* Use Single mode by default

### KFileMetaData

* ffmpegextractor: Use av_find_default_stream_index to find video stream

### KHolidays

* Update Mauritius holidays for 2021
* Update Taiwanese holidays

### KI18n

* Don't set codec for textstream when building against Qt6

### KImageFormats

* Simplify portion of NCLX color profile code
* [imagedump] Add "list MIME type" (-m) option
* Fix crash with malformed files
* ani: Make sure riffSizeData is of the correct size before doing the quint32_le cast dance
* Add plugin for animated Windows cursors (ANI)

### KIO

* Use Q_LOGGING_CATEGORY macro instead of explicit QLoggingCategory (bug 432406)
* Fix default codec being set to "US-ASCII" in KIO apps (bug 432406)
* CopyJob: fix crash with skip/retry (bug 431731)
* KCoreDirLister: un-overload canceled() and completed() signals
* KFilePreviewGenerator: modernise code base
* KCoreDirLister: un-overload clear() signal
* MultiGetJob: un-overload signals
* FileJob: un-overload close() signal
* SkipDialog: deprecate result(SkipDialog *_this, int _button) signal
* Fix lockup when renaming a file from properties dialog (bug 431902)
* Deprecate addServiceActionsTo and addPluginActionsTo
* [KFilePlacesView] Lower opacity for hidden items in "show all"
* Don't change directories when opening non listable urls
* Tweak KFileWidget::slotOk logic when in files+directory mode
* FileUndoManager: fix undo of copy empty directory
* FileUndoManager: don't overwrite files on undo
* FileUndoManager: deprecate undoAvailable() method
* ExecutableFileOpenDialog: make the text label more generic
* KProcessRunner: only emit processStarted() signal once
* Revert "kio_trash: fix the logic when no size limit is set"

### Kirigami

* Use non symbolic icon for quit action
* Fix toolbar menu buttons depress correctly
* Use subsection instead of section
* [controls/BasicListItem]: Add reserveSpaceForSubtitle property
* make nav button implicit height explicit
* Fix BasicListItem vertical alignment
* [basiclistitem] Ensure icons are square
* [controls/ListItem]: Remove indented separator for leading items
* Re-add right list item separator margin when there's a leading item
* Don't manually call reverseTwinsChanged when destructing FormLayout (bug 428461)
* [org.kde.desktop/Units] Make durations match controls/Units
* [Units] Reduce veryLongDuration to 400ms
* [Units] Reduce shortDuration and longDuration by 50ms
* Don't consider Synthetized mouse events as Mouse (bug 431542)
* more aggressively use implicitHeight instead of preferredHeight
* Use atlas textures for icons
* controls/AbstractApplicationHeader: vertically center children
* [actiontextfield] Fix inline action margins and sizing
* correctly update header size (bug 429235)
* [controls/OverlaySheet]: Respect Layout.maximumWidth (bug 431089)
* [controls/PageRouter]: Expose set parameters when dumping currentRoutes
* [controls/PageRouter]: Expose top level parameters in 'params' property map
* move pagerouter related examples in a subfolder
* Possible to drag the window by non interactive areas
* AbstractApplicationWindow: Use wide window on desktop systems for all styles
* Don't hide list item separator on hover when background is transparent
* [controls/ListItemDragHandle] Fix wrong arrangement on no-scrolling case (bug 431214)
* [controls/applicationWindow]: Account for drawer widths when computing wideScreen

### KNewStuff

* Refactor the KNSQuick Page header and footer for Kirigami-ness
* Add number label to Ratings component for easier readability
* Reduce minimum size of QML GHNS dialog window
* Match lighter hovered appearance for KCM grid delegates
* Ensure the minimum width for the QML Dialog is screen-width or less
* Fix the BigPreview delegate's layout
* Revalidate cached entries before showing dialog
* Add support for kns:/ urls to the knewstuff-dialog tool (bug 430812)
* filecopyworker: Open files before reading/writing
* Reset entry to updateable when no payload is identified for updating (bug 430812)
* Fix occasional crash due to holding a pointer incorrectly
* Deprecate DownloadManager class
* Deprecate AcceptHtmlDownloads property
* Deprecate ChecksumPolicy and SignaturePolicy properties
* Deprecate Scope property
* Deprecate CustomName property

### KNotification

* Emit NewMenu when new context menu is set (bug 383202)
* Deprecate KPassivePopup
* Make sure all backends ref the notification before doing work
* Make the notification example app build and work on Android
* Move notification id handling into KNotification class
* Fix removing pending notification from queue (bug 423757)

### KPackage-Framework

* Document PackageStructure ownership when using PackageLoader

### KPty

* Fix generating the full path to kgrantpty in the code for ! HAVE_OPENPTY

### KQuickCharts

* Add a "first" method to ChartDataSource and use it in Legend (bug 432426)

### KRunner

* Check for selected action in case of informational match
* Fix empty result string for current activity
* Deprecate overloads for QueryMatch ids
* [DBus Runner] Test RemoteImage

### KService

* Deprecate KPluginInfo::dependencies()
* CMake: Specify add_custom_command() dependencies
* Explicitly deprecate overload for KToolInvocation::invokeTerminal
* Add method to get KServicePtr of default terminal application
* KService: add method to set workingDirectory

### KTextEditor

* [Vimode] Do not switch view when changing case (~ command) (bug 432056)
* Increase maximum indentation width to 200 (bug 432283)
* ensure we update the range mapping e.g. on invalidation of now empty ranges
* Only show bookmark chars error when in vimode (bug 424172)
* [vimode] Fix motion to matching item off-by-one
* Retain replacement text as long as the power search bar is not closed (bug 338111)
* KateBookMarks: modernise code base
* Don't ignore alpha channel when there is a mark
* Fix alpha channel being ignored when reading from config interface
* Prevent bracket match preview from overextending outside the view
* Prevent bracket match preview from sometimes lingering after switching to a different tab
* Make the bracket match preview more compact
* Do not show bracket match preview if it will cover the cursor
* Maximize width of bracket match preview
* Hide the bracket match preview when scrolling
* avoid duplicated highlighting ranges that kill ARGB rendering
* expose the global KSyntaxHighlighting::Repository read-only
* Correct indentation bug when line contains "for" or "else"
* Correct an indentation bug
* remove the special case tagLine, it did lead to random update faults in e.g. the
* fix rendering of word wrap makers + selection
* Fix indent for when pressing enter and the function param has a comma at the end
* paint the small gap in selection color, if previous line end is in selection
* simplify code + fix comments
* revert cut error, did delete too much code for extra renderings
* avoid full line selection painting, more in line with other editors
* adapt indenter to changed hl files
* [Vimode] Port Command to QRegularExpression
* [Vimode] Port findPrevWordEnd and findSurroundingBrackets to QRegularExpression
* [Vimode] Port QRegExp::lastIndexIn to QRegularExpression and QString::lastIndexOf
* [Vimode] Port ModeBase::addToNumberUnderCursor to QRegularExpression
* [Vimode] Port simple uses of QRegExp::indexIn to QRegularExpression and QString::indexOf
* Use rgba(r,g,b,aF) to handle the alpha channel for html export
* Respect alpha colors while exporting html
* Introduce a helper method for getting the color name correctly
* Alpha colors support: Config layer
* avoid that line changed markers kill current line highlight
* paint current line highlighting over iconborder, too
* Enable alpha channel for editor colors
* Fix current line highlight has a tiny gap at the beginning for dyn. wrapped lines
* Dont do needles computation, just compare values directly
* Fix current line highlight has a tiny gap at the beginning
* [Vimode] Do not skip folded range when the motion lands inside
* Use a range-for loop over m_matchingItems instead of using QHash::keys()
* Port normalvimode to QRegularExpression
* properly export right dependencies
* expose the KSyntaxHighlighting theme
* add configChanged to KTextEditor::Editor, too, for global configuration changes
* reorder the config stuff a bit
* fix key handling
* add doc/view parameters to new signals, fix old connects
* allow to access & alter the current theme via config interface
* Remove const qualifier when passing LineRanges around
* Use .toString() since QStringView is lacking .toInt() in older Qt versions
* Declare toLineRange() constexpr inline whenever possible
* Reuse QStringView version from QStringRev, remove const qualifier
* Move TODO KF6 comment out of doxygen comment
* Cursor, Range: Add fromString(QStringView) overload
* Allow "Dynamic Word Wrap Align Indent" to be disabled (bug 430987)
* LineRange::toString(): Avoid '-' sign, as for negative numbers it's confusing
* Use KTextEditor::LineRange in notifyAboutRangeChange() mechanism
* Port tagLines(), checkValidity() and fixLookup() to LineRanges
* Add KTextEditor::LineRange
* Move KateTextBuffer::rangesForLine() impl to KateTextBlock and avoid unnecessary container constructions
* [Vimode]Fix search inside fold ranges (bug 376934)
* [Vimode] Fix Macro Completion Replay (bug 334032)

### KTextWidgets

* Have more private classes inherit from those of the parents

### KUnitConversion

* Define variable before using it

### KWidgetsAddons

* Make use of AUTORCC
* Have more private classes inherit from those of the parents
* Explicitly include QStringList

### KWindowSystem

* Add fractional opacity convenience helpers
* Really fix includes
* Fix includes
* xcb: Work with the active screen as reported by QX11Info::appScreen()

### KXMLGUI

* Fix includes
* Add KXMLGUIFactory::shortcutsSaved signal
* Use the correct kde get involved url (bug 430796)

### Plasma Framework

* [plasmoidheading] Use intended color for footers
* [plasmacomponents3/spinbox] Fix selected text color
* widgets>lineedit.svg: fix pixel misalignment woes (bug 432422)
* Fix inconsistent left and right padding in PlasmoidHeading
* Update breeze-dark/breeze-light colors
* Revert "[SpinBox] Fix logic error in scroll directionality"
* [calendar] fix missing import names
* [ExpandableListItem] Make expanded actions list view respect font
* DaysCalendar: port to PC3/QQC2 where possible
* Remove hover animation from flat buttons, calendar, listitems, buttons
* Dump plasmoid errors into console
* Solve cyclic dependency of Units.qml
* [PlasmaComponents MenuItem] Create dummy action when action gets destroyed
* don't make pixmaps bigger than needed
* Add JetBrains IDEs project files to ignored
* Fix Connections warnings
* Add RESET to globalShortcut property (bug 431006)

### Purpose

* [nextcloud] Rework configuration UI
* Evaluate initial configuration
* Clip ListViews in kdeconnect and bluetooth config
* Remove unneeded Layout attached properties
* [plugins/nextcloud] Use Nextcloud icon
* [cmake] Move find_package to toplevel CMakeLists.txt

### QQC2StyleBridge

* [combobox] Fix touchpad scroll speed (bug 400258)
* qw can be null
* Support QQuickWidget (bug 428737)
* allow drag window from empty areas

### Solid

* CMake: use configure_file() to ensure noop incremental builds
* [Fstab] Ignore docker overlay mounts (bug 422385)

### Sonnet

* Dont do multiple lookups when one is enough

### Syntax Highlighting

* add missing increment of version of context.xml
* Support official file endings, see https://mailman.ntg.nl/pipermail/ntg-context/2020/096906.html
* update reference results
* less vibrant operator color for breeze themes
* fix syntax for new hugo
* fix syntax errors
* Improve readability of Solarized dark theme
* remove unnecessary captures with a dynamic rule
* merge operator highlighting => update references
* merge operator highlighting
* add const overloads for some accessors
* Bash, Zsh: fix cmd;; in a case (bug 430668)
* Atom Light, Breeze Dark/Light: new color for Operator ; Breeze Light: new color for ControlFlow
* email.xml: Detect nested comments and escaped characters (bug 425345)
* Update atom light to use alpha colors
* remap some Symbol and Operator styles to dsOperator
* Bash: fix } in ${!xy*} and more Parameter Expansion Operator (# in ${#xy} ; !,*,@,[*],[@] in ${!xy*}) (bug 430668)
* Update theme revisions
* Update kconfig highlighter to Linux 5.9
* Use rgba(r,g,b,aF) to handle the alpha channel for Qt/Browsers correctly
* Dont use rgba when in themeForPalette
* Make sure rgba is respected in html and Format
* Fixes for atom-one-dark
* Some more updates for Atom One Dark
* Dont check for rgba when checking for a best match
* Allow using the alpha channel in theme colors
* Update monokai colors and fix incorrect blue
* C++: fix us suffix
* Bash: fix #5: $ at the end of a double quoted string
* VHDL: fix function, procedure, type range/units and other improvements
* Update breeze-dark.theme
* Raku: #7: fix symbols that starts with Q
* quickfix missing contrast for Extension
* Add Oblivion color scheme from GtkSourceView/Pluma/gEdit

### ThreadWeaver

* Fix map iterators when building against Qt6
* Don't explicitly init mutexes as NonRecursive

### Sicherheitsinformation

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
