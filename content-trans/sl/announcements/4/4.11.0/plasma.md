---
date: 2013-08-14
hidden: true
title: Plasma Workspaces 4.11 še naprej izboljšuje uporabniško izkušnjo
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`KDE Plasma Workspaces 4.11` width="600px" >}}

V izdaji Plasma Workspaces 4.11 je opravilna vrstica – eden najpogosteje uporabljenih pripomočkov Plasma – <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new- task-manager'>je bil prenesen v QtQuick</a>. Nova opravilna vrstica, čeprav je ohranila videz in funkcionalnost starega dvojnika, kaže bolj dosledno in tekoče vedenje. Prenos je odpravil tudi številne dolgotrajne napake. Pripomoček za baterijo (ki je prej lahko prilagajal svetlost zaslona) zdaj podpira tudi svetlost tipkovnice in se lahko ukvarja z več baterijami v perifernih napravah, kot sta brezžična miška in tipkovnica. Prikazuje napolnjenost baterije za vsako napravo in opozori, ko je ena izpraznjena. Meni Kickoff zdaj nekaj dni prikazuje nedavno nameščene aplikacije. Nenazadnje pojavna okna z obvestili imajo zdaj gumb za prilagajanje, kjer lahko preprosto spremenite nastavitve za to določeno vrsto obvestila.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Izboljšana obdelava obvestil` width="600px" >}}

KMix, KDE-jev mešalnik zvoka, je prejel znatno zmogljivost in stabilnost ter <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>popolno podporo za nadzor medijskega predvajalnika, ki</a> temelji na standardu MPRIS2.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`Prenovljeni programček baterije v akciji` width="600px" >}}

## KWin Window Manager in Compositor

Naš upravitelj oken KWin je ponovno prejel pomembne posodobitve, ki se oddaljujejo od stare tehnologije in vključujejo komunikacijski protokol 'XCB'. Posledica tega je bolj gladko in hitrejše upravljanje oken. Predstavljena je bila tudi podpora za OpenGL 3.1 in OpenGL ES 3.0. Ta izdaja vključuje tudi prvo eksperimentalno podporo za naslednika X11 Wayland. To omogoča uporabo KWin z X11 na vrhu sklada Wayland. Za več informacij o uporabi tega eksperimentalnega načina glejte <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/ '>to objavo</a>. Skriptni vmesnik KWin je doživel velike izboljšave, zdaj ima podporo za uporabniški vmesnik za konfiguracijo, nove animacije in grafične učinke ter številne manjše izboljšave. Ta izdaja prinaša boljšo ozaveščenost na več zaslonih (vključno z možnostjo sijaja robov za "vroče kote"), izboljšano hitro polaganje ploščic (s nastavljivimi območji ploščic) in običajno vrsto popravkov napak in optimizacij. Glejte <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>tukaj</a> in <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>tukaj</a> za več podrobnosti.

## Upravljanje zaslonov in spletne bližnjice

Prilagoditev monitorja v sistemskih nastavitvah je <a href='http://www.afiestas.org/kscreen-1-0-released/'>nadomestilo novo orodje KScreen</a>. KScreen prinaša inteligentnejšo podporo za več monitorjev v Plasma Workspaces, ki samodejno prilagodi nove zaslone in si zapomni nastavitve za ročno prilagojene zaslone. Ima intuitiven, vizualno usmerjen vmesnik in upravlja prerazporeditev zaslonov s preprostim vlečenjem in spuščanjem.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`Novo upravljanje zaslona KScreen` width="600px" >}}

Spletne bližnjice, najlažji način za hitro iskanje tistega, kar iščete v spletu, so bile očiščene in izboljšane. Številne so bile posodobljene za uporabo varno šifriranih (TLS/SSL) povezav, dodane so bile nove spletne bližnjice in nekaj zastarelih bližnjic odstranjenih. Izboljšan je bil tudi postopek dodajanja lastnih spletnih bližnjic. Več podrobnosti najdete <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>tukaj</a>.

Ta izdaja označuje konec Plasma Workspaces 1, ki je del serije funkcij KDE SC 4. Za lažji prehod na naslednjo generacijo bo ta izdaja podprta še vsaj dve leti. Poudarek razvoja funkcij se bo zdaj preusmeril na Plasma Workspaces 2, izboljšave zmogljivosti in odpravljanje napak pa se bodo osredotočili na serijo 4.11.

#### Nameščanje Plasma

Programska oprema KDE, vključno z vsemi knjižnicami in aplikacijami, je prosto na voljo pod odprto-kodnimi licencami. Programska oprema KDE deluje na različnih konfiguracijah strojne opreme in arhitekturah CPE, kot sta ARM in x86, operacijskih sistemih in deluje s kakršnim koli upravljalnikom oken ali namiznim okoljem. Poleg Linuxa in drugih operacijskih sistemov, ki temeljijo na sistemu UNIX, lahko najdete različice večine aplikacij KDE za Microsoft Windows na spletnem mestu <a href='http://windows.kde.org'>Programska oprema KDE za Windows</a> in Apple Mac OS X različice na <a href='http://mac.kde.org/'>Programska oprema KDE na spletnem mestu Mac</a>. Eksperimentalne različice aplikacij KDE za različne mobilne platforme, kot so MeeGo, MS Windows Mobile in Symbian, je mogoče najti na spletu, vendar trenutno niso podprte. <a href='http://plasma-active.org'>Plasma Active</a> je uporabniška izkušnja za širši spekter naprav, kot so tablični računalniki in druga mobilna strojna oprema.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paketi

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Mesta paketov

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Sistemske zahteve

Da bi kar najbolje izkoristili te izdaje, priporočamo uporabo novejše različice Qt, kot je 4.8.4. To je potrebno za zagotovitev stabilne in učinkovite izkušnje, saj so bile nekatere izboljšave programske opreme KDE dejansko narejene v osnovnem okviru Qt.<br /> Da bi v celoti izkoristili zmogljivosti programske opreme KDE, priporočamo tudi za uporabo najnovejših grafičnih gonilnikov za vaš sistem, saj lahko s tem bistveno izboljšate uporabniško izkušnjo, tako v izbirni funkcionalnosti kot pri splošni zmogljivosti in stabilnosti.

## Danes objavljeno tudi:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Aplikacije KDE 4.11 prinašajo ogromen korak naprej pri upravljanju osebnih podatkov in vsepovsod izboljšave</a>

Ta izdaja zaznamuje velike izboljšave sklada KDE PIM, kar daje veliko boljšo zmogljivost in številne nove zmožnosti. Kate izboljšuje produktivnost razvijalcev Python in Javascript z novimi vtičniki, Dolphin je postal hitrejši in izobraževalne aplikacije prinašajo različne nove zmožnosti.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/>Platforma KDE 4.11 zagotavlja boljšo zmogljivost</a>

Ta izdaja platforme KDE 4.11 se še naprej osredotoča na stabilnost. Nove zmožnosti se izdelujejo za našo prihodnjo izdajo KDE Frameworks 5.0, toda za stabilno izdajo smo uspeli stisniti optimizacije za naš okvir Nepomuk.
