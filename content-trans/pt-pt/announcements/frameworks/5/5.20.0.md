---
aliases:
- ../../kde-frameworks-5.20.0
date: 2016-03-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Ícones do Brisa

- Muitos ícones novos
- Adição dos ícones de tipos MIME do VirtualBox e de mais alguns tipos MIME em falta
- Adição do suporte de ícones do Synaptic e Octopi
- Correcção do ícone de corte (erro 354061)
- Correcção do nome do audio-headphones.svg (+=d)
- Ícones de classificação com margens menores (1px)

### Integração da Plataforma

- Remover o nome do ficheiro possível em KDEPlatformFileDialog::setDirectory()
- Não filtrar por nome, caso existam tipos MIME

### KActivities

- Remoção da dependência do Qt5::Widgets
- Remoção da dependência do KDBusAddons
- Remoção de dependência do KI18n
- Remoção de inclusões não usadas
- Resultado dos programas de consola melhorado
- Adição do modelo de dados (ActivitiesModel) que mostra as actividades para a biblioteca
- Compilação por omissão apenas da biblioteca
- Remoção das componentes do serviço e da área de trabalho da compilação
- Passagem da biblioteca de 'src/lib/core' para 'src/lib'
- Correcção de avisos do CMake
- Correcção de estoiro no menu de contexto das actividades (erro 351485)

### KAuth

- Correcção de bloqueio do 'kded5' quando um programa que usa o 'kauth' sai

### KConfig

- KConfigIniBackend: Correcção da dissociação dispendiosa na pesquisa

### KCoreAddons

- Correcção da migração da configuração do Kdelibs4 para o Windows
- Adição de uma API para obter a informação da versão em execução das Plataformas
- KRandom: Não usar 16K do /dev/urandom como base para o rand() (erro 359485)

### KDeclarative

- Não invocar um ponteiro para um objecto nulo (erro 347962)

### KDED

- Possibilidade de compilação com o -DQT_NO_CAST_FROM_ASCII

### Suporte para a KDELibs 4

- Correcção da gestão de sessões nas aplicações baseadas no KApplication (erro 354724)

### KDocTools

- Uso de caracteres Unicode para as chamadas de atenção

### KFileMetaData

- O KFileMetadata consegue agora pesquisar e sondar informações sobre o e-mail original ao qual um dado ficheiro gravado foi associado como anexo

### KHTML

- Correcção da actualização do cursor na janela
- Limite no uso da memória em cadeias de caracteres
- Visualizador de 'applets' de Java do KHTML: reparação da chamada inválida de DBus ao 'kpasswdserver'

### KI18n

- Usar uma macro de importação portável para o 'nl_msg_cat_cntr'
- Ignorar a pesquisa do '.' e '..' para descobrir as traduções de uma aplicação
- Restrição do uso do '_nl_msg_cat_cntr' para as implementações de 'gettext' da GNU
- Adição do KLocalizedString::languages()
- Colocação das chamadas do Gettext apenas se tiver sido localizado o catálogo

### KIconThemes

- Certificação de que a variável está a ser inicializada

### KInit

- kdeinit: Preferência no carregamento das bibliotecas a partir do RUNPATH
- Implementação do "POR-FAZER do Qt5: uso do QUrl::fromStringList"

### KIO

- Correcção da quebra de ligação do 'app-slave' do KIO se a variável 'appName' tiver uma '/' (erro 357499)
- Tentativa de vários métodos de autenticação em caso de falha
- ajuda: correcção do mimeType() no get()
- KOpenWithDialog: apresentação do nome e comentário do tipo MIME no texto da opção "Recordar" (erro 110146)
- Uma série de alterações para evitar uma nova listagem de uma pasta após mudar o nome de um ficheiro em mais casos (erro 359596)
- http: mudança de nome do 'm_iError' para 'm_kioError'
- kio_http: leitura e eliminação do conteúdo após um 404 com 'errorPage=false'
- kio_http: correcção de detecção do tipo MIME quando o URL termina em '/'
- FavIconRequestJob: adição de método hostUrl() para que o Konqueror possa descobrir para o que foi a tarefa no 'slot'
- FavIconRequestJob: correcção do bloqueio da tarefa ao interromper no caso de um 'favicon' demasiado grande
- FavIconRequestJob: correcção do errorString(), sendo que só tinha o URL
- KIO::RenameDialog: reposição do suporte para a antevisão, adição das legendas de datas e tamanho (erro 356278)
- KIO::RenameDialog: remodelação de código duplicado
- Correcção de conversões inválidas de localização-para-QUrl
- Uso do 'kf5.kio' no nome da categoria para corresponder a outras categorias

### KItemModels

- KLinkItemSelectionModel: Adição de um novo construtor predefinido
- KLinkItemSelectionModel: Tornar alterável o modelo de selecção ligada
- KLinkItemSelectionModel: Tratamento das alterações ao modelo 'selectionModel'
- KLinkItemSelectionModel: Não armazenar localmente o modelo
- KSelectionProxyModel: Correcção de erro na iteração
- Reposição do estado do KSelectionProxyModel quando necessário
- Adição de uma propriedade que indica se os modelos formam uma cadeia ligada
- KModelIndexProxyMapper: Simplificação da lógica da verificação da ligação

### KJS

- Limite no uso da memória em cadeias de caracteres

### KNewStuff

- Apresentação de um aviso em caso de erro no Motor

### Plataforma de Pacotes

- Marcação do KDocTools como opcional no KPackage

### KPeople

- Correcção de utilizações obsoletas da API
- Adição do 'actionType' ao 'plugin' decorativo
- Inversão da lógica de filtragem no PersonsSortFilterProxyModel
- Ligeiramente melhor usabilidade do exemplo em QML
- Adição do 'actionType' ao PersonActionsModel

### KService

- Simplificação do código, redução das eliminações de referências de ponteiros, melhorias relacionadas com o contentor
- Adição do programa de testes 'kmimeassociations_dumper', inspirado pelo erro 359850
- Correcção das aplicações do Chromium/Wine não carregarem em algumas distribuições (erro 213972)

### KTextEditor

- Correcção do realce de todas as ocorrências no ReadOnlyPart
- Não iterar sobre uma QString como se fosse uma QStringList
- Inicialização adequada de QMaps estáticos
- Preferência do toDisplayString(QUrl::PreferLocalFile)
- Suporte do envio de caracteres subjugados a partir do método de introdução
- Não estoirar no encerramento, quando a animação de texto está ainda em execução

### Plataforma da KWallet

- Certificação que o KDocTools é pesquisado
- Não passar um número negativo ao DBus, dado que estoira na 'libdbus'
- Limpeza dos ficheiros do CMake
- KWallet::openWallet(Synchronous): não expirar ao fim do tempo-limite de 25 segundos

### KWindowSystem

- suporte para o _NET_WM_BYPASS_COMPOSITOR (erro 349910)

### KXMLGUI

- Usar o nome não-nativo da Língua como alternativa
- Correcção da gestão de sessões defeituosa desde o KF5 / Qt5 (erro 354724)
- Esquemas de atalhos: suporte para esquemas instalados a nível global
- Uso do qHash(QKeySequence) do Qt ao compilar com o Qt 5.6+
- Esquemas de atalhos: correcção do erro em que dois KXMLGUIClients com o mesmo nome substituem o ficheiro de esquema um do outro
- kxmlguiwindowtest: adição da janela de atalhos, para testar o editor de esquemas de atalhos
- Esquemas de atalhos: melhoria da usabilidade na alteração de textos na GUI
- Esquemas de atalhos: melhoria na lista esquemas (tamanho automático, não limpar em caso de esquemas desconhecidos)
- Esquemas de atalhos: não anteceder o nome do cliente GUI no nome do ficheiro
- Esquemas de atalhos: criação de uma pasta ao tentar gravar um novo esquema de atalhos
- Esquemas de atalhos: reposição da margem da disposição por parecer muito desorganizada
- Correcção de fuga de memória na rotina de arranque do KXmlGui

### Plataforma do Plasma

- IconItem: Não substituir o código ao usar o QIcon::name()
- ContainmentInterface: Correcção do uso do right() e bottom() do QRect
- Remoção efectiva de código duplicado para lidar com QPixmaps
- Adição de documentação da API para o IconItem
- Correcção de folhas de estilo (erro 359345)
- Não limpar a máscara da janela em cada mudança de geometria, quando a composição está activa e não está definida nenhuma máscara
- Applet: Não estoirar com a remoção do painel (erro 345723)
- Theme: Eliminação da 'cache' de imagens ao mudar de tema (erro 359924)
- IconItemTest: Ignorar quando o 'grabToImage' é mal-sucedido
- IconItem: Correcção da mudança de cores dos ícones SVG carregados a partir do tema de ícones
- Correcção da resolução do 'iconPath' do SVG no IconItem
- Se for passada a localização, escolher a parte final (erro 359902)
- Adição das propriedades 'configurationRequired' e 'reason'
- Passagem do 'contextualActionsAboutToShow' para a Applet
- ScrollViewStyle: Não usar as margens do item invertido
- DataContainer: Correcção das verificações dos 'slots' antes de ligar/desligar
- ToolTip: Impedimento das mudanças múltiplas de geometria ao mudar o conteúdo
- SvgItem: Não usar o Plasma::Theme na tarefa de desenho
- AppletQuickItem: Correcção de pesquisa da própria disposição associada (erro 358849)
- Expansão menor para a barra de tarefas
- ToolTip: Parar a apresentação do tempo se for invocado o 'hideTooltip' (erro 358894)
- Desactivação da animação dos ícones nas dicas do Plasma
- Animações em queda das dicas
- O tema predefinido segue o esquema de cores
- Correcção do IconItem, onde não se carregava os ícones fora do tema com o nome (erro 359388)
- Preferir outros contentores que não o ecrã no containmentAt()
- WindowThumbnail: Eliminação da imagem do GLX no stopRedirecting() (erro 357895)
- Remoção do filtro de 'applets' antigas
- ToolButtonStyle: Não se basear num ID do exterior
- Não assumir a descoberta de uma Corona (erro 359026)
- Calendar: Adição de botões adequados para recuar/avançar e um botão "Hoje" (erros 336124, 348362, 358536)

### Sonnet

- Não desactivar a detecção da língua só porque está definida uma língua
- Desligar a desactivação automática ortográfica por omissão
- Correcção das quebras de texto
- Correcção dos locais de pesquisa do Hunspell onde falta o '/' (erro 359866)
- Adição do &lt;pasta da aplicação&gt;/../share/hunspell à localização de pesquisa dos dicionários

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
