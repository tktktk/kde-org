---
aliases:
- ../announce-applications-15.08.2
changelog: true
date: 2015-10-13
description: O KDE Lança as Aplicações do KDE 15.08.2
layout: application
title: O KDE Lança as Aplicações do KDE 15.08.2
version: 15.08.2
---
13 de Outubro de 2015. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../15.08.0'>Aplicações do KDE 15.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 30 correcções de erros registadas incluem as melhorias no ark, gwenview, kate, kbruch, kdelibs, kdepim, lokalize e umbrello.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.13.
