---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: O KDE Lança as Aplicações do KDE 16.04.0
layout: application
title: O KDE Lança as Aplicações do KDE 16.04.0
version: 16.04.0
---
20 de Abril de 2016. O KDE introduz hoje as Aplicações do KDE 16.04, com uma lista impressionante de actualizações no que respeita a uma melhor facilidade de acesso. A introdução de funcionalidades realmente úteis, bem como a eliminação de alguns problemas menores, faz com que as Aplicações do KDE fiquem um passo mais próximo de lhe oferecer a configuração perfeita para o seu dispositivo.

O <a href='https://www.kde.org/applications/graphics/kcolorchooser/'>KColorChooser</a>, o <a href='https://www.kde.org/applications/utilities/kfloppy/'>KFloppy</a>, o <a href='https://www.kde.org/applications/games/kmahjongg/'>KMahjongg</a> e o <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> foram agora migrados para as Plataformas do KDE 5 e estamos à espera da sua reacção e opiniões sobre as funcionalidades mais recentes que foram introduzidas com esta versão. Também o encorajamos em grande medida a dar o seu suporte ao <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a>, já que teve a sua introdução nas nossas Aplicações do KDE, para obter alguma informação sobre o que gostaria de ver.

### Novidade Mais Recente do KDE

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

Foi adicionada uma nova aplicação ao pacote de Educação do KDE. O <a href='https://minuet.kde.org'>Minuet</a> é uma Aplicação de Educação Musical que lhe oferece suporte completo de MIDI, com o controlo do tempo, timbre e volume, o que o torna adequado tanto para os músicos em aprendizagem como para os mais experientes.

O Minuet inclui 44 exercícios de treino auditivo sobre escalas, acordes, intervalos e ritmos, activa a visualização do conteúdo musical no teclado do piano e permite a integração transparente dos seus próprios exercícios.

### Mais Ajuda para Si

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

O KHelpCenter, que antigamente era distribuído dentro do Plasma, agora faz parte das Aplicações do KDE.

Uma campanha massiva de triagem e limpeza de erros conduzida pela equipa do KHelpCenter resultou em 49 erros resolvidos, muitos dos quais estavam relacionados com a melhoria e na reposição da funcionalidade de pesquisa, que anteriormente não estava a funcionar.

O motor de busca interno dos documentos, que se baseava na aplicação obsoleta ht::/dig, foi substituído por um sistema de indexação e pesquisa novo baseada no Xapian, o que leva à reposição de funcionalidades, como a pesquisa dentro das páginas do 'man', do 'info' e da documentação oferecida pelo KDE, com a capacidade acrescida de adicionar favoritos para as páginas da documentação.

Com a remoção do Suporte para as KDELibs4 e com mais alguma limpeza do código e alguns erros menores, a manutenção do código foi também reorganizada profundamente para lhe apresentar o KHelpCenter de uma forma nova e agradável.

### Controlo Agressivo de Problemas

O pacote Kontact teve um conjunto alargado de 55 erros corrigidos; alguns dos quais estavam relacionados com problemas na definição dos alarmes, bem como na importação de correio do Thunderbird, na redução dos ícones de conversação do Skype &amp; Google Talk na área do Painel de Contactos, algumas soluções alternativas relacionadas com o KMail, como a importação de pastas, as importações de vCard's, a abertura de anexos de e-mail em ODF, a introdução de URL's do Chromium e as diferenças no menu de Ferramentas com a aplicação a ser iniciada como componente do Kontact face a um uso autónomo, a falta de um item de menu 'Enviar', entre outros. Foi adicionado o suporte para as fontes de RSS em Português do Brasil, em conjunto com a correcção dos endereços das fontes em Húngaro e Espanhol.

As novas funcionalidades incluem uma remodelação do editor de contactos do KAddressbook, um novo tema predefinido de Cabeçalhos do KMail, melhorias no Exportador de Configurações e uma correcção no suporte de Favicon's no Akregator. A interface do compositor do KMail foi limpa, em conjunto com a introdução de um novo Tema de Cabeçalhos predefinido do KMail com o tema de Grantlee usado para a página 'Acerca do KMail', tal como o Kontact e o Akregator. O Akregator agora usa o QtWebKit - um dos motores mais importantes para desenhar páginas Web e executar código em JavaScript - dado que o motor de desenho Web e o processo de implementação do suporte para o QtWebEngine no Akregator e nas outras aplicações do pacote Kontact já tinha sido iniciado. Embora diversas funcionalidades já estivessem implementadas como módulos no 'kdepim-addons', as bibliotecas PIM foram divididas em diversos pacotes.

### Pacotes em Profundidade

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

O <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, o gestor de pacotes, teve dois erros relevantes corrigidos, pelo que agora o Ark avisa o utilizador se uma extracção for mal-sucedida por causa de falta de espaço na pasta de destino, assim como também não preenche a RAM durante a extracção de ficheiros enormes por arrastamento.

O Ark também inclui agora uma janela de propriedades que mostra informações como o tipo do pacote, o tamanho comprimido e não-comprimido, os códigos de validação MD5/SHA-1/SHA-256 do pacote aberto de momento.

O Ark também consegue agora abrir e extrair pacotes RAR sem usar os utilitários do RAR não-livres, podendo também abrir, extrair e criar pacotes TAR comprimidos com os formatos lzop/lzip/lrzip.

A Interface de Utilizador do Ark foi reorganizada no menu e na barra de ferramentas, para melhorar a usabilidade e para remover as ambiguidades, assim como para poupar espaço vertical, graças ao facto de a barra de estado ficar agora escondida por omissão.

### Mais Precisão nas Edições de Vídeo

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

O <a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, o editor de vídeo não-linear, tem diversas novas funcionalidades implementadas. O gerador de títulos tem agora uma funcionalidade de grelha, gradientes, a adição de sombras no texto e o ajuste do espaço entre letras/linhas.

A apresentação integrada dos níveis de áudio permite uma monitorização simples do áudio no seu projecto, em conjunto com novas camadas do monitor de vídeo que apresentam a taxa de imagens da reprodução, as zonas seguras e as formas de onda de áudio, bem como uma barra de ferramentas com marcadores de posições e um monitor de ampliação.

Foi também adicionada uma nova funcionalidade da biblioteca que lhe permite copiar/colar as sequências entre projectos e uma vista dividida na linha temporal para comparar e visualizar os efeitos aplicados ao 'clip', tendo uma divisão o efeito e a outra não.

A janela de desenho foi remodelada com uma opção adicional para obter uma codificação mais rápida, dado que a produção de ficheiros grandes e o efeito de velocidade foi feito também para funcionar com o som.

As curvas nas imagens-chave foram introduzidas para alguns efeitos e agora os seus efeitos preferidos poderão ser rapidamente acedidos através do elemento de efeitos favoritos. Ao usar a ferramenta de corte, agora a linha vertical na linha temporal irá mostrar a imagem exacta onde será efectuado o corte, sendo que os geradores de 'clips' acabados de adicionar o permitirão criar 'clips' de barras de cores e contadores. Para além disso, a contagem de utilização do 'clip' foi reintroduzida no Grupo do Projecto, sendo que as miniaturas de áudio também foram remodeladas para ficarem muito mais rápidas.

As correcções de erros mais importantes incluem o estoiro na utilização de títulos (que obriga a usar a última versão do MLT), a correcção de problemas que ocorriam quando a taxa de imagens por segundo do projecto são diferentes de 25, os estoiros na remoção de faixas, o modo de sobreposição com problemas na linha temporal e os problemas/perdas dos efeitos quando usavam uma língua que tem a vírgula como separador. À parte desses problemas, a equipa tem estado constantemente a trabalho para fazer novas melhorias na estabilidade, no funcionamento e em pequenas questões de usabilidade.

### E ainda há mais!

O visualizador de documentos <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a> traz novas funcionalidades sob a forma de anotações incorporadas personalizadas com contornos, a possibilidade de abrir directamente ficheiros incorporados, em vez de apenas os gravar, e apresentar um índice de marcadores quando as ligações-filhas estiverem recolhidas.

O <a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> introduziu um suporte melhorado para o GnuPG 2.1, com uma correcção de erros de testes automáticos e as actualizações incómodas de chaves provocadas pela nova disposição de directórios do GnuPG (GNU Privacy Guard). A geração de chaves ECC foi adicionada, com a apresentação agora dos detalhes da Curva nos certificados ECC. O Kleopatra agora é lançado como um pacote em separado e foi incluído o suporte para os ficheiros .pfx e .crt. Para resolver as diferenças no comportamento das chaves privadas importadas e nas geradas, o Kleopatra permite-lhe agora muda a confiança no dono para total, no caso das chaves privadas importadas. 
