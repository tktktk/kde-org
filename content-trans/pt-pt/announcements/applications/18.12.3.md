---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: O KDE Lança as Aplicações 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: O KDE Lança as Aplicações do KDE 18.12.3
version: 18.12.3
---
{{% i18n_date %}}

Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../18.12.0'>Aplicações do KDE 18.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

A dúzia de correcções de erros registadas inclui melhorias no Kontact, no Ark, no Cantor, no Dolphin, no Filelight, no Juk, no Lokalize, no Umbrello, entre outros.

As melhorias incluem:

- Foi corrigido o carregamento de pacotes .tar.zstd no Ark
- O Dolphin não estoira mais ao parar uma actividade do Plasma
- A mudança para uma partição diferente já não estoira mais o Filelight
