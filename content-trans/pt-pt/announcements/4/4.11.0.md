---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: O KDE Lança a Versão 4.11 da Área de Trabalho Plasma, as Aplicações e
  a Plataforma.
title: Compilação de Aplicações do KDE 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`A Área de Trabalho Plasma do KDE 4.11` >}} <br />

14 de Agosto de 2013. A Comunidade do KDE orgulha-se em anunciar as últimas alterações importantes da Área de Trabalho Plasma, das Aplicações e da Plataforma de Desenvolvimento, oferecendo novas funcionalidades e correcções, enquanto se prepara a plataforma para futuras evoluções. A Área de Trabalho Plasma 4.11 irá ter um suporte a longo prazo, enquanto a equipa se foca na transição técnica para as Plataformas 5. Apresenta deste modo o último lançamento combinado da Área de Trabalho, das Aplicações e da Plataforma sob o mesmo número de versão.<br />

Esta versão é dedicada à memória de <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis</a>, um grande campeão de 'Software' Livre e Aberto da Índia. O Atul conduziu as conferências de Linux em Bangalore e da FOSS.IN desde 2001, sendo ambos eventos marcantes na cena de 'Software' Livre na Índia. O  KDE Índia nasceu no primeiro FOSS.in em Dezembro de 2005. Muitos colaboradores indianos do KDE começaram nestes eventos. Só graças ao encorajamento do Atul é que o Dia do Projecto KDE no FOSS.IN era sempre um enorme sucesso. O Atul deixou-nos a 3 de Junho após perder uma batalha contra o cancro. Que a sua alma descanse em paz. Estamos gratos pelas suas contribuições para um mundo melhor.

Estas versões estão todas traduzidas para 54 línguas; esperamos que sejam adicionadas mais línguas nas versões com correcções de erros subsequentes do KDE. A equipa de documentação actualizou 91 manuais de aplicações para esta versão.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="A Área de Trabalho Plasma do KDE 4.11" width="64" height="64" /> A Área de Trabalho Plasma 4.11 Continua a Afinar a Experiência do Utilizador</a>

Destinando-se a uma manutenção a longo prazo, a Área de Trabalho do Plasma oferece mais melhorias para as funcionalidades básicas com uma barra de tarefas mais suave, um item de bateria mais inteligente e uma mesa de mistura de som melhorada. A introdução do KScreen traz um tratamento inteligente de vários monitores ao ambiente de trabalho, assim como algumas melhorias de performance em grande escala, combinadas com alguns ajustes de usabilidade, para que possa ter uma experiência de utilização global mais agradável.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="As Aplicações do KDE 4.11"/> As Aplicações do KDE 4.11 Dão um Enorme Passo em Frente na Gestão de Informações Pessoais e Tiveram Melhorias em Todo o Lado</a>

Esta versão marca grandes melhorias na plataforma do KDE PIM, ganhando uma performance muito melhor e muitas funcionalidades novas. O Kate melhora a produtividade dos programadores de Python e JavaScript com novos 'plugins', o Dolphin ficou mais rápido e as aplicações educativas tiveram muitas novas funcionalidades.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="A Plataforma de Desenvolvimento do KDE 4.11"/> A Plataforma do KDE 4.11 Oferece uma Melhor Performance</a>

Esta versão da Plataforma do KDE 4.11 continua a focar-se na estabilidade. As funcionalidades novas estão a ser implementadas na nossa futura versão 5.0 da plataforma, mas para a versão estável continuámos a esforçar a optimização da nossa plataforma Nepomuk.

<br />
Ao actualizar, observe por favor as <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>notas de lançamento</a>.<br />

## Espalhe a Palavra e Veja o Que se Passa: Marque como &quot;KDE&quot;

O KDE encoraja as pessoas a passarem palavra na Web social. Envie histórias para páginas de notícias, use canais como o delicious, digg, reddit, twitter, identi.ca. Envie imagens para serviços como o Facebook, Flickr, ipernity e Picasa, publicando-as nos grupos apropriados. Crie capturas do ecrã por vídeo e envie-as para o YouTube, Blip.tv e Vimeo. Por favor, marque as publicações e itens enviados como &quot;KDE&quot;. Isto torna-as muito fáceis de descobrir, permitindo à equipa de Promoção do KDE uma forma de analisar a cobertura do lançamento da versão 4.11 das aplicações do KDE.

## Festas de Lançamento

Como sempre, os membros da comunidade do KDE organizam festas de lançamento em todo o mundo. Muitas já foram agendadas e virão outras mais. Descubra <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>aqui uma lista das festas</a>. Todos são bem-vindos para se juntarem! Existirá uma combinação de companhia interessante, conversas inspiradoras, assim como comida e bebida. É uma óptima hipótese de aprender mais o que se passa no KDE, para se envolver, ou simplesmente para conhecer outros utilizadores e colaboradores.

Encorajamos as pessoas a organizarem as suas próprias festas. São divertidas de organizar e estão abertas a todos! Veja algumas <a href='http://community.kde.org/Promo/Events/Release_Parties'>dicas para organizar uma festa</a>.

## Acerca destes anúncios de lançamento

Estes anúncios de lançamento foram preparados por Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin e outros membros da Equipa de Promoção do KDE e pela comunidade abrangente do KDE. Eles cobrem alguns destaques das várias alterações feitas às aplicações do KDE ao longo dos últimos seis meses.

#### Apoiar o KDE

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

O novo <a href='http://jointhegame.kde.org/'>programa de Membros de Suporte</a> está agora aberto. Por 25&euro; por trimestre poderá garantir que a comunidade internacional do KDE continua a crescer e a desenvolver Software Livre de classe mundial.
