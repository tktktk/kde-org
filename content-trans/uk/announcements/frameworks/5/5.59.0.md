---
aliases:
- ../../kde-frameworks-5.59.0
date: 2019-06-08
layout: framework
libCount: 70
---
### Baloo

- Усунено спроби індексувати дампи баз даних SQL
- Зі списків індексування усунено файли .gcode та файли віртуальних машин

### BluezQt

- До обробника/породжувача XML DBus додано програмний інтерфейс Bluez

### Піктограми Breeze

- Додано gcompris-qt
- Піктограму falkon зроблено справжнім зображенням SVG
- Додано пропущені піктограми для програм, — маємо їх переробити: https://bugs.kde.org/show_bug.cgi?id=407527
- Додано піктограму kfourinline зі сховища програми (потребує оновлення)
- Додано піктограму kigo: https://bugs.kde.org/show_bug.cgi?id=407527
- Додано піктограму kwave з kwave (має бути перероблено у стилі breeze)
- Створено символічні посилання arrow-*-double на go-*-skip, додано go-*-skip розміром у 24 пк
- Змінено стилі піктограм пристроїв input-*, додано піктограми розміром 16 пк
- Додано темну версію нової піктограми Knights
- Створено нову піктограму Knights на основі піктограми з Anjuta (виправлено ваду 407527)
- Додано піктограми програм, яких не вистачало у breeze. Ці піктограми слід оновити у стилі breeze, але поточні версії потрібні для kde.org/applications вже зараз.
- Додано піктограму kxstitch з kde:kxstitch (слід оновити)
- Усунено загальне використання замінників
- Забезпечено перевірку ScaledDirectories

### Додаткові модулі CMake

- Створено особливий каталог для файла категорій журналювання для Qt
- Усунено вмикання QT_STRICT_ITERATORS у Windows

### Інтеграція бібліотек

- Забезпечено пошук і у застарілих місцях
- Реалізовано пошук файлів knsrc у нових місцях

### KArchive

- Додано перевірку читання та позиціювання у KCompressionDevice
- KCompressionDevice: вилучено bIgnoreData
- KAr: усунено читання за межами діапазону даних (при некоректних вхідних даних) портуванням на QByteArray
- KAr: виправлено обробку довгих назв файлів, якщо використано Qt 5.10
- KAr: враховано, що права доступу визначаються як вісімкові значення, а не як десяткові
- KAr::openArchive: реалізовано перевірку того, чи не є ar_longnamesIndex &lt; 0
- KAr::openArchive: усунено некоректний доступ до пам'яті для пошкоджених файлів
- KAr::openArchive: реалізовано захист від переповнення буфера купи для пошкоджених файлів
- KTar::KTarPrivate::readLonglink: усунено аварійне завершення роботи при обробці помилково форматованих файлів

### KAuth

- Усунено жорстке визначення каталогу встановлення для правил DBus

### KConfigWidgets

- Реалізовано використання валюти локалі для піктограми фінансової підтримки

### KCoreAddons

- Виправлено компіляцію прив'язок до python (виправлено ваду 407306)
- Додано GetProcessList для отримання списку поточних активних процесів

### KDeclarative

- Виправлено файли qmldir

### Підтримка KDELibs 4

- Вилучено QApplication::setColorSpec (порожній метод)

### KFileMetaData

- Реалізовано показ 3 значимих цифр при показі дійсних чисел із подвійною точністю (виправлено ваду 343273)

### KIO

- Реалізовано обробку байтів замість символів
- Виправлено помилку, пов'язану із тим, що виконувані файли kioslave ніколи не завершували роботу, якщо встановлено KDE_FORK_SLAVES
- Виправлено стільничне посилання на файл або каталог (виправлено ваду 357171)
- Реалізовано перевірку поточного фільтра до встановлення нового (виправлено ваду 407642)
- [kioslave/file] Додано кодек для застарілих назв файлів (виправлено ваду 165044)
- Реалізовано використання QSysInfo для отримання подробиць щодо системи
- До типового списку «Місця» додано пункт «Документи»
- kioslave: реалізовано збереження argv[0] з метою виправлення applicationDirPath() у системах, які не є системами Linux
- Уможливлено скидання одного файла або однієї теки на KDirOperator (виправлено ваду 45154)
- Реалізовано обрізання довгих назв файлів до створення посилання (виправлено ваду 342247)

### Kirigami

- [ActionTextField] Підказки QML зроблено однорідними
- Реалізовано врахування висоти пунктів, які повинні мати верхню фаску (виправлено ваду 405614)
- Швидкодія: реалізовано стискання змін у кольорі без QTimer
- [FormLayout] Реалізовано використання правильних інтервалів згори і знизу для роздільника (виправлено ваду 405614)
- ScrollablePage: забезпечено отримання фокуса для панелей із гортанням, якщо встановлюється фокус (виправлено ваду 389510)
- Поліпшено використання панелі інструментів лише за допомогою клавіатури (виправлено ваду 403711)
- Смітник зроблено FocusScope

### KNotification

- Реалізовано обробку програми, які встановлюють властивість desktopFileName із суфіксом назви файла

### KService

- Виправлено оцінку (hash != 0), коли файл вилучається іншим процесом
- Виправлено іншу оцінку, якщо файли зникає: ASSERT: "ctime != 0"

### KTextEditor

- Усунено вилучення усього попереднього рядка клавішею backspace у позиції 0 (виправлено ваду 408016)
- Використано перевірку перезапису природного діалогового вікна
- Додано пункт дії зі скидання розміру шрифту
- Показувати позначку статичного перенесення рядків завжди, якщо таку визначено
- Забезпечено показ позначки початку і кінця підсвіченого діапазону після розгортання
- Усунено підсвічування при збереженні певних файлів (виправлено ваду 407763)
- Автоматичні відступи: реалізовано використання std::vector замість QList
- Виправлення: реалізовано використання типового режиму відступів для нових файлів (виправлено ваду 375502)
- Вилучено дублювання надання змінній значення
- Реалізовано враховування параметра автоматичного дописування дужок при перевірці балансу дужок
- Поліпшено перевірку некоректності символів при завантаженні даних (виправлено ваду 406571)
- Нові меню для підсвічування синтаксичних конструкцій на смужці стану
- Усунено нескінченну циклічну обробку під час дії «Перемкнути вузли всередині»

### KWayland

- Уможливлено надсилання засобами композиції дискретних значень на вісях (виправлено ваду 404152)
- Реалізовано set_window_geometry
- Реалізовано wl_surface::damage_buffer

### KWidgetsAddons

- KNewPasswordDialog: додано кінцеві крапки до віджетів повідомлень

### NetworkManagerQt

- Усунено отримання статистики щодо пристрою при побудові даних

### Бібліотеки Plasma

- Реалізовано використання у світлій та темній темах Breeze більшої кількості кольорів системи
- Експортовано стовпчик критерію упорядковування SortFilterModel до QML
- plasmacore: виправлено qmldir — ToolTip.qml більше не є частиною модуля
- Реалізовано сигнал availableScreenRectChanged для усіх аплетів
- Реалізовано використання простого configure_file для створення файлів plasmacomponents3
- Оновлено *.qmltypes до поточного програмного інтерфейсу модулів QML
- FrameSvg: реалізовано очищення кешу масок при clearCache()
- FrameSvg: уможливлено обробку hasElementPrefix() префіксів, які завершуються на «-»
- FrameSvgPrivate::generateBackground: реалізовано створення тла, навіть якщо reqp != p
- FrameSvgItem: реалізовано надсилання сигналу maskChanged і з geometryChanged()
- FrameSvg: усунено аварійне завершення роботи, якщо mask() викликано до створення рамки
- FrameSvgItem: реалізовано надсилання сигналів maskChanged завжди від doUpdate()
- Документація із програмного інтерфейсу: зауваження щодо FrameSvg::prefix()/actualPrefix() і завершального «-»
- Документація із програмного інтерфейсу: додано посилання на версії Plasma5 у techbase, якщо такі доступні
- FrameSvg: ліві і праві або верхні і нижні рамки не обов'язково мають бути однакового співвідношення між висотою і шириною

### Purpose

- [JobDialog] Реалізовано скасування сигналу, коли вікно закрито користувачем
- Реалізовано звітування про скасовування налаштовування як про завершення з помилкою (виправлено ваду 407356)

### QQC2StyleBridge

- Вилучено анімацію DefaultListItemBackground і MenuItem
- [Стиіль повзунків QQC2] Виправлено помилкове розташування елемента керування, коли початковим значенням є 1 (виправлено ваду 405471)
- ScrollBar: уможливлено роботу у форматі горизонтальної смужки гортання (виправлено ваду 390351)

### Solid

- Перероблено спосіб, у який збираються і реєструються модулі обробки даних пристроїв
- [Fstab] Використано піктограму folder-decrypted для шифрування точок монтування fuse

### Підсвічування синтаксису

- YAML: реалізовано обробку коментарів лише після пробілів, інші поліпшення та виправлення (виправлено ваду 407060)
- Markdown: реалізовано використання includeAttrib у блоках коду
- Виправлено підсвічування «\0» у режимі Сі
- Tcsh: виправлено оператори та ключові слова
- Додано визначення синтаксичних конструкцій для мови CIL (Common Intermediate Language)
- SyntaxHighlighter: виправлено колір тла для тексту без спеціального підсвічування (виправлено ваду 406816)
- Додано приклад програми для друку підсвіченого фрагмента тексту до PDF
- Markdown: для списків використано контрастніший колір (виправлено ваду 405824)
- Вилучено суфікс .conf з підсвічування файлів INI для визначення засобу підсвічування на основі типу MIME (виправлено ваду 400290)
- Perl: виправлено обробку оператора // (виправлено ваду 407327)
- Виправлено регістр символів у типах UInt* для підсвічування коду мовою Julia (виправлено ваду 407611)

### Відомості щодо безпеки

Випущений код підписано за допомогою GPG з використанням такого ключа: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Відбиток основного ключа: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
