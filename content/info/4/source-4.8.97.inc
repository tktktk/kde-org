<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">SHA1 Sum</th>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/analitza-4.8.97.tar.xz">analitza-4.8.97</a></td>
   <td align="right">139kB</td>
   <td><tt>8a91b68ed179c0f36c40aaddb54fa5b90bd6684f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/ark-4.8.97.tar.xz">ark-4.8.97</a></td>
   <td align="right">150kB</td>
   <td><tt>57233fba7f55996d4663fda23d926ea4a8322f32</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/audiocd-kio-4.8.97.tar.xz">audiocd-kio-4.8.97</a></td>
   <td align="right">53kB</td>
   <td><tt>fc4dd691c5f51c79f019ab7f1a0f60aa56da2c6f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/blinken-4.8.97.tar.xz">blinken-4.8.97</a></td>
   <td align="right">551kB</td>
   <td><tt>e7af7ad460cf160590c0401b865c49042bfd1bf8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/cantor-4.8.97.tar.xz">cantor-4.8.97</a></td>
   <td align="right">258kB</td>
   <td><tt>8e5ef8d42a71c3e7f8cbeae69de707e3580cb40c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/dragon-4.8.97.tar.xz">dragon-4.8.97</a></td>
   <td align="right">382kB</td>
   <td><tt>f1d2eab7228e7cd31d9dd5a4f26ff86965bcac86</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/ffmpegthumbs-4.8.97.tar.xz">ffmpegthumbs-4.8.97</a></td>
   <td align="right">19kB</td>
   <td><tt>fbbafd3dd018f13f8cf61466312a4db6a229cf22</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/filelight-4.8.97.tar.xz">filelight-4.8.97</a></td>
   <td align="right">287kB</td>
   <td><tt>750f96517538f7d299b4dc0dea903e63742e7c49</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/gwenview-4.8.97.tar.xz">gwenview-4.8.97</a></td>
   <td align="right">1.9MB</td>
   <td><tt>5368a49cfe73f4f7cd650f6aac838f4640d9a563</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/jovie-4.8.97.tar.xz">jovie-4.8.97</a></td>
   <td align="right">357kB</td>
   <td><tt>ac2759c78ba1ed0a10bf71e65a3b12fea3fb8cf6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/juk-4.8.97.tar.xz">juk-4.8.97</a></td>
   <td align="right">430kB</td>
   <td><tt>71b418afd6d19188d87b8c6d70d3e88cf69dc7c8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kaccessible-4.8.97.tar.xz">kaccessible-4.8.97</a></td>
   <td align="right">20kB</td>
   <td><tt>ad70cb9c3d11a4cc3881d689ec7329585a725e60</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kactivities-4.8.97.tar.xz">kactivities-4.8.97</a></td>
   <td align="right">81kB</td>
   <td><tt>46ceae20db381c0e4de77f46246ff0db6fdd43a4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kalgebra-4.8.97.tar.xz">kalgebra-4.8.97</a></td>
   <td align="right">428kB</td>
   <td><tt>af8f692dcc58992e464acf39882eb069b615f1df</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kalzium-4.8.97.tar.xz">kalzium-4.8.97</a></td>
   <td align="right">3.5MB</td>
   <td><tt>c8e8989eb4bbf943d1183a55a45df074d5b68f17</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kamera-4.8.97.tar.xz">kamera-4.8.97</a></td>
   <td align="right">35kB</td>
   <td><tt>f7896c41672035d4750af6c5fc57a548888b40e1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kanagram-4.8.97.tar.xz">kanagram-4.8.97</a></td>
   <td align="right">1.0MB</td>
   <td><tt>4ae7de38749d23cb8aebbd351d0205ecd0315045</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kate-4.8.97.tar.xz">kate-4.8.97</a></td>
   <td align="right">2.0MB</td>
   <td><tt>2f5481aff6802057ce2b46c6d421c8f5657926b3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kbruch-4.8.97.tar.xz">kbruch-4.8.97</a></td>
   <td align="right">884kB</td>
   <td><tt>4970be77ae5a9d059f5bf6aa94dbc120cf2312a5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kcalc-4.8.97.tar.xz">kcalc-4.8.97</a></td>
   <td align="right">85kB</td>
   <td><tt>d9d83233755e705aa385ec5a39abfafb80922001</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kcharselect-4.8.97.tar.xz">kcharselect-4.8.97</a></td>
   <td align="right">83kB</td>
   <td><tt>58c4877499e1233076cbca884fdfdd8a04d37ca2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kcolorchooser-4.8.97.tar.xz">kcolorchooser-4.8.97</a></td>
   <td align="right">4kB</td>
   <td><tt>7bf1ec5487e79f38a95a5d3752a756360be1cacb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdeadmin-4.8.97.tar.xz">kdeadmin-4.8.97</a></td>
   <td align="right">1.0MB</td>
   <td><tt>f118b707c2f1d0d64ee321992f14da9e3b5518f9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdeartwork-4.8.97.tar.xz">kdeartwork-4.8.97</a></td>
   <td align="right">131MB</td>
   <td><tt>b8f2d992b5aa8b54300df27d180f33db849cf708</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-baseapps-4.8.97.tar.xz">kde-baseapps-4.8.97</a></td>
   <td align="right">2.3MB</td>
   <td><tt>acaff6f33648b3c39d9af402d24c9197e8b179e4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-base-artwork-4.8.97.tar.xz">kde-base-artwork-4.8.97</a></td>
   <td align="right">15MB</td>
   <td><tt>f219c91586ff9b9c8540512588f553b6660ca282</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdegames-4.8.97.tar.xz">kdegames-4.8.97</a></td>
   <td align="right">53MB</td>
   <td><tt>81a28078640a69e36412722a43be03ebb6224ea1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdegraphics-mobipocket-4.8.97.tar.xz">kdegraphics-mobipocket-4.8.97</a></td>
   <td align="right">19kB</td>
   <td><tt>31ecdd38e977fa2b222646ce4fbdf5975eece5f8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdegraphics-strigi-analyzer-4.8.97.tar.xz">kdegraphics-strigi-analyzer-4.8.97</a></td>
   <td align="right">39kB</td>
   <td><tt>cbd7f9de4b00060af7774f817a5dc0c2312a3953</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdegraphics-thumbnailers-4.8.97.tar.xz">kdegraphics-thumbnailers-4.8.97</a></td>
   <td align="right">40kB</td>
   <td><tt>c5dc4b4a658fd268f9b0ec0895ccf4b2100cac3e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdelibs-4.8.97.tar.xz">kdelibs-4.8.97</a></td>
   <td align="right">11MB</td>
   <td><tt>adbbc70a2e0d243f3875469cafe22b63b0625a7e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdenetwork-4.8.97.tar.xz">kdenetwork-4.8.97</a></td>
   <td align="right">8.5MB</td>
   <td><tt>e273b9cb322f9f978a48e687637dbc03601ac71e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdepim-4.8.97.tar.xz">kdepim-4.8.97</a></td>
   <td align="right">13MB</td>
   <td><tt>9bd4594ec3151f3cfb6a45d6163405bde4d8b178</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdepimlibs-4.8.97.tar.xz">kdepimlibs-4.8.97</a></td>
   <td align="right">2.6MB</td>
   <td><tt>5d8cfed6f902b101a78372ff1b4220dc21a65ed0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdepim-runtime-4.8.97.tar.xz">kdepim-runtime-4.8.97</a></td>
   <td align="right">1.0MB</td>
   <td><tt>c31f09b734e558f0c8e279270058ec0cc62fed3c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdeplasma-addons-4.8.97.tar.xz">kdeplasma-addons-4.8.97</a></td>
   <td align="right">1.6MB</td>
   <td><tt>b56299d2197c3b6007454644361671cd7157ba30</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-runtime-4.8.97.tar.xz">kde-runtime-4.8.97</a></td>
   <td align="right">6.0MB</td>
   <td><tt>5eff74a293c9adf8e76fb45f5a085726c083c179</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdesdk-4.8.97.tar.xz">kdesdk-4.8.97</a></td>
   <td align="right">4.8MB</td>
   <td><tt>8711677da32d29a38ed96bd9066d5521ea18c37d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdetoys-4.8.97.tar.xz">kdetoys-4.8.97</a></td>
   <td align="right">369kB</td>
   <td><tt>2ca542d74ff1ef7317a2bc43712fd323efc7fc37</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-wallpapers-4.8.97.tar.xz">kde-wallpapers-4.8.97</a></td>
   <td align="right">73MB</td>
   <td><tt>41b94b1d24a01bb3ba7762a03c2c0e7bdbfa2df6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdewebdev-4.8.97.tar.xz">kdewebdev-4.8.97</a></td>
   <td align="right">2.4MB</td>
   <td><tt>9c1f13cc5dd0c089509e04756bc72eb5aed437fe</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-workspace-4.8.97.tar.xz">kde-workspace-4.8.97</a></td>
   <td align="right">21MB</td>
   <td><tt>c02cab54df42b1452c183a1234405cfd6df18be7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kdf-4.8.97.tar.xz">kdf-4.8.97</a></td>
   <td align="right">151kB</td>
   <td><tt>f84341b0a6400570802960fffd7bf618282e2604</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kfloppy-4.8.97.tar.xz">kfloppy-4.8.97</a></td>
   <td align="right">59kB</td>
   <td><tt>a21e3fc298d7015be965208bec5667ad17a54667</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kgamma-4.8.97.tar.xz">kgamma-4.8.97</a></td>
   <td align="right">25kB</td>
   <td><tt>baa1b40d4e9c89946879697bd63111e94349295d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kgeography-4.8.97.tar.xz">kgeography-4.8.97</a></td>
   <td align="right">6.4MB</td>
   <td><tt>77042de18753c2663de65e224337a91591a4b98e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kgpg-4.8.97.tar.xz">kgpg-4.8.97</a></td>
   <td align="right">790kB</td>
   <td><tt>871381198bdf229ce4e2c4f619ba81babc97a415</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/khangman-4.8.97.tar.xz">khangman-4.8.97</a></td>
   <td align="right">3.5MB</td>
   <td><tt>e242517e4f57e24086161cb1d4428b9852cbe8d6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kig-4.8.97.tar.xz">kig-4.8.97</a></td>
   <td align="right">1.4MB</td>
   <td><tt>c365608497df374d7ea0f31271cead02ffe2de89</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kimono-4.8.97.tar.xz">kimono-4.8.97</a></td>
   <td align="right">915kB</td>
   <td><tt>e2f5d0a97364fc74844bfb5c443008502f82b9d0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kiten-4.8.97.tar.xz">kiten-4.8.97</a></td>
   <td align="right">11MB</td>
   <td><tt>2b7061fd8f8c88acb6c0a9e05a514083165418e1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/klettres-4.8.97.tar.xz">klettres-4.8.97</a></td>
   <td align="right">2.5MB</td>
   <td><tt>d2af187dde78d992e55d0bc9bab31e9fee9bc300</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kmag-4.8.97.tar.xz">kmag-4.8.97</a></td>
   <td align="right">89kB</td>
   <td><tt>fc25e930cdd1c9b03553a2fb6923ead410793c6a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kmix-4.8.97.tar.xz">kmix-4.8.97</a></td>
   <td align="right">293kB</td>
   <td><tt>1da4c0ce895ed39dd25e6da1c6c85671536a8391</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kmousetool-4.8.97.tar.xz">kmousetool-4.8.97</a></td>
   <td align="right">41kB</td>
   <td><tt>c5f413aeb0ff01d574e66bc310c8b074dd952cf5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kmouth-4.8.97.tar.xz">kmouth-4.8.97</a></td>
   <td align="right">311kB</td>
   <td><tt>1d553d6c2a0fac255a21616b8b0fe87348862adf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kmplot-4.8.97.tar.xz">kmplot-4.8.97</a></td>
   <td align="right">661kB</td>
   <td><tt>a13fcad49de1fb6583d86aa4d17d2330112f0c22</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kolourpaint-4.8.97.tar.xz">kolourpaint-4.8.97</a></td>
   <td align="right">1.1MB</td>
   <td><tt>de14aab1473a886cdd77170e4d8db34413a4be1c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/konsole-4.8.97.tar.xz">konsole-4.8.97</a></td>
   <td align="right">424kB</td>
   <td><tt>f644fbe162d1f852396898f11ae52765da356ecb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/korundum-4.8.97.tar.xz">korundum-4.8.97</a></td>
   <td align="right">156kB</td>
   <td><tt>432655321eefb8b3635c03c55e02175163340131</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kremotecontrol-4.8.97.tar.xz">kremotecontrol-4.8.97</a></td>
   <td align="right">1.0MB</td>
   <td><tt>5892208920d3466eb29b7b91db768729dca45db2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kross-interpreters-4.8.97.tar.xz">kross-interpreters-4.8.97</a></td>
   <td align="right">127kB</td>
   <td><tt>ab041689b0e1386997f5dc062eb791c17ed5987b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kruler-4.8.97.tar.xz">kruler-4.8.97</a></td>
   <td align="right">130kB</td>
   <td><tt>88f8b63297466fd719c195df23d55363ba272db5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/ksaneplugin-4.8.97.tar.xz">ksaneplugin-4.8.97</a></td>
   <td align="right">12kB</td>
   <td><tt>919e9b34de57c13c67c919efb5b8dd95adf6cbac</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kscd-4.8.97.tar.xz">kscd-4.8.97</a></td>
   <td align="right">91kB</td>
   <td><tt>7ce062b170ff3e3e299b24ca618ac11243afd05a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/ksnapshot-4.8.97.tar.xz">ksnapshot-4.8.97</a></td>
   <td align="right">256kB</td>
   <td><tt>5416d6a1415b41cd71c302ace7332ab22a048f1d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kstars-4.8.97.tar.xz">kstars-4.8.97</a></td>
   <td align="right">11MB</td>
   <td><tt>faf9b9701dadb2cd2458757c5f03861752eaadb5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/ktimer-4.8.97.tar.xz">ktimer-4.8.97</a></td>
   <td align="right">146kB</td>
   <td><tt>a4e2a19f5dcd342d039b8a5c95372f13a668640f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/ktouch-4.8.97.tar.xz">ktouch-4.8.97</a></td>
   <td align="right">1.6MB</td>
   <td><tt>fa545d6159f41e699b56f7a75afb980a781f169a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kturtle-4.8.97.tar.xz">kturtle-4.8.97</a></td>
   <td align="right">207kB</td>
   <td><tt>59b192b9fd31568d63d6192c038530fe009f303f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kwallet-4.8.97.tar.xz">kwallet-4.8.97</a></td>
   <td align="right">274kB</td>
   <td><tt>601d3fee7551767329aa65a833c22253b49c57f1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kwordquiz-4.8.97.tar.xz">kwordquiz-4.8.97</a></td>
   <td align="right">1.1MB</td>
   <td><tt>659448fd9ad9bd22aed74e42a297e62346d94d38</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/libkcddb-4.8.97.tar.xz">libkcddb-4.8.97</a></td>
   <td align="right">153kB</td>
   <td><tt>5ca224e3debf8d48358d32c69bce488753ba47c2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/libkcompactdisc-4.8.97.tar.xz">libkcompactdisc-4.8.97</a></td>
   <td align="right">74kB</td>
   <td><tt>9e5acb7c8017f3ed0698a633683ef0c30d258312</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/libkdcraw-4.8.97.tar.xz">libkdcraw-4.8.97</a></td>
   <td align="right">261kB</td>
   <td><tt>2e64db1b96b48e2f52baa1e66d8914b75640602c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/libkdeedu-4.8.97.tar.xz">libkdeedu-4.8.97</a></td>
   <td align="right">204kB</td>
   <td><tt>65e9a0054eac02b0232b647a82d9a4f72a0644a2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/libkexiv2-4.8.97.tar.xz">libkexiv2-4.8.97</a></td>
   <td align="right">129kB</td>
   <td><tt>6d93848eae11495ced6a93891b8f1e09310237ce</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/libkipi-4.8.97.tar.xz">libkipi-4.8.97</a></td>
   <td align="right">74kB</td>
   <td><tt>bce6a8783f36db958890f5b162441a7f550e1a6a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/libksane-4.8.97.tar.xz">libksane-4.8.97</a></td>
   <td align="right">78kB</td>
   <td><tt>221dd7e63335f23682e61ab6c3cde471ad06282d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/marble-4.8.97.tar.xz">marble-4.8.97</a></td>
   <td align="right">18MB</td>
   <td><tt>218cb42ccb8946ee1bb8f35b770e0f8b7c9b5ebb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/mplayerthumbs-4.8.97.tar.xz">mplayerthumbs-4.8.97</a></td>
   <td align="right">26kB</td>
   <td><tt>293f54e6edcca03859abfdc717139693455cd16e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/nepomuk-core-4.8.97.tar.xz">nepomuk-core-4.8.97</a></td>
   <td align="right">363kB</td>
   <td><tt>6d456fd2de9daa8b97c730346b7377b8df265e56</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/okular-4.8.97.tar.xz">okular-4.8.97</a></td>
   <td align="right">1.3MB</td>
   <td><tt>a31da767627783ee4d27e4a9bf2bacd893fcb467</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/oxygen-icons-4.8.97.tar.xz">oxygen-icons-4.8.97</a></td>
   <td align="right">264MB</td>
   <td><tt>f87a6f800cdb2a1948c382f3f421ed2fde1fe932</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/pairs-4.8.97.tar.xz">pairs-4.8.97</a></td>
   <td align="right">1.8MB</td>
   <td><tt>2247f515a1392507d7cf65e2f78ad002972b1e09</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/parley-4.8.97.tar.xz">parley-4.8.97</a></td>
   <td align="right">7.8MB</td>
   <td><tt>f8486ed259820621926c66b93dfd0d288b0dd05e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/perlkde-4.8.97.tar.xz">perlkde-4.8.97</a></td>
   <td align="right">39kB</td>
   <td><tt>e0577df7a98bb4c7e7c3be3cb124ff1553a76f85</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/perlqt-4.8.97.tar.xz">perlqt-4.8.97</a></td>
   <td align="right">1.7MB</td>
   <td><tt>387afb6746d55fa4d63ee5885e91cd12b6161f5e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/printer-applet-4.8.97.tar.xz">printer-applet-4.8.97</a></td>
   <td align="right">35kB</td>
   <td><tt>972af45987799c1f53ccf76e674f64f79c3acdc9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/pykde4-4.8.97.tar.xz">pykde4-4.8.97</a></td>
   <td align="right">1.8MB</td>
   <td><tt>734f97218bb867bd36ac9d25bac89c5d8b0b60cd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/qtruby-4.8.97.tar.xz">qtruby-4.8.97</a></td>
   <td align="right">517kB</td>
   <td><tt>9b553519fcc65348d2c2c87bb36fb2c74490f058</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/qyoto-4.8.97.tar.xz">qyoto-4.8.97</a></td>
   <td align="right">498kB</td>
   <td><tt>6c5699a81e5ed76a808e4ca08844cfa49e92df7f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/rocs-4.8.97.tar.xz">rocs-4.8.97</a></td>
   <td align="right">1.1MB</td>
   <td><tt>15daf5a0becd09f2de96e74753a632de905c09e7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/smokegen-4.8.97.tar.xz">smokegen-4.8.97</a></td>
   <td align="right">143kB</td>
   <td><tt>8348e0f3b7bb5acbb6646e6c08dbdf5d17fa32dc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/smokekde-4.8.97.tar.xz">smokekde-4.8.97</a></td>
   <td align="right">36kB</td>
   <td><tt>04961b5a81728393995bc40e31a777012774078f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/smokeqt-4.8.97.tar.xz">smokeqt-4.8.97</a></td>
   <td align="right">29kB</td>
   <td><tt>bec234f07c2f2c126bc73b163fc5f4052954d228</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/step-4.8.97.tar.xz">step-4.8.97</a></td>
   <td align="right">362kB</td>
   <td><tt>444eebae0e493fe1bea39530e432365fbd8f231c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/superkaramba-4.8.97.tar.xz">superkaramba-4.8.97</a></td>
   <td align="right">374kB</td>
   <td><tt>dd5d13f9ee4708afd1202a0f24e0dd2d6d4161a7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/svgpart-4.8.97.tar.xz">svgpart-4.8.97</a></td>
   <td align="right">8kB</td>
   <td><tt>7a3f8d2b63c54487ffb7d47a2b62275a5530d2ae</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/sweeper-4.8.97.tar.xz">sweeper-4.8.97</a></td>
   <td align="right">82kB</td>
   <td><tt>2a6a8dd0301e6a95d02091a3021906af031630bd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-ar-4.8.97.tar.xz">kde-l10n-ar-4.8.97</a></td>
   <td align="right">3.2MB</td>
   <td><tt>5f9d6bd7c6124211bc68d3d07984dd292e053cb1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-bg-4.8.97.tar.xz">kde-l10n-bg-4.8.97</a></td>
   <td align="right">1.9MB</td>
   <td><tt>930bcdea966b1e14194b861c05319688115c9b3b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-bs-4.8.97.tar.xz">kde-l10n-bs-4.8.97</a></td>
   <td align="right">2.2MB</td>
   <td><tt>ff93360aae6613a9934208b62dfd2468be88cc60</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-ca-4.8.97.tar.xz">kde-l10n-ca-4.8.97</a></td>
   <td align="right">10MB</td>
   <td><tt>990c7f9fc5faf76cf6c984a636d6b8b4879e539e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-ca@valencia-4.8.97.tar.xz">kde-l10n-ca@valencia-4.8.97</a></td>
   <td align="right">2.1MB</td>
   <td><tt>bf9d17d253226026007b5871054b3d5b2a1049fa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-cs-4.8.97.tar.xz">kde-l10n-cs-4.8.97</a></td>
   <td align="right">3.0MB</td>
   <td><tt>b17a904a6ce6b4e86b4b67a2fa02c5e64781a1ca</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-da-4.8.97.tar.xz">kde-l10n-da-4.8.97</a></td>
   <td align="right">12MB</td>
   <td><tt>51eea63b3f2ad501c1495f356eb057d08a803aaf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-de-4.8.97.tar.xz">kde-l10n-de-4.8.97</a></td>
   <td align="right">36MB</td>
   <td><tt>0547731904f8c84ee135e9b7d553fb065db2842a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-el-4.8.97.tar.xz">kde-l10n-el-4.8.97</a></td>
   <td align="right">4.5MB</td>
   <td><tt>e94d6ed7e366171b3239ea921c5c8a726507a19d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-en_GB-4.8.97.tar.xz">kde-l10n-en_GB-4.8.97</a></td>
   <td align="right">3.0MB</td>
   <td><tt>8231320ee198b11b5b0685a803f83ad973c67a67</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-es-4.8.97.tar.xz">kde-l10n-es-4.8.97</a></td>
   <td align="right">22MB</td>
   <td><tt>1902c4778e8c6d4c5737df3f72d996fc32bcb5e4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-et-4.8.97.tar.xz">kde-l10n-et-4.8.97</a></td>
   <td align="right">7.1MB</td>
   <td><tt>8d2d8b8adc6a01aa2091c6971881da38b70cc8d1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-eu-4.8.97.tar.xz">kde-l10n-eu-4.8.97</a></td>
   <td align="right">2.0MB</td>
   <td><tt>acbd7c77c0ae27f39d2f525e1878ca92659cadd6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-fa-4.8.97.tar.xz">kde-l10n-fa-4.8.97</a></td>
   <td align="right">1.8MB</td>
   <td><tt>cdaef09052c217f3354b04e566a83e50e3983b36</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-fi-4.8.97.tar.xz">kde-l10n-fi-4.8.97</a></td>
   <td align="right">2.5MB</td>
   <td><tt>b6e10e4c78d2fe63b2b9bbe4102f414b490ce930</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-fr-4.8.97.tar.xz">kde-l10n-fr-4.8.97</a></td>
   <td align="right">35MB</td>
   <td><tt>71aa24f279c4dc24e21d5ced113e3b734e48f6fe</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-ga-4.8.97.tar.xz">kde-l10n-ga-4.8.97</a></td>
   <td align="right">2.7MB</td>
   <td><tt>1c2132e56367260d95e3200067c545c54354f3dc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-gl-4.8.97.tar.xz">kde-l10n-gl-4.8.97</a></td>
   <td align="right">4.0MB</td>
   <td><tt>55e66bc2658d1b608824388259a4f5d17b4e9656</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-he-4.8.97.tar.xz">kde-l10n-he-4.8.97</a></td>
   <td align="right">2.0MB</td>
   <td><tt>ceae3f33542b91b77eb57db60d22e0fd7288a169</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-hr-4.8.97.tar.xz">kde-l10n-hr-4.8.97</a></td>
   <td align="right">1.9MB</td>
   <td><tt>99a7be5cd50a14074a7d6096d96b06b99ec5a9ea</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-hu-4.8.97.tar.xz">kde-l10n-hu-4.8.97</a></td>
   <td align="right">4.0MB</td>
   <td><tt>5f41f1a413a1622b25c9371e0e34d17344c3b59b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-ia-4.8.97.tar.xz">kde-l10n-ia-4.8.97</a></td>
   <td align="right">1.2MB</td>
   <td><tt>f1ac06907e54b89c8e668f91829e0894a76924fc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-id-4.8.97.tar.xz">kde-l10n-id-4.8.97</a></td>
   <td align="right">502kB</td>
   <td><tt>f13fecb81382fa4e77eff2b80a5fdca6ae796936</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-is-4.8.97.tar.xz">kde-l10n-is-4.8.97</a></td>
   <td align="right">1.8MB</td>
   <td><tt>7bd5a214de9f60e96a3fbd7f0b4dc218a1b499ca</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-it-4.8.97.tar.xz">kde-l10n-it-4.8.97</a></td>
   <td align="right">9.9MB</td>
   <td><tt>9dd3e9884b0753f834f99a9f684a918be48cea71</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-ja-4.8.97.tar.xz">kde-l10n-ja-4.8.97</a></td>
   <td align="right">2.2MB</td>
   <td><tt>5afbdadefca66e7373a2530f402a5ff4fa524f04</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-kk-4.8.97.tar.xz">kde-l10n-kk-4.8.97</a></td>
   <td align="right">2.4MB</td>
   <td><tt>dc70bf397c64be658f7f5cab5a182ad47146b4da</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-km-4.8.97.tar.xz">kde-l10n-km-4.8.97</a></td>
   <td align="right">2.1MB</td>
   <td><tt>7410d21096aa07193f94068f2e18830b7a7c0267</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-ko-4.8.97.tar.xz">kde-l10n-ko-4.8.97</a></td>
   <td align="right">1.7MB</td>
   <td><tt>4c42fde963c23cc44659978b884d2e3345becff5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-lt-4.8.97.tar.xz">kde-l10n-lt-4.8.97</a></td>
   <td align="right">10MB</td>
   <td><tt>0dbaf2c50de380b8dc7e63bac1994279a63808a9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-lv-4.8.97.tar.xz">kde-l10n-lv-4.8.97</a></td>
   <td align="right">2.1MB</td>
   <td><tt>842095c86eec7a32157c4930569d2d2a7f9a10ae</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-nb-4.8.97.tar.xz">kde-l10n-nb-4.8.97</a></td>
   <td align="right">2.3MB</td>
   <td><tt>ff13ef1f7b9528c6a83470e2062b2213794f4197</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-nds-4.8.97.tar.xz">kde-l10n-nds-4.8.97</a></td>
   <td align="right">3.0MB</td>
   <td><tt>e3ce9d912795bb1609d7b6ffba91ad91d0a739e5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-nl-4.8.97.tar.xz">kde-l10n-nl-4.8.97</a></td>
   <td align="right">15MB</td>
   <td><tt>bbee4d78994f5b56b79c4e8cab7009be3c902e33</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-nn-4.8.97.tar.xz">kde-l10n-nn-4.8.97</a></td>
   <td align="right">1.9MB</td>
   <td><tt>f718ef36c417b3818bf8de5bc6dad6ffe403784c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-pa-4.8.97.tar.xz">kde-l10n-pa-4.8.97</a></td>
   <td align="right">1.8MB</td>
   <td><tt>626cd1d5aaad712d66cc47fc78b5a27d8ea6eae9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-pl-4.8.97.tar.xz">kde-l10n-pl-4.8.97</a></td>
   <td align="right">16MB</td>
   <td><tt>f2ea009068834d4426ed24af88172bc73ee9b46f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-pt-4.8.97.tar.xz">kde-l10n-pt-4.8.97</a></td>
   <td align="right">5.6MB</td>
   <td><tt>c2d6f8f2d2e0b3d3d60f54426129c7a312cdbfaa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-pt_BR-4.8.97.tar.xz">kde-l10n-pt_BR-4.8.97</a></td>
   <td align="right">25MB</td>
   <td><tt>829682e621b707a358c13a0f46be2e087bd4e3b4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-ro-4.8.97.tar.xz">kde-l10n-ro-4.8.97</a></td>
   <td align="right">2.8MB</td>
   <td><tt>83ec2d6b7148612e5cd49f579205157c515c0c75</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-ru-4.8.97.tar.xz">kde-l10n-ru-4.8.97</a></td>
   <td align="right">22MB</td>
   <td><tt>079c6f81ef673b5404364b5b18bf47705ce4bbd3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-si-4.8.97.tar.xz">kde-l10n-si-4.8.97</a></td>
   <td align="right">1.0MB</td>
   <td><tt>351f6a888175e83ca3f92995b120eebad231a88b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-sk-4.8.97.tar.xz">kde-l10n-sk-4.8.97</a></td>
   <td align="right">4.1MB</td>
   <td><tt>de01ab33e3d3512ae5628edb3dcf4444dc7265ec</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-sl-4.8.97.tar.xz">kde-l10n-sl-4.8.97</a></td>
   <td align="right">3.2MB</td>
   <td><tt>5dbca1ba1b6bae7f89856ecc201a3ad9b7f502a3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-sr-4.8.97.tar.xz">kde-l10n-sr-4.8.97</a></td>
   <td align="right">5.7MB</td>
   <td><tt>11c90416ab8338e8411656076bd351af229850e5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-sv-4.8.97.tar.xz">kde-l10n-sv-4.8.97</a></td>
   <td align="right">16MB</td>
   <td><tt>de749da6c1b7f9617714e9fa2d68760cd4bd62ea</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-tg-4.8.97.tar.xz">kde-l10n-tg-4.8.97</a></td>
   <td align="right">1.9MB</td>
   <td><tt>92f10e9455220834fc3e71c6a93860b7c34f186d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-th-4.8.97.tar.xz">kde-l10n-th-4.8.97</a></td>
   <td align="right">1.6MB</td>
   <td><tt>eaa47abbf95e7e08b04b16588196d051177a7995</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-tr-4.8.97.tar.xz">kde-l10n-tr-4.8.97</a></td>
   <td align="right">3.8MB</td>
   <td><tt>6bab605c59d86b7f474f064521ffe443d8bdeec1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-ug-4.8.97.tar.xz">kde-l10n-ug-4.8.97</a></td>
   <td align="right">1.6MB</td>
   <td><tt>de54504c5668683d3e96a4afb6d60099d9009b14</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-uk-4.8.97.tar.xz">kde-l10n-uk-4.8.97</a></td>
   <td align="right">22MB</td>
   <td><tt>b144c591980cf6190a72e47736795201a2cfbd71</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-vi-4.8.97.tar.xz">kde-l10n-vi-4.8.97</a></td>
   <td align="right">911kB</td>
   <td><tt>9b1d3d025d2995fb00b3daeaa1e5793187202ba8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-wa-4.8.97.tar.xz">kde-l10n-wa-4.8.97</a></td>
   <td align="right">1.6MB</td>
   <td><tt>11acf1c16cd809c4a4f511a9afb19bf890810bbb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-zh_CN-4.8.97.tar.xz">kde-l10n-zh_CN-4.8.97</a></td>
   <td align="right">3.4MB</td>
   <td><tt>2659796a718229c9082150f21754721e7cfc2782</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.97/src/kde-l10n/kde-l10n-zh_TW-4.8.97.tar.xz">kde-l10n-zh_TW-4.8.97</a></td>
   <td align="right">2.4MB</td>
   <td><tt>fbdd15af039c706f7a56a85908aad6e7746ebc9a</tt></td>
</tr>

</table>
