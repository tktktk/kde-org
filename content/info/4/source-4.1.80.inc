<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdeaccessibility-4.1.80.tar.bz2">kdeaccessibility-4.1.80</a></td><td align="right">6,4MB</td><td><tt>40b4d8b4f9f40fb57f72d50d66798a1d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdeadmin-4.1.80.tar.bz2">kdeadmin-4.1.80</a></td><td align="right">1,9MB</td><td><tt>820fb72d34ee5106818785e9c761df8f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdeartwork-4.1.80.tar.bz2">kdeartwork-4.1.80</a></td><td align="right">41MB</td><td><tt>5016d1e9c01e9e090c9d83e5da3b47bc</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdebase-4.1.80.tar.bz2">kdebase-4.1.80</a></td><td align="right">4,3MB</td><td><tt>018039c8c51d92ace5365a8d6cd66fbf</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdebase-runtime-4.1.80.tar.bz2">kdebase-runtime-4.1.80</a></td><td align="right">60MB</td><td><tt>f32b010ddd2840c192413a170927d1d3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdebase-workspace-4.1.80.tar.bz2">kdebase-workspace-4.1.80</a></td><td align="right">47MB</td><td><tt>404a67541864085fbb6585afab7b95d2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdebindings-4.1.80.tar.bz2">kdebindings-4.1.80</a></td><td align="right">4,7MB</td><td><tt>4e690a2686d8c10f8a15eb2b4de2f729</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdeedu-4.1.80.tar.bz2">kdeedu-4.1.80</a></td><td align="right">58MB</td><td><tt>1532cc5ec3962ae462f2dc56db17fec9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdegames-4.1.80.tar.bz2">kdegames-4.1.80</a></td><td align="right">36MB</td><td><tt>58e1c8f00b291a00320437361c1a3dca</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdegraphics-4.1.80.tar.bz2">kdegraphics-4.1.80</a></td><td align="right">3,4MB</td><td><tt>17560a0c7df4e22eb12031050498961c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdelibs-4.1.80.tar.bz2">kdelibs-4.1.80</a></td><td align="right">11MB</td><td><tt>dc33ed72409eb2fecd760a66ea01bb0c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdemultimedia-4.1.80.tar.bz2">kdemultimedia-4.1.80</a></td><td align="right">1,5MB</td><td><tt>65e44c87a7f3cc0ffebe3998683f22ee</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdenetwork-4.1.80.tar.bz2">kdenetwork-4.1.80</a></td><td align="right">7,4MB</td><td><tt>5ec0fc0bde3239e15af8b487b70ac045</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdepim-4.1.80.tar.bz2">kdepim-4.1.80</a></td><td align="right">13MB</td><td><tt>dccd455696af39a8557b3a0c890a316d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdepimlibs-4.1.80.tar.bz2">kdepimlibs-4.1.80</a></td><td align="right">1,5MB</td><td><tt>8090760c6a43756160d1436676f1a305</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdeplasma-addons-4.1.80.tar.bz2">kdeplasma-addons-4.1.80</a></td><td align="right">4,4MB</td><td><tt>801c6177f66d89078387943938dffc60</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdesdk-4.1.80.tar.bz2">kdesdk-4.1.80</a></td><td align="right">5,2MB</td><td><tt>9d2bdeb07fb54287b1cc47ec0a7ecf69</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdetoys-4.1.80.tar.bz2">kdetoys-4.1.80</a></td><td align="right">1,3MB</td><td><tt>5354d6afcc6b228dd85bd40f83afd0d0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdeutils-4.1.80.tar.bz2">kdeutils-4.1.80</a></td><td align="right">2,1MB</td><td><tt>72663bbe1261ad6a2930a15031093b3e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdevelop-3.9.83.tar.bz2">kdevelop-3.9.83</a></td><td align="right">2,9MB</td><td><tt>ed66a2f24d8fdc7a86067ccb9880c27b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdevplatform-0.9.83.tar.bz2">kdevplatform-0.9.83</a></td><td align="right">1,1MB</td><td><tt>e4e155450574ebebbd49e4e6834ae542</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/kdewebdev-4.1.80.tar.bz2">kdewebdev-4.1.80</a></td><td align="right">3,3MB</td><td><tt>879d589dbc3fa4be861b0be0ea447d76</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.80/src/phonon-4.2.80.tar.bz2">phonon-4.2.80</a></td><td align="right">548KB</td><td><tt>885d63087d9861d1660888f10cadd1c5</tt></td></tr>
</table>
