<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdeaccessibility-4.1.3.tar.bz2">kdeaccessibility-4.1.3</a></td><td align="right">6,1MB</td><td><tt>85011b7ec7c8591f492f4dbf444408b3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdeadmin-4.1.3.tar.bz2">kdeadmin-4.1.3</a></td><td align="right">1,8MB</td><td><tt>a1b3c8e3c1dccbfff477dc6939f78d5e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdeartwork-4.1.3.tar.bz2">kdeartwork-4.1.3</a></td><td align="right">41MB</td><td><tt>131ffcd97e392053a65c9e97161508f9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdebase-4.1.3.tar.bz2">kdebase-4.1.3</a></td><td align="right">4,3MB</td><td><tt>cfeea9d5eb9004810b6e8a67ae937e6b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdebase-runtime-4.1.3.tar.bz2">kdebase-runtime-4.1.3</a></td><td align="right">50MB</td><td><tt>9fe84d0fa5fac149ba6cefeb004775ac</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdebase-workspace-4.1.3.tar.bz2">kdebase-workspace-4.1.3</a></td><td align="right">46MB</td><td><tt>42233b75ce73eaaacae61c0d5db8befa</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdebindings-4.1.3.tar.bz2">kdebindings-4.1.3</a></td><td align="right">4,6MB</td><td><tt>653539f01d018b746a5131beb11fc205</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdeedu-4.1.3.tar.bz2">kdeedu-4.1.3</a></td><td align="right">55MB</td><td><tt>8b513d6dde79119842eef059550c0123</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdegames-4.1.3.tar.bz2">kdegames-4.1.3</a></td><td align="right">31MB</td><td><tt>d6b2d87808696bf0a7364cca86f360bb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdegraphics-4.1.3.tar.bz2">kdegraphics-4.1.3</a></td><td align="right">3,3MB</td><td><tt>62513c5d92fae359705266eb2365c526</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdelibs-4.1.3.tar.bz2">kdelibs-4.1.3</a></td><td align="right">8,8MB</td><td><tt>562090d3c1d891081bc4a392e0b513ac</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdemultimedia-4.1.3.tar.bz2">kdemultimedia-4.1.3</a></td><td align="right">1,4MB</td><td><tt>5ccc4ff4b5a53c85db36e05bfdb2f01a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdenetwork-4.1.3.tar.bz2">kdenetwork-4.1.3</a></td><td align="right">7,1MB</td><td><tt>0a560e1c117049735542a89692480679</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdepim-4.1.3.tar.bz2">kdepim-4.1.3</a></td><td align="right">13MB</td><td><tt>655ff3c21c36a3378723bc2afb8cb434</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdepimlibs-4.1.3.tar.bz2">kdepimlibs-4.1.3</a></td><td align="right">1,9MB</td><td><tt>e6db27f7074d31cdad26fb705ece2825</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdeplasma-addons-4.1.3.tar.bz2">kdeplasma-addons-4.1.3</a></td><td align="right">3,9MB</td><td><tt>72d2d1c95b55fadbbce0e9cd201f1b80</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdesdk-4.1.3.tar.bz2">kdesdk-4.1.3</a></td><td align="right">4,8MB</td><td><tt>77787a59f74b1b5ed71b49b8643a75f9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdetoys-4.1.3.tar.bz2">kdetoys-4.1.3</a></td><td align="right">1,3MB</td><td><tt>ca5a74c6ae9f4c2f5e5b70e3c7711c78</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdeutils-4.1.3.tar.bz2">kdeutils-4.1.3</a></td><td align="right">2,2MB</td><td><tt>2b0eee36ad16f442c9fae85ac0bfc168</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.3/src/kdewebdev-4.1.3.tar.bz2">kdewebdev-4.1.3</a></td><td align="right">2,5MB</td><td><tt>9c283ffce644a8dfa8868b237ad92435</tt></td></tr>
</table>
