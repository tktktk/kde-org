---
version: "5.247.0"
title: "KDE Frameworks 6 Beta 2 Source Info and Download"
type: info/frameworks6
date: 2023-12-20
signer: Jonathan Esk-Riddell
signing_fingerprint: E0A3EB202F8E57528E13E72FD7574483BB57B18D
custom_annc: kdes-6th-megarelease-alpha
draft: false
---
