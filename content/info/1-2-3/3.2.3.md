---
title: "KDE 3.2.3 Info Page"
---

<p>
KDE 3.2.3 was released on June 9th, 2004. Read the <a
href="/announcements/announce-3.2.3">official announcement</a>.</p>


<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#00097;&#105;lt&#0111;&#058;s&#0101;&#x63;urity&#064;kd&#101;&#00046;org">&#115;&#x65;c&#117;r&#x69;t&#121;&#x40;k&#00100;&#x65;&#0046;&#x6f;r&#103;</a>.</p>

<p>Patches for the issues mentioned below are available from
<a href="ftp://ftp.kde.org/pub/kde/security_patches">ftp://ftp.kde.org/pub/kde/security_patches</a>
unless stated otherwise.</p>

<ul>
<li>
KDE fails to ensure the integrity of certain symlinks. This can be abused by a local
attacker to create or truncate arbitrary files or to prevent KDE applications
from functioning correctly.
<br />Read the <a href="/info/security/advisory-20040811-1.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.2.3 are affected.
</li>
<li>
KDE's DCOPServer creates temporary files in an insecure manner. Since the temporary
files are used for authentication related purposes this can potentially allow a local
attacker to compromise the account of any user which runs a KDE application.
<br />Read the <a href="/info/security/advisory-20040811-2.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.2.3 are affected.
</li>
<li>
The Konqueror webbrowser allows websites to load webpages into
a frame of any other frame-based webpage that the user may have open.
<br />Read the <a href="/info/security/advisory-20040811-3.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.2.3 are affected.
</li>
<li>
Konqueror allows websites to set cookies for certain country specific secondary top level domains.
(Cross-domain cookie injection)
<br />Read the <a href="/info/security/advisory-20040823-1.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.2.3 are affected.
</li>
<li>
KPDF contains multiple integer overflow and integer arithmetic flaws that may make it possible
to execute arbitrary code on the client machine via remotely supplied PDF files.
<br />Read the <a href="/info/security/advisory-20041021-1.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.3.1 are affected.
</li>
<li>
KDE may unexpectedly expose user provided passwords in certain cases, especially passwords for
SMB shares.
<br />Read the <a href="/info/security/advisory-20041209-1.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.3.2 are affected.
</li>
<li>
KFax contains several vulnerabilities that may cause specially crafted fax files to trigger
buffer overflows and execute arbitrary code.
<br />Read the <a href="/info/security/advisory-20041209-2.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.3.1 are affected.
<br />No source patches are available for this problem, users are advised to either remove KFax or
to upgrade to KDE 3.3.2.
</li>
<li>
The Konqueror webbrowser allows websites to load webpages into
a window or tab currently used by another website.
<br />Read the <a href="/info/security/advisory-20041213-1.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.3.2 are affected.
</li>
<li>
Two flaws in the Konqueror webbrowser make it possible to by pass
the sandbox environment which is used to run Java-applets.
<br />Read the <a href="/info/security/advisory-20041220-1.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.3.1 are affected.
</li>
<li>
kpdf contains a buffer overflow in its xpdf-based code which can be triggered 
by a specially crafted pdf file.
<br />Read the <a href="/info/security/advisory-20041223-1.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.3.2 are affected.
</li>
<li>
ftp kioslave contains a vulnerability which allows to inject arbitrary ftp or
smtp commands.
<br />Read the <a href="/info/security/advisory-20050101-1.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.3.2 are affected.
</li>
<li>
kpdf contains a buffer overflow in its xpdf-based code which can be triggered 
by a specially crafted pdf file.
<br />Read the <a href="/info/security/advisory-20050119-1.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.3.2 are affected.
</li>
<li>
A local user can lock up the dcopserver of arbitrary other users
on the same machine by stalling the DCOP authentication process.
<br />Read the <a href="/info/security/advisory-20050316-1.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.3.2 are affected.
</li>
<li>
International Domain Name (IDN) support in Konqueror/KDE makes
KDE vulnerable to a phishing technique known as a Homograph attack.
<br />Read the <a href="/info/security/advisory-20050316-2.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.3.2 are affected.
</li>
<li>
The dcopidlng script is vulnerable to symlink attacks, potentially
allowing a local user to overwrite arbitrary files of a user when
the script is run on behalf of that user. This only affects users
who compile KDE or KDE applications themselves.
<br />Read the <a href="/info/security/advisory-20050316-3.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.3.2 are affected.
</li>
<li>
The Kate KPart (used by the applications kate and kwrite, possibly others)
generates a backup file with default permissions upon saving. Depending
on the setup, this could cause file content leak to local and remote
(due to network transparency) users.
<br />Read the <a href="/info/security/advisory-20050718-1.txt">detailed advisory</a>.
KDE 3.2.x up to including KDE 3.4.0 are affected.
</li>
<li>
The Gadu-Gadu protocol handler of Kopete 3.3 and above contains a copy
of libgadu, that is used if there is no system installed libgadu library.
Multiple integer overflow vulnerabilities have been found in libgadu.
<br />Read the <a href="/info/security/advisory-20050721-1.txt">detailed advisory</a>.
KDE 3.2.3 up to including KDE 3.4.1 are affected.
</li>
<li>
The langen2kvtml script (included in kdeedu/kvoctrain) contains
multiple temp file generation vulnerabilities. 
<br />Read the <a href="/info/security/advisory-20050815-1.txt">detailed advisory</a>.
KDE 3.0.x up to including KDE 3.4.2 are affected.
</li>
<li>
The kcheckpass utility contains on certain platforms a local
root vulnerability.
<br />Read the <a href="/info/security/advisory-20050905-1.txt">detailed advisory</a>.
KDE 3.2.0 up to including KDE 3.4.2 are affected.
</li>
<li>
kpdf contains several buffer overflows in its xpdf-based code which can be triggered 
by a specially crafted pdf file.
<br />Read the <a href="/info/security/advisory-20051207-2.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.5.0 are affected.
</li>

<li>
kjs contains a heap based buffer overflow when decoding certain malcrafted utf8
uri sequences.
<br />Read the <a href="/info/security/advisory-20060119-1.txt">detailed advisory</a>.
All versions of KDE starting with KDE 3.2.0 up to and including KDE 3.5.0 are affected.
</li>

<li>
KDM contains a symlink attack vulnerability that allows a normal
user to read files from other users including root.<br />
Read the <a href="/info/security/advisory-20060614-1.txt">detailed advisory</a>.
All versions of KDE starting with KDE 3.2.0 up to and including KDE 3.5.2
are affected.
</li>




</ul>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release date:</p>

<ul>
<li>Grave regressions in Cervisia have been found too late.
<a href="http://bugs.kde.org/show_bug.cgi?id=82852">Highlighting incorrect in resolve dialog</a> and
<a href="http://bugs.kde.org/show_bug.cgi?id=82853">Version selection buttons in resolve dialog don't work</a>.
</li>
</ul>
<p>Please check the <a href="http://bugs.kde.org/">bug database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>FAQ</h2>

See the <a href="https://userbase.kde.org/Asking_Questions">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed
<a href="http://konqueror.kde.org/faq/">to the Konqueror FAQ</a>
and sound related questions are answered <a
href="http://www.arts-project.org/doc/handbook/faq.html">in the FAQ of
the aRts Project</a>

<h2>Download and Installation</h2>

<p>
<u>Library Requirements</u>.
 <a href="/info/1-2-3/requirements/3.2">KDE 3.2 requires or benefits</a>
 from the given list of libraries, most of which should be already installed
 on your system or available from your OS CD or your vendor's website.
</p>
<p>
  The complete source code for KDE 3.2.3 is available for download:
</p>

{{< readfile "/content/info/1-2-3/source-3.2.3.inc" >}}

<!-- Comment the following if Konstruct is not up-to-date -->
<p>The <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> build toolset can help you
downloading and installing these tarballs.</p>

<u><a name="binary">Binary packages</a></u>

<p>
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.2 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/">http</a> or
  <a href="/mirrors/ftp.php">FTP mirrors</a>.
</p>

<p>
  At the time of this release, pre-compiled packages are available for:
</p>

{{< readfile "/content/info/1-2-3/binary-3.2.3.inc" >}}

<p>
Additional binary packages might become available in the coming weeks,
as well as updates to the current packages.
</p>

<h2>Developer Info</h2>

If you need help porting your application to KDE 3.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/3.5/kdelibs/KDE3PORTING.html">
porting guide</a> or subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a>
to ask specific questions about porting your applications.

<p>There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html">architecture</a>
and the <a href="http://developer.kde.org/documentation/library/3.2-api/">
programming interface of KDE 3.2</a>.
</p>
