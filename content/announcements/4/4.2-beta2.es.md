---
aliases:
- ../announce-4.2-beta2
date: '2008-12-16'
title: Anuncio de la publicación de KDE 4.2 Beta 2
---

<h3 align="center">
  Versión KDE 4.2 Beta 2 para ser probada
</h3>

<p align="justify">
  <strong>
La comunidad de KDE publica la segunda versión beta de KDE 4.2, invitando a la
comunidad a que la pruebe e informe de fallos.
</strong>
</p>

<p align="justify">
La <a href="/">comunidad de
KDE</a> anunció hoy la disponibilidad inmediata de <em>«Canaria»</em>, (alias
KDE 4.2 Beta 2), la segunda versión de pruebas del nuevo escritorio KDE 4.2. 
<em>Canaria</em> está dirigida a probadores y críticos. Debería proporcionar
una base sólida para <a href="http://bugs.kde.org">informar de fallos</a> que
necesiten ser abordados antes de que se publique KDE 4.2.0.  Los críticos
pueden usar esta beta para obtener una primera impresión del inminente
escritorio KDE 4.2, que proporciona importantes mejoras en todo el escritorio
y en las aplicaciones.<br />
Desde la primera beta, que se publicó hace menos de 4 semanas, se han abierto
1665 bugs nuevos, y se han cerrado 2243.  Desde la publicación de KDE 4.1.0,
se han cerrado más de diez mil bugs, lo que demuestra la enorme atención que
se está prestando a la estabilidad del inminente KDE 4.2.0, que se publicará
en enero de 2009, 6 meses después de KDE 4.1.  A KDE 4.2.0 le seguirá una
serie de actualizaciones mensuales, así como KDE 4.3.0 en verano de 2009.
</p>

<div class="text-center">
	<a href="/announcements/4/4.2-beta2/panel.png">
	<img src="/announcements/4/4.2-beta2/panel_thumb.png" class="img-fluid">
	</a> <br/>
	<em>El panel en KDE 4.2 Beta 2</em>
</div>
<br/>

<h2>Importantes mejoras de Plasma y KWin, el espacio de trabajo de KDE</h2>
<p>
    <ul>
        <li>
            La serie KDE 4.2 ofrecerá considerables mejoras en estabilidad,
funcionalidad y rendimiento sobre las versiones anteriores de KDE4.
        </li>
        <li>
            Se han incorporado muchas características al escritorio de Plasma
que los usuarios echaban en falta en las versiones anteriores de KDE4.
        </li>
        <li>
            Se han añadido muchas funcionalidades a las aplicaciones que
acompañan a KDE 4.2-Beta, y resuelto muchos de sus fallos.
        </li>
        <li>
            La plataforma de desarrollo KDE ha visto significativas mejoras en
plataformas distintas a Linux, tales como BSD, Windows y Mac OS X, haciendo
que haya más aplicaciones disponibles a los usuarios de esos sistemas
operativos.
        </li>
        <li>
            Escribir aplicaciones y añadidos de KDE en lenguajes de script
como Python y Ruby es más fácil que nunca.
        </li>
    </ul>
</p>

<p>
Para la versión 4.2, el equipo de KDE ha corregido literalmente miles de
fallos e implementado docenas de características que faltaban hasta ahora. 
Esta versión beta le ofrece la oportunidad de ayudar a pulir los detalles
restantes.  El equipo de publicación de KDE ha preparado una lista con las
mejoras más significativas de KDE 4.2 Beta 2:
</p>

<h2>Muchas mejoras</h2>
<p>
<ul>
  <li>
    Los <strong>efectos de composición del escritorio</strong> están
habilitados si el hardware y los controladores lo admiten, con una
configuración básica predeterminada.  Unas comprobaciones automáticas
verifican si la composición funciona antes de habilitarla en el espacio de
trabajo.
  </li>
  <li>
    Se han añadido <strong>nuevos efectos de escritorio</strong>, como la
<em>Lámpara maravillosa</em>, el efecto <em>Minimizar</em> y los
intercambiadores de escritorio <em>Cubo</em> y <em>Esfera</em>.  Otros, como
la rejilla de escritorio, han sido mejorados.  Todos los efectos han sido
pulidos y parecen más naturales debido al uso de dinámicas de movimiento.  Se
ha revisado la interfaz de usuario para elegir los efectos para facilitar la
selección de los efectos más usados.
  </li>
  <li>
    Los <strong>elementos centrales</strong> del escritorio han experimentado
mejoras significativas para proporcionar un escritorio usable y coherente. 
Entre éstas se incluyen la posibilidad de agrupar las ventanas y disponerlas
en varias filas en la barra de tareas, la ocultación de iconos en la bandeja
del sistema, notificaciones y seguimiento de trabajos en Plasma, y la
posibilidad de tener de nuevo iconos en el escritorio usando una «Vista de
carpeta» como fondo de escritorio.  Se reincorporán características como la
ocultación automática del panel para maximizar el espacio de pantalla
productivo, que ahora los iconos permanezcan donde se colocaron en la «Vista
de carpeta», y las previsualizaciones de ventanas y consejos en el panel y la
barra de tareas.  También mejora la ubicación de las nuevas miniapliaciones.
  </li>
  <li>
    Entre las <strong>nuevas miniaplicaciones de Plasma</strong> hay
miniaplicaciones para dejar mensajes en una pantalla bloqueda, previsualizar
ficheros, cambiar la actividad del escritorio, monitorizar fuentes de noticias
y utilidades como la miniaplicación Pastebin, el calendario, cronómetro,
selector de caracteres especiales, un lanzador rápido de aplicaciones, y un
monitor del sistema, entre muchas otras.
  </li>
  <li>
    Se han ampliado las funcionalidades de KRunner, la  <strong>ventana de
"Ejecutar orden..."</strong> a través de varios nuevos complementos, como el
corrector ortográfico, historial de navegación de konqueror, control de la
gestión de energía mediante PowerDevil, ubicaciones de KDE, documentos
recientes, y la posibilidad de iniciar sesiones específicas del editor Kate,
Konqueror y Konsole.  El complemento de conversión de unidades también puede
ahora convertir rápidamente entre unidades de velocidad, masa y distancia.
  </li>
  <li>
    Plasma ya puede cargar <strong>Google Gadgets</strong>.  Las
<strong>miniaplicaciones</strong> de Plasma se pueden escribir en
<strong>Ruby</strong> y <strong>Python</strong>.  Se ha mejorado aún más el
manejo de miniaplicaciones escritas en JavaScript y los <em>widgets</em> del
<em>dashboard</em> de Mac OS.
  </li>
  <li>
    Los fondos de pantalla ahora se proporcionan como complementos, de modo
que los desarrolladores pueden escribir fácilmente <strong>sistemas de fondo
de pantalla personalizados</strong> en KDE 4.2.  Los complementos de fondo de
pantalla disponibles en KDE 4.2 serán presentaciones y, por supuesto, imágenes
estáticas normales y colores planos.
  </li>
  <li>
    Las <strong>mejoras para aplicar temas</strong> sobre la barra de tareas,
el lanzador de aplicaciones, la bandeja del sistema y la mayoría de los demás
componentes de Plasma racionalizan su aspecto y comportamiento, y aumentan la
consistencia.  Un nuevo módulo de Ajustes del sistema, «Detalles del tema del
escritorio», proporciona al usuario control sobre cada uno de los elementos de
los diversos temas de Plasma.
  </li>
  <li>
    Se ha mejorado la utilización de <strong>varias pantallas</strong> por
medio de la biblioteca Kephan, corrigiendo muchos fallos cuando se ejecuta KDE
en más de un monitor.
  </li>
</ul>
</p>

<div class="text-center">
	<a href="/announcements/4/4.2-beta2/desktop.png">
	<img src="/announcements/4/4.2-beta2/desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>El escritorio como Vista de carpeta (escritorio tradicional)</em>
</div>
<br/>

<h2>Aplicaciones nuevas y mejoradas</h2>
<p>
Todas las aplicaciones han recibido correcciones de errores, nuevas
características, mejoras en la interfaz de usuario y en general un mejor
acabado durante los últimos 4 meses de desarrollo.  La siguiente lista da una
idea sobre las áreas de mejora.
</p>
<p>
<ul>
  <li>
    Dolphin permite ahora previsualizaciones de ficheros en las sugerencias, e
incorpora un deslizador para aumentar y reducir el tamaño en las vistas de
fichero.  Ahora también puede mostrar la ruta completa en la barra de miguitas
de pan.
  </li>
  <li>
    Konqueror ofrece una <strong>mayor velocidad de carga</strong> al
predescargar datos de nombres de dominio en KHTML.  Una barra para buscar
mientras se teclea mejora la navegación en las páginas web.  También incorpora
la opción para usar sus marcadores como la página de inicio por medio del
nuevo esclavo KIO Marcadores.
  </li>
  <li>
    KMail tiene una potente y atractiva <strong>lista de cabeceras del
mensaje</strong>, y se ha revisado su una vista de adjuntos.
  </li>
  <li>
    Los editores de texto KWrite y Kate pueden ahora funcionar en el modo de
entrada de <strong>vi</strong>, para comodidad de quienes estén acostumbrado
al editor tradicional de Unix.
  </li>
  <li>
    PowerDevil, el nuevo gestor de energía de KDE4, proporciona una
herramienta moderna e integrada para controlar diversos aspectos de los
dispositivos móviles.
  </li>
  <li>
    Se ha mejorado la interfaz de usuario de la herramienta de archivo Ark, y
se le ha añadido la posibilidad de usar archivos protegidos por contraseña. 
Ahora también está accesible por medio de un <strong>menú contextual</strong>
desde los gestores de ficheros.
  </li>
  <li>
    Un <strong>nuevo sistema de configuración de la impresión</strong> trae de
vuelta varias características que los usuarios echaban en falta en KDE 4.0 y
4.1.  Los componentes «printer-applet» y «system-config-printer» se
proporcionan con los módules kdeadmin y kdeutils.
  </li>
  <li>
      En el cliente de escritorio remoto KRDC se ha mejorado la implementación
del <strong>Active Directory</strong> de Microsoft a través de LDAP.
  </li>
  <li>
    Kontact ha recibido un nuevo <strong>resumen del calendario</strong> y
posibilidad de arrastrar y soltar en la vista de ocios y ocupaciones.
  </li>
  <li>
    Ahora KSnapshot almacena el título de la ventana como metadato al guardar
capturas de pantalla, facilitando su indexación mediante motores de búsqueda.
  </li>
  <li>
    Los protocolos seguros de transferencia de ficheros SFTP y FISH también
están disponibles ahora en la <strong>plataforma KDE sobre Windows</strong>.
  </li>
  <li>
    Killbots es un nuevo juego del módulo kdegames.  Se ha mejorado la
interacción con el usuario de otros juegos, y añadido temas y niveles.
  </li>
  <li>
    Se han realizado enormas mejoras en la interfaz gráfica y característica
de aplicaciones educativas como KAlgebra, KStars, KTurtle y Parley.
  </li>
  <li>
    Se han mejorado significativamente varios aspectos de la interfaz de
usuario del editor hexadecimal Okteta.
  </li>
</ul>
Esta lista de nuevas características de KDE 4.2 todavía no es exhaustiva. 
Visite la <a href="http://techbase.kde.org/Schedules/KDE4/4.2_Changelog">lista
de cambios</a> para obtener más detalles.
</p>

<div class="text-center">
	<a href="/announcements/4/4.2-beta2/improved-desktop-grid.png">
	<img src="/announcements/4/4.2-beta2/improved-desktop-grid_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Efecto mejorado del escritorio «rejilla»</em>
</div>
<br/>

<p>
Para obtener más información sobre el escritorio KDE 4 y sus aplicaciones,
visite también las notas de publicación de <a
href="/announcements/4.1/">KDE 4.1.0</a> y <a
href="/announcements/4.0/">KDE 4.0.0</a>.  No se recomienda
KDE 4.2 Beta 2 para su uso cotidiano.
A esta beta le seguirá una versión candidata el 6 de enero y la versión final
de KDE 4.2.0 el 27 de enero, 6 meses después de la publicacióin de KDE 4.1.0.
<p />

<h4>
  Instalación de paquetes binarios de KDE 4.2 Beta 2
</h4>
<p align="justify">
  <em>Empaquetadores</em>.
  Algunos proveedores de sistemas operativos Linux/Unix han tenido la
amabilidad de proporcionar paquetes de KDE 4.2 Beta 2, y en otros caso lo han
hecho voluntarios de la comunidad.  Algunos de estos paquetes binarios se
puedes descargar libremente desde la <a
href="/info/4/4.1.85#binary">página de información de KDE 4.2 Beta 2</a>.
  Durante las próximas semanas, puede que haya disponibles más paquetes
binarios, así como actualizaciones para los paquetes que hay disponibles
ahora.
</p>

<p align="justify">
  <a id="package_locations"><em>Ubicaciones de los paquetes</em></a>.
  Visite la <a href="/info/4/4.1.85"> página de información sobre KDE 4.2
Beta 2</a> para obtener una lista actualizada de los paquetes binarios
disponibles de los que el Proyecto KDE haya sido informado.
</p>

<h4>
  Compilación de KDE 4.2 Beta 2
</h4>
<p align="justify">
  <a id="source_code"></a>
  El código fuente completo de KDE 4.1.85 se puede <a
href="/info/4/4.1.85#desktop">descargar libremente</a>.
Las instrucciones para compilar e instalar KDE 4.1.85 están disponibles en la
<a href="/info/4/4.1.85">página de información de KDE 4.2 Beta 2.</a>.
</p>



