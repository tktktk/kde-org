---
aliases:
- ../../fulllog_releases-24.08.1
title: KDE Gear 24.08.1 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ Fix loading/saving resource config file in config dialog when using Akonadi Instances. [Commit](http://commits.kde.org/akonadi/87776473e71b976f4ed441dd896c9b632d1039d3).
+ Fix crash when query is evicted from QueryCache. [Commit](http://commits.kde.org/akonadi/dc86d700ee726a7645bd66738c201cf2cb8957af). Fixes bug [#492547](https://bugs.kde.org/492547).
{{< /details >}}
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Fix duplicated text in "remind later" dialog. [Commit](http://commits.kde.org/akonadi-calendar/3671fadfa0cdc433912c650b5854ab75be67c3e2).
{{< /details >}}
{{< details id="analitza" title="analitza" link="https://commits.kde.org/analitza" >}}
+ Switch to the CMake target for GLEW as it appears that GLEW_LIBRARY is no longer being set as a variable. [Commit](http://commits.kde.org/analitza/532327562737fb320822576dc6331292dee8eb2a).
{{< /details >}}
{{< details id="arianna" title="arianna" link="https://commits.kde.org/arianna" >}}
+ Fix build with Qt 6.8. [Commit](http://commits.kde.org/arianna/39799aee4d720a0baeefd983b17d74e1791f08d5). Fixes bug [#491761](https://bugs.kde.org/491761).
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Settings: Fix the Use Current Location button. [Commit](http://commits.kde.org/dolphin/71cc4e65df54c44d1d8a6e2d78a53a002ca8145c). Fixes bug [#491753](https://bugs.kde.org/491753).
+ KItemListRoleEditor: minimize resize() occurences. [Commit](http://commits.kde.org/dolphin/63fd6ed1b1e248ebc752c9c2270e13335138310c). Fixes bug [#479695](https://bugs.kde.org/479695).
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Remove filters to hide title, artist, and album from Now Playing view. [Commit](http://commits.kde.org/elisa/303cb9b8dd4d0a2275784417c0b32ed693ade495). Fixes bug [#468644](https://bugs.kde.org/468644).
+ Android: add missing icon. [Commit](http://commits.kde.org/elisa/1b0d37f66130f148433317b5ad3ce074024db322).
+ Fix mismatched toolbar heights. [Commit](http://commits.kde.org/elisa/afe9bffd7150720186400b2792548686416cb063).
+ HeaderBar: unmaximize when track information clicked. [Commit](http://commits.kde.org/elisa/58495f838c8f9307678999e91741a4e17c7689e0).
+ SimplePlayListView: remove hardcoded text color. [Commit](http://commits.kde.org/elisa/821c1ef93c7895adb51d124d306c1b6e64805d1e).
+ SimplePlayListView: disable animations. [Commit](http://commits.kde.org/elisa/29a6ab12f40f4de379535352791dd0baadce5b86).
+ HeaderBar: set background image background. [Commit](http://commits.kde.org/elisa/e56e239800599948985d8e29465e7d506e2c310c). Fixes bug [#464865](https://bugs.kde.org/464865).
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Always reset the filemodel when the tree changes. [Commit](http://commits.kde.org/filelight/6b2e0b823359495a6ab05a33c351ff44684ac922). Fixes bug [#492155](https://bugs.kde.org/492155).
{{< /details >}}
{{< details id="isoimagewriter" title="isoimagewriter" link="https://commits.kde.org/isoimagewriter" >}}
+ Update Neon key for new 2024 ISO signing key. [Commit](http://commits.kde.org/isoimagewriter/ab6f363e1ce983abe6681805991456892526a8f1).
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Adjust to Craft Boost blueprint changes. [Commit](http://commits.kde.org/itinerary/5e644bd93a568fbc49fc6415742097f052b03017).
+ Add diagnostic information about language settings. [Commit](http://commits.kde.org/itinerary/5f6c30325acef403ef4e674b5f70298495d94ac9).
+ Fix icon recoloring for the transfer icon. [Commit](http://commits.kde.org/itinerary/26c41d7b36d6a680c22e6900560efb5f6c7053f8).
+ Only search for KF::ColorScheme on Android. [Commit](http://commits.kde.org/itinerary/e4f8a0245dfe07c2197cd69c4dd9bb7ff89fbde5).
+ Enable automatic dark mode handling on Android. [Commit](http://commits.kde.org/itinerary/7a773ee4cf0f3b536b3e1304c96a4c8b8daba926).
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Fix appdata syntax. [Commit](http://commits.kde.org/kalarm/34e86f80446dfba846d7a134531f7b733ff87789).
+ Fix build. [Commit](http://commits.kde.org/kalarm/d51102c2e82a94cefe5a27ced1511d35cb7933c5).
+ Bug 492425: Fix audio alarms not repeating (using libVLC). [Commit](http://commits.kde.org/kalarm/0533e2ff74b2c689a62b9e98b7f76c418f76dab2).
+ Add release description. [Commit](http://commits.kde.org/kalarm/d54f18d0983f26f5ded8a2ba57d6ca574db38dd8).
{{< /details >}}
{{< details id="kanagram" title="kanagram" link="https://commits.kde.org/kanagram" >}}
+ Main: Increase size of 'Next Anagram' and 'Configure' text. [Commit](http://commits.kde.org/kanagram/7602d1178e422d71217c7d3dc709ac6f5f18d800).
+ Main: Increase size of moreOptionsText. [Commit](http://commits.kde.org/kanagram/3702f1a3a5eeaf8cb2dc96e1454e65e5d855217b).
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Back to normal konsole. [Commit](http://commits.kde.org/kate/1778fb3495268fc84469f9deeb02a7002cace549).
+ Remove unneeded search result expanding limit. [Commit](http://commits.kde.org/kate/593c138fe1ac7353a7ff3f91990fe83f00a61b08). Fixes bug [#491890](https://bugs.kde.org/491890).
+ KateBuildView: Remove close button entirely. [Commit](http://commits.kde.org/kate/f676834ba339c35f955541ec24ce51f264f5cc1e).
+ Formatter: add formatting for Odin language. [Commit](http://commits.kde.org/kate/f260ba217d59d2eca88dc4704678eadad1191eb4).
+ Change the default LSP client for GLSL language to glsl_analyzer. [Commit](http://commits.kde.org/kate/81ed97ba358f34756fb435feea7b0663f74f6a04).
+ Fix tooltip hides on scrolling using scrollbar. [Commit](http://commits.kde.org/kate/9198d8f8a002bce9f885e7acb51f402eaaecb72e). Fixes bug [#492092](https://bugs.kde.org/492092).
+ Tooltip: Only highlight blocks that are marked BlockCodeLanguage. [Commit](http://commits.kde.org/kate/e223796258b309b6cbde2ed018c9d4976abcd885). Fixes bug [#492076](https://bugs.kde.org/492076).
+ Dont insert line breaks in tooltip. [Commit](http://commits.kde.org/kate/af5d2c83457fe4e788fc7d465f387fe90e707b76). Fixes bug [#492062](https://bugs.kde.org/492062).
+ Fix crash on window close. [Commit](http://commits.kde.org/kate/228b256129ac62a78accc98c4b9690bbfda815af).
+ Build release branch. [Commit](http://commits.kde.org/kate/7d793fbd7bf199e104275c6d87e74c42ce233e30).
{{< /details >}}
{{< details id="kclock" title="kclock" link="https://commits.kde.org/kclock" >}}
+ Fix timer circle alignment. [Commit](http://commits.kde.org/kclock/424bcb294e7e1bf6466690b1701acc24ac5c7949). Fixes bug [#481170](https://bugs.kde.org/481170).
+ Call portal for autostart only in flatpak. [Commit](http://commits.kde.org/kclock/d8a1799b3381317855b71c52fd3a023afeea8c4f).
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Revert changes to QML incompatible with Qt 6.6 and older. [Commit](http://commits.kde.org/kdeconnect-kde/f3fa818cbfe9eb39184f68324689c2ad9eeb6371).
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix reassigning timecode to project clip. [Commit](http://commits.kde.org/kdenlive/69950f0c64d71bbd34bf069125b03926c47b7f6f). Fixes bug [#492697](https://bugs.kde.org/492697).
+ Fix possible crash on undo/redo single selection move. [Commit](http://commits.kde.org/kdenlive/cf988a8217ee55c1e07192db58735410761bf5a1).
+ Fix dragging transitions to a clip cut to create a mix. [Commit](http://commits.kde.org/kdenlive/6628dabaee8a7a6ba12372c85c2f7cbb0e643c0c).
+ Fix multiple selection broken. [Commit](http://commits.kde.org/kdenlive/818abe6453c06ef8759a681d727d449225a95f54).
+ Fix clip offset not appearing on selection in timeline. [Commit](http://commits.kde.org/kdenlive/d35a1caa94d72be6d1c5ee975d63abd3a30c2939).
+ Ensure bin clips with effects disabled keep their effects disabled when added to a new sequence. [Commit](http://commits.kde.org/kdenlive/b0898947e8847e765e83f114e5408a8552e91d06).
+ Fix keyframe at last frame prevents resizing clip on high zoom. [Commit](http://commits.kde.org/kdenlive/f966bf90878ce0fbbdceb8214d45a90284734abb).
+ Fix effects/compositions list size. [Commit](http://commits.kde.org/kdenlive/8c66897592a3008d81429822255090226d586e54). Fixes bug [#492586](https://bugs.kde.org/492586).
+ Fix compositions cannot be easily selected in timeline. [Commit](http://commits.kde.org/kdenlive/e3cd1a4cf7c073c100e4e264e5ae2deb6164d4c1).
+ Replace : and ? chars in guides names for rendering. [Commit](http://commits.kde.org/kdenlive/df095fc3d7932c7629113544bfacfe1995d7f686). See bug [#492595](https://bugs.kde.org/492595).
+ Don't trigger timeline scroll when mouse exits timeline on a clip drag, it caused incorrect droppings and ghost clips. [Commit](http://commits.kde.org/kdenlive/cbdcfda07e8fe0610b599f671f90850cc48b9865). See bug [#492720](https://bugs.kde.org/492720).
+ Fix scolling timeline with rubberband or when dragging from file manager can move last selected clip in timeline. [Commit](http://commits.kde.org/kdenlive/527c74e96c8de4d2cd0e52a59fe6b235f7edfedb). Fixes bug [#492635](https://bugs.kde.org/492635).
+ Fix adding marker from project notes always adds it at 00:00. [Commit](http://commits.kde.org/kdenlive/e61520c2e0dd2078761d0e72b6de34b69338df3c). Fixes bug [#492697](https://bugs.kde.org/492697).
+ Fix blurry widgets on high DPI displays. [Commit](http://commits.kde.org/kdenlive/fd9abc1a795e80e220728c29760cb968e5af15e9).
+ Fix keyframe param not correctly enabled on first keyframe click. [Commit](http://commits.kde.org/kdenlive/4c87c45d60667d3a314f90cbe7fa568bbfe33335).
+ Fix curveeditor crash on empty track. [Commit](http://commits.kde.org/kdenlive/06691fe7d76804ed49a2e9c1f6e549640361ff46).
+ Ensure rendering with separate file for each audio track keeps the correct audio tag in the file name. [Commit](http://commits.kde.org/kdenlive/2ab244cdac74d729d2dc3593414c6096f1012ee2).
+ Fix render project folder sometimes lost, add proper enums instead of unreadable ints. [Commit](http://commits.kde.org/kdenlive/f04dc6a47f59d3135fc5b55e9e233789fb56f323). See bug [#492476](https://bugs.kde.org/492476).
+ Fix MLT lumas not correctly recognized by archive feature. [Commit](http://commits.kde.org/kdenlive/74eaf13e87c9744d9f943a2f63effa47475a726d). Fixes bug [#492435](https://bugs.kde.org/492435).
+ Fix configure toolbars messing UI layout. [Commit](http://commits.kde.org/kdenlive/675e498d6c6502bdc73f1c8856b2e706c1822441).
+ Effects List: ensure deprecated category is always listed last. [Commit](http://commits.kde.org/kdenlive/e77037c42c11a46a0a97aeab753d5f6b63cdad6c).
+ Fix tabulations in Titler (requires latest MLT git). [Commit](http://commits.kde.org/kdenlive/818ae8a016d12cc6cd8192e63ec53c7f194ee5c5).
+ Titler: ensure only plain text can be pasted, prepare support for tabulations (needs MLT patch). [Commit](http://commits.kde.org/kdenlive/a862ec594e1d4f298366fb7188acd537c21ede6e).
+ Don't accept empty whisper device. [Commit](http://commits.kde.org/kdenlive/1a1cef306e32aa28e70ad10511dd443617a6535b).
+ Fix ffmpeg path for Whisper on Mac. [Commit](http://commits.kde.org/kdenlive/cc0bb00a4e8abeda6860fe2b777578db49efeba7).
+ Fix archive doesn't save the video assets when run multiple times. [Commit](http://commits.kde.org/kdenlive/d0fb7c46f4c10d3d2725a3df9c09de2f5f60235b).
+ Fix document notes timecode links may be broken after project reload. [Commit](http://commits.kde.org/kdenlive/2fd7cad71dd060739086a88f216fa8edc3c0714e). See bug [#443597](https://bugs.kde.org/443597).
+ Fix broken qml font on AppImage. [Commit](http://commits.kde.org/kdenlive/e98a8bbcfb6877b04e5b8013b7ef4cae801d8603).
+ Remove incorrect taskmanager unlock. [Commit](http://commits.kde.org/kdenlive/588b674e8fcda6c1c80c5c932d368b5240078391).
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Forward network error to the resouce. [Commit](http://commits.kde.org/kdepim-runtime/13bda2e41f84df8ba04df0e32be6c213a56be121). Fixes bug [#485799](https://bugs.kde.org/485799). Fixes bug [#484579](https://bugs.kde.org/484579).
{{< /details >}}
{{< details id="kdevelop" title="kdevelop" link="https://commits.kde.org/kdevelop" >}}
+ UsesWidget: don't try to insert items at invalid positions. [Commit](http://commits.kde.org/kdevelop/c7d509b5846f24760c6af2dbb5fec8e727c5a86a). Fixes bug [#492221](https://bugs.kde.org/492221).
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Add United Airlines reservations parser. [Commit](http://commits.kde.org/kitinerary/b52ad62796a59bafea530b03432ecade82657a3b).
+ Sunnycars: Extract drop-off location, if available. [Commit](http://commits.kde.org/kitinerary/25e112d15024c736ab37f5d80d5c57f83ba7f3ad).
+ Handle MAV ticket barcodes without a trip block. [Commit](http://commits.kde.org/kitinerary/b17eadeca193126ff000f7b160b99be44df229cc).
+ Fix latitude/longitude mixup. [Commit](http://commits.kde.org/kitinerary/7f1f940a5407bb5ef619d2f712fac8adb1539a52).
+ Add extractor script for (new?) Italo PDF tickets. [Commit](http://commits.kde.org/kitinerary/7f41ed4fb8e2f442a29c64887768b609b2be74bb).
+ Support French date/time format in BlablaBus tickets. [Commit](http://commits.kde.org/kitinerary/c9937dd33309785a821028219226c3c26e8bbfea).
+ Fix extraction of multi-page BlablaBus PDF tickets. [Commit](http://commits.kde.org/kitinerary/e85ce9798179e1a4ff9034f45f561128dcfbd5fa).
+ Handle more Eurostar PDF layout variants. [Commit](http://commits.kde.org/kitinerary/27c9f5c2ff492b53aad23ae3ecef1ccde4ce112d).
+ Add extractor script for Reisnordland ferries. [Commit](http://commits.kde.org/kitinerary/85fec3e71c8c0b1a8738a741d1dc674fbc31b804). Fixes bug [#492096](https://bugs.kde.org/492096).
+ Unify the two DB ical extractor methods a bit. [Commit](http://commits.kde.org/kitinerary/37479b99c7c3d69829c2b8d3043d1d66e3b00a83).
+ Add Reservix extractor. [Commit](http://commits.kde.org/kitinerary/329040d10bd6c35b6b0891c1464ae8c9c5097320).
+ Try harder to decode names in VDV tickets. [Commit](http://commits.kde.org/kitinerary/b2ca4aa896f10b320d9c5f7e9fa73199a63950e5).
{{< /details >}}
{{< details id="kjournald" title="kjournald" link="https://commits.kde.org/kjournald" >}}
+ Make tests more robust for long journald readings. [Commit](http://commits.kde.org/kjournald/1af2c61c1e4c184ec5c86d1a376427c8a3bd0671).
+ Rework, simplify and add fallbacks for cursor seeking. [Commit](http://commits.kde.org/kjournald/601d40de81b4377c8ac5394f5102732a28f637c8).
+ Provide more debug output for cursor test failure. [Commit](http://commits.kde.org/kjournald/44df35a21a1f3dc697013f1b1f730de3c0875e54).
+ Always convert back to UTF8. [Commit](http://commits.kde.org/kjournald/c6f3086c97e68e62fbf6b3cddf1bd8c2c44ebad3).
+ Enforce that data/time is always interpret as UTC. [Commit](http://commits.kde.org/kjournald/f92461d020b369d93e5f4d06204f95ec4df5ded5).
+ Use UTC for test value setup to be independent of local time. [Commit](http://commits.kde.org/kjournald/51f70d02db40fa91f1703aef1d592b5215796080).
{{< /details >}}
{{< details id="kongress" title="kongress" link="https://commits.kde.org/kongress" >}}
+ Enable automatic dark mode support on Android. [Commit](http://commits.kde.org/kongress/d528b16f047d82e42c91ce8a2c61ae0cd1a4dd43).
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Disable GPU for HTML thumbnailer. [Commit](http://commits.kde.org/konqueror/d1136849b21412254ec913496d76229643bac6e0). Fixes bug [#492188](https://bugs.kde.org/492188).
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Update Hafas feature code mapping for codes observed at BLS. [Commit](http://commits.kde.org/kpublictransport/86aa2b8775010413dd24ae23431d2e0f2c5707ff).
+ Add Hafas vehicle feature codes used by NASA. [Commit](http://commits.kde.org/kpublictransport/339b1d86740a7987c18fa5c3429b4aeb48683b1c).
+ Downgrade coverage quality for France for ÖBB. [Commit](http://commits.kde.org/kpublictransport/2f09529357c0ebd49a010049fb722447b06baac0).
+ Add more Hafas feature codes observed in Switzerland. [Commit](http://commits.kde.org/kpublictransport/39e0730ad48ba09c8b4d624c6eeef350a7205bf4).
+ Update ZVV API key. [Commit](http://commits.kde.org/kpublictransport/3abc95d6858eeb32f1ee6aa5a263b20afe1625e5).
{{< /details >}}
{{< details id="kwalletmanager" title="kwalletmanager" link="https://commits.kde.org/kwalletmanager" >}}
+ Fix service file name. [Commit](http://commits.kde.org/kwalletmanager/9b25d58f40a3ecf0fb41200ff735fef158f3925f). Fixes bug [#492138](https://bugs.kde.org/492138).
{{< /details >}}
{{< details id="lokalize" title="lokalize" link="https://commits.kde.org/lokalize" >}}
+ Call updateHeader with UTF-8 to reduce noise by Scripty. [Commit](http://commits.kde.org/lokalize/e6ecc65191a0bac20419481dd125e57e8d1f13f1).
+ Remove new lines at EOF. [Commit](http://commits.kde.org/lokalize/9bebb53d808ba0cc2118785b7c4f3f0faf6eec7a).
+ Fix Translation Memory import job not finishing. [Commit](http://commits.kde.org/lokalize/aa584b665bb2c0d8bf8468dbc704dab66f236e05). Fixes bug [#451461](https://bugs.kde.org/451461).
+ Fix QLocale language loop QLocale::Akoose is not the last language anymore. [Commit](http://commits.kde.org/lokalize/e4d77e5be0039f1ecc97638a1db68196857e56d4).
+ Be specific in which file is not being indexed. [Commit](http://commits.kde.org/lokalize/4a6c7590f959114c665092a40824b69290763d2b).
{{< /details >}}
{{< details id="merkuro" title="merkuro" link="https://commits.kde.org/merkuro" >}}
+ Don't register contactconfig manually. [Commit](http://commits.kde.org/merkuro/3a45b142eca88d781b9dfdbae9a6b2d7bfab88f7).
+ Compile contacts plugin metadata in the actual QML plugin, not in the shared library. [Commit](http://commits.kde.org/merkuro/9f175f2973fea624c1f4de1fda26f64ffb236ec0). Fixes bug [#491808](https://bugs.kde.org/491808).
+ Add missing GENERATE_PLUGIN_SOURCE to merkuro_contact_plugin. [Commit](http://commits.kde.org/merkuro/23b279548f8229d624ccf790daf8a19600993a29).
{{< /details >}}
{{< details id="neochat" title="neochat" link="https://commits.kde.org/neochat" >}}
+ Web Shortcuts: kcmshell5 does not exist anymore on Plasma 6. [Commit](http://commits.kde.org/neochat/b15ee49691d9ac9b9d75717c2f36f2032cd35529).
+ LocationHelper: Move clamp from zoomToFit to QML. [Commit](http://commits.kde.org/neochat/1c0a5edd2e911a2cea529cc719df155f8f5ec29a).
+ AuthorDelegate: Don't make empty space clickable. [Commit](http://commits.kde.org/neochat/47408d536ddfefd8f18db3556ce484ccd40c2af1).
+ AuthorDelegate: Add pointing hand cursor to indicate you can tap. [Commit](http://commits.kde.org/neochat/fc21eea7e7dfc749b2db9da7561b7b24a5cc3d8f).
+ Make the SectionDelegate look just a little bit better. [Commit](http://commits.kde.org/neochat/ca95eb3505fdde80d67b4873ed75dd2000fffcad).
+ Fix pending indicator. [Commit](http://commits.kde.org/neochat/22d0d84fee56cd8265bd863506206f4c56ae8986).
+ Use plaintext for reply author component. [Commit](http://commits.kde.org/neochat/941a7243819f87ead159a274bfea3a8ea385393c).
+ Don't run QtKeychain job in a nested event loop. [Commit](http://commits.kde.org/neochat/7e880c66632232951421acd16d310ace2063ec06).
+ Adapt to libQuotient API change. [Commit](http://commits.kde.org/neochat/22315b810cb06c57cf20f510e097be1352d2bf23).
+ Ensure that room list does not show rooms without title during startup. [Commit](http://commits.kde.org/neochat/d0c41d522474120be388a597a3eab9b6dba8d89d).
+ Include missing ECMQmlModule. [Commit](http://commits.kde.org/neochat/a7e06375fddf7927b0b2bb5e8f986ace9d9a57eb).
+ Re-add pending event indicator. [Commit](http://commits.kde.org/neochat/b34936a0173fab6a4aa157e0e16126e62522fb5e). Fixes bug [#491277](https://bugs.kde.org/491277).
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Set the Form tooltip if there's one. [Commit](http://commits.kde.org/okular/d4b9eb63e9bd59299c7b5f096970295dd62947b1). See bug [#486681](https://bugs.kde.org/486681).
{{< /details >}}
{{< details id="plasmatube" title="plasmatube" link="https://commits.kde.org/plasmatube" >}}
+ Piped: fix filter search parameter. [Commit](http://commits.kde.org/plasmatube/3a65891247e8759468e5ea42a34b581554e46446).
+ Flatpak: Update LuaJIT to a snapshot. [Commit](http://commits.kde.org/plasmatube/edfc47b26c7f66ac57608e909e39e43d347fa5fd).
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Set fallback icon theme. [Commit](http://commits.kde.org/spectacle/401c34ce457514cd94eadd5983de942f2c5aefc1). Fixes bug [#492824](https://bugs.kde.org/492824).
+ Fix InlineMessage headers/appearance on "no screenshot" dialog. [Commit](http://commits.kde.org/spectacle/ea4234db53de0ca5ae7885f2a5388c3477dc9200).
+ Set suggested filename for copied images. [Commit](http://commits.kde.org/spectacle/3fe86edf6e15a66ad4b7631406f9703b3e81f491).
+ Go back to copying images with setImageData. [Commit](http://commits.kde.org/spectacle/92a59d38ddf1c620a9c318d3f109f202c12c76d4). Fixes bug [#485096](https://bugs.kde.org/485096). See bug [#465781](https://bugs.kde.org/465781). See bug [#465972](https://bugs.kde.org/465972).
{{< /details >}}
{{< details id="tokodon" title="tokodon" link="https://commits.kde.org/tokodon" >}}
+ Stop opening the sidebar on startup. [Commit](http://commits.kde.org/tokodon/26c04ead9b9553fdc4b183636b4db254d28d136e). Fixes bug [#492030](https://bugs.kde.org/492030).
+ Fix registration not working. [Commit](http://commits.kde.org/tokodon/0213690ffb07b7c75b3119ca00c12365d6c54670).
+ Fix erroneous if check that was using the wrong arguments list. [Commit](http://commits.kde.org/tokodon/3ffdeb95a10e7be7bfc370d289f9e71410ee0d3a).
+ Add KCrash. [Commit](http://commits.kde.org/tokodon/a724959aa6137b5554d07609eb5c850861e9136c).
+ Mention everyone in replies. [Commit](http://commits.kde.org/tokodon/47cecb9156e0737ae5f1d3dc72182afd5e2f0e0c).
+ Do not reload accounts when initially setting the application proxy. [Commit](http://commits.kde.org/tokodon/469ee2325aabbd294bf4f1358786c0f7db68b471). Fixes bug [#492383](https://bugs.kde.org/492383).
+ Only emit reload accounts signal if they actually were. [Commit](http://commits.kde.org/tokodon/5950882cbe1bc264cb4b6dac1c5ddae72ed0a6c1). See bug [#492383](https://bugs.kde.org/492383).
+ Fix creating new account on mobile. [Commit](http://commits.kde.org/tokodon/53b795f762634957b1df66d04d62153775720416). Fixes bug [#491928](https://bugs.kde.org/491928).
+ Flatpak: Update LuaJIT to a snapshot. [Commit](http://commits.kde.org/tokodon/a88f79bf7f6e86428925d35ab63f426cba453f26).
+ Close modal sidebar when clicking on an entry. [Commit](http://commits.kde.org/tokodon/cd69b060735921fae06cb80fb0759dd192212b2e). Fixes bug [#491929](https://bugs.kde.org/491929).
{{< /details >}}
{{< details id="umbrello" title="umbrello" link="https://commits.kde.org/umbrello" >}}
+ Fix building png images from svg files caused by broken attribute 'xlink:href="#(null)"'. [Commit](http://commits.kde.org/umbrello/461968b265df7a861b3b0282f42180c20aef1a82). Fixes bug [#492246](https://bugs.kde.org/492246).
+ Cmake: fix building svg2png with Qt5. [Commit](http://commits.kde.org/umbrello/61fc72a35b06ba3234b8728fe9f18f07eeba06a8). See bug [#492246](https://bugs.kde.org/492246).
{{< /details >}}
