---
title: Plasma 5.27.6 complete changelog
version: 5.27.6
hidden: true
plasma: true
type: fulllog
---
{{< details title="breeze-gtk" href="https://commits.kde.org/breeze-gtk" >}}
+ Gtk3: revert button padding hack. [Commit.](http://commits.kde.org/breeze-gtk/2f2923c2a5ce8b656e2f531bb0791c1ec105873a) See bug [#414777](https://bugs.kde.org/414777)
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Rpmostree: Simplify filter. [Commit.](http://commits.kde.org/discover/c35d4b5f53be3a8823e5db70cd59c0a47551eea2) Fixes bug [#470372](https://bugs.kde.org/470372)
+ Add math.max back. [Commit.](http://commits.kde.org/discover/e27fe8a1a0865cc6b832e60683c2420a94f1ce89) 
+ Fix broken checkbox pixel alignment. [Commit.](http://commits.kde.org/discover/9bddbdd44a8c2fd5b0c2257737da216227d35eef) Fixes bug [#471067](https://bugs.kde.org/471067)
+ FlatpakBackend: Update appstream cache before initializing. [Commit.](http://commits.kde.org/discover/48f40d6e22cba9f03af3195b0acb4be2575b0793) See bug [#448521](https://bugs.kde.org/448521)
+ ApplicationPage: make heading/short description text selectable. [Commit.](http://commits.kde.org/discover/51f3a98a9587de37dcba0108e153d470039821ce) Fixes bug [#470297](https://bugs.kde.org/470297)
+ Appdata: remove duplicate entry. [Commit.](http://commits.kde.org/discover/d93b48db2577f6de8abda4aab21fd6f8f66104e7) 
+ Flatpak: Make sure we are refreshing the appstream metadata. [Commit.](http://commits.kde.org/discover/7b5cc5afa74b4c217a6643ef1cf395676fb25518) 
+ Pk: Actually start the launch job. [Commit.](http://commits.kde.org/discover/c9d1f794a4c923a803b9549897f768cb58a7390d) Fixes bug [#466709](https://bugs.kde.org/466709)
+ Fwupd: don't refresh directory-type remotes on setup. [Commit.](http://commits.kde.org/discover/c51bbc828465557915b806cdaf15632c7f2256b8) 
{{< /details >}}

{{< details title="Flatpak Permissions" href="https://commits.kde.org/flatpak-kcm" >}}
+ Add KItemModels to dependencies. [Commit.](http://commits.kde.org/flatpak-kcm/568f08ba427ecd35fce9f02974022860baa2a9d5) 
+ FlatpakPermissionModel: Merge defaults from a list of overrides. [Commit.](http://commits.kde.org/flatpak-kcm/6c0616c12632952c66fc0b8100b3ae40913634b4) Fixes bug [#466997](https://bugs.kde.org/466997)
+ FlatpakReference: Port metadata and overrides to a list of files. [Commit.](http://commits.kde.org/flatpak-kcm/dd378eb8c7aa6aba2a36893df0189426a8ba211b) 
{{< /details >}}

{{< details title="kde-cli-tools" href="https://commits.kde.org/kde-cli-tools" >}}
+ Kinfo: Use kcmshell5 with Plasma *5*. [Commit.](http://commits.kde.org/kde-cli-tools/726690b34c1b75307fb8c830ba05dd00b98aa85a) 
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Gsettings: use `const char*` directly. [Commit.](http://commits.kde.org/kde-gtk-config/32dfdcb31496389614ab95986c2d058ca2e8a6a3) 
+ Kded: update `scaling-factor` when setting global scale. [Commit.](http://commits.kde.org/kde-gtk-config/53aa07cf6eae02462ae6bdb9caa58d038b379748) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Color Picker: Round color values before formatting them. [Commit.](http://commits.kde.org/kdeplasma-addons/3f672fe4a989fe99f820a696ef7a764c5b38287b) 
+ Alternatecalendar: set dayLabel for Qt calendar providers. [Commit.](http://commits.kde.org/kdeplasma-addons/a9bc7061eec365d35650cf9fb83d71d1fa2c4d29) Fixes bug [#470034](https://bugs.kde.org/470034)
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Add boundaries to resolution text. [Commit.](http://commits.kde.org/kscreen/4614c0e72889f5e63f5e09a0aebd289630ed7634) Fixes bug [#460397](https://bugs.kde.org/460397)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Xwayland: Remove cursor definition. [Commit.](http://commits.kde.org/kwin/fb238b25d126df6f1b7a48ddbdef10b7cc572f60) Fixes bug [#442839](https://bugs.kde.org/442839). Fixes bug [#459468](https://bugs.kde.org/459468)
+ Screencast: Discard pending buffer and fence if stream state changes. [Commit.](http://commits.kde.org/kwin/777ae88e78e6ee1b66a5e61ac811a0b9e6e0ceee) 
+ Screen edge: Emit cleanup signals if needed on teardown. [Commit.](http://commits.kde.org/kwin/c89c892a0184204f69a37853ab6de2fa43acd2ff) Fixes bug [#403354](https://bugs.kde.org/403354)
+ Effects/magiclamp: Make it look good with hidden panels. [Commit.](http://commits.kde.org/kwin/f2e08f0d259ab42ecfdfa04a5a918fb222fb4869) 
+ XdgPopupWindow: Allow position to be set by Plasma. [Commit.](http://commits.kde.org/kwin/4d4769b8c5b3ddd5b3bfae6dc7fb4cf7057127a6) Fixes bug [#463272](https://bugs.kde.org/463272)
+ Plugins/qpa: initialize buffer in backingstore. [Commit.](http://commits.kde.org/kwin/ba5230659bf61a4e927d67901cb51291e646c018) Fixes bug [#437062](https://bugs.kde.org/437062)
+ Tiles: Adjust padding between windows. [Commit.](http://commits.kde.org/kwin/1f2c1ec9b3b2f3bee5a78eb2ee3ee6097e9b3b87) Fixes bug [#469720](https://bugs.kde.org/469720)
+ Refine order of previous/next screens in Workspace::findOutput. [Commit.](http://commits.kde.org/kwin/736d90787b43697beb50e922fb8d3e8a7682329a) Fixes bug [#467996](https://bugs.kde.org/467996)
+ Effects/magiclamp: Improve code readability. [Commit.](http://commits.kde.org/kwin/156778c554b4ce194a5ad74611e1276495e96a66) 
+ Effects/magiclamp: Make it look good with floating panels. [Commit.](http://commits.kde.org/kwin/e749d285feccc91111255155b2ee2d21cb00b205) Fixes bug [#361121](https://bugs.kde.org/361121). Fixes bug [#466177](https://bugs.kde.org/466177)
+ Effects/magiclamp: Improve animation direction heuristic. [Commit.](http://commits.kde.org/kwin/575f56b5e6bebc3861cde28631acc3b927c310e1) Fixes bug [#463581](https://bugs.kde.org/463581)
+ Revert "wayland: Avoid pointer warp if constraint region is invalid". [Commit.](http://commits.kde.org/kwin/3d62e7548955e2ddf298c61a55fcd26007ea27bb) See bug [#457021](https://bugs.kde.org/457021). Fixes bug [#469555](https://bugs.kde.org/469555)
+ Xcbutils: fix nativeFloor. [Commit.](http://commits.kde.org/kwin/2237391e979b2423462bb5eaf1fdf4c0876f7476) Fixes bug [#459373](https://bugs.kde.org/459373)
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Dpms: Don't crash if trying to interact with the fake screen. [Commit.](http://commits.kde.org/libkscreen/56f0286a1adc70d56b8ce44cabe0a3ffd7ee8d9a) Fixes bug [#470484](https://bugs.kde.org/470484)
+ Fix the wrong argument in usage example. [Commit.](http://commits.kde.org/libkscreen/e0298b15cd28979d1cd7b42501e208018f154350) 
+ XRandR: Avoid a null pointer dereference in crtcChanged(). [Commit.](http://commits.kde.org/libkscreen/e23ccb5063e1ae70c6eae9df4875dbd9fe4e72e9) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Panel: mitigate plasmashell high CPU usage when moving windows. [Commit.](http://commits.kde.org/plasma-desktop/14d56ef4eecea4e6623ba45409e4e4042fcb5938) 
+ Desktop: set accentColor binding enabled condition. [Commit.](http://commits.kde.org/plasma-desktop/cd05ae276c5fb95b129ffd1b2bf651549afe5a48) See bug [#470280](https://bugs.kde.org/470280)
+ Applets/kickoff: consider `devicePixelRatio` in `minimumGridRowCount`. [Commit.](http://commits.kde.org/plasma-desktop/c7baeb8f99543f39da92be1630ca7facc9fb9a66) 
+ Applets/kimpanel: address QStringLiteral data duplication. [Commit.](http://commits.kde.org/plasma-desktop/d06dda5163b510d4398cfd587ab4a4eec51b8abe) 
+ Applets/taskmanager: subscribe to windowViewAvailableChanged signal. [Commit.](http://commits.kde.org/plasma-desktop/cc1c297b0cef927012eb5b8a728b2f244ac72908) Fixes bug [#469731](https://bugs.kde.org/469731)
+ Knetattach: don't store username in WebDAV URL. [Commit.](http://commits.kde.org/plasma-desktop/2257a6cbf51efd55efd77af163e82cf91103dadd) See bug [#430894](https://bugs.kde.org/430894)
+ Applets/taskmanager: avoid updating dragSource when active is false. [Commit.](http://commits.kde.org/plasma-desktop/42b671f8575d9df27f740c704fe98a0471efbda5) Fixes bug [#466675](https://bugs.kde.org/466675)
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Applet: Fix crash when opening context menu with no contents. [Commit.](http://commits.kde.org/plasma-pa/9c84c3264ae3eb47cc7e6f0c47c0e7c8cecf4075) Fixes bug [#470847](https://bugs.kde.org/470847)
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Fix layout of Add New Page dialog with wordy languages. [Commit.](http://commits.kde.org/plasma-systemmonitor/6ae7d92ba55963c660e84b719d4ba6864eac7ce1) Fixes bug [#470726](https://bugs.kde.org/470726)
{{< /details >}}

{{< details title="plasma-thunderbolt" href="https://commits.kde.org/plasma-thunderbolt" >}}
+ Fix UI with huge window sizes. [Commit.](http://commits.kde.org/plasma-thunderbolt/d8474c7594093b9cb58ea618a1c029425a3327cb) Fixes bug [#461102](https://bugs.kde.org/461102)
{{< /details >}}

{{< details title="plasma-welcome" href="https://commits.kde.org/plasma-welcome" >}}
+ Remove duplicated entry. [Commit.](http://commits.kde.org/plasma-welcome/f2f7df269db08c72d9a2fe6390888d53860583df) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Applets/kicker: fix test error in testrunnermodel. [Commit.](http://commits.kde.org/plasma-workspace/1ab5a7adaed9e45e1a515cb1adcc700de01e9388) 
+ Kcms/krdb: Set X root window cursor. [Commit.](http://commits.kde.org/plasma-workspace/c0f47614a3b2364ebe35abf10a2f8d697a224f38) 
+ Shell: make accent color optional, and enable accent color after slot connection. [Commit.](http://commits.kde.org/plasma-workspace/4c08bf02d42b3e67854640e4c2ee3a712adf3099) Fixes bug [#470280](https://bugs.kde.org/470280)
+ Libtaskmanager: fix copy leftover in document. [Commit.](http://commits.kde.org/plasma-workspace/f0d9288903dfbfbc56d24b5e11de817327432b4f) 
+ Runners/calculator: implement thread-safety in QalculateEngine::evaluate. [Commit.](http://commits.kde.org/plasma-workspace/9d18e0821455366c00a763252515d48741316f6c) Fixes bug [#470219](https://bugs.kde.org/470219)
+ Applets/batterymonitor: Fix wrong tooltip when plugged in but discharging. [Commit.](http://commits.kde.org/plasma-workspace/98f39bbce64e526679f7c9195f1282fa972dedaa) Fixes bug [#470632](https://bugs.kde.org/470632)
+ Applets/mediacontroller: add busy indicator for remote images. [Commit.](http://commits.kde.org/plasma-workspace/294cb5c44bdd8b4147d94d5406af76bf468edb3f) 
+ Widgetexplorer: Properly handle translations for applet categories. [Commit.](http://commits.kde.org/plasma-workspace/7883535cfbeb63b775f50335607d8cec375ab7fe) Fixes bug [#460523](https://bugs.kde.org/460523)
+ Wallpapers/image: resize svg wallpaper to requested size. [Commit.](http://commits.kde.org/plasma-workspace/987238ac7b414a40d0f10d252b8d73cdb1f0f865) Fixes bug [#469294](https://bugs.kde.org/469294)
+ Klipper config: set UI state when loading the same way as when editing. [Commit.](http://commits.kde.org/plasma-workspace/1e25653e368555fc61dff4c6d604df1dcec7b55c) 
+ Holidaysevents: reload event data when config changes. [Commit.](http://commits.kde.org/plasma-workspace/bdbf04207fed6f88a041a76c5797cf51272a72ee) Fixes bug [#437654](https://bugs.kde.org/437654)
+ Runners/services: Do not match categories for short queries. [Commit.](http://commits.kde.org/plasma-workspace/fc675e7f4690d690e46ef6d8ef4dc47dbe56d643) Fixes bug [#469769](https://bugs.kde.org/469769)
+ Components/calendar: reset label under Gregorian date when plugin list changes. [Commit.](http://commits.kde.org/plasma-workspace/bafb4157125bcff0f9ca1ab5f3d5d1ed40a68cda) See bug [#469935](https://bugs.kde.org/469935)
+ Components/calendar: use QHash::contains instead of QHash::count. [Commit.](http://commits.kde.org/plasma-workspace/8b98571ceb0285e89f403ba3046be63a08232f84) 
+ Applets/digital-clock: show today's sublabel in tooltip if there is one. [Commit.](http://commits.kde.org/plasma-workspace/6b56933ea51ef0cb6929ae95758d1ec58350e1e2) See bug [#469935](https://bugs.kde.org/469935)
+ Applets/systemtray: fix highlight being on top of first column items. [Commit.](http://commits.kde.org/plasma-workspace/0ae0fbecf223c55bc6c713dbc504acb0e5927194) Fixes bug [#456180](https://bugs.kde.org/456180)
+ Applets/digital-clock: Fix localization for TimeZoneModel. [Commit.](http://commits.kde.org/plasma-workspace/adc559663bd5ac457b7ebb89857037c2572b5197) Fixes bug [#469196](https://bugs.kde.org/469196)
+ Applets/digital-clock: Allow event titles to be wrapped. [Commit.](http://commits.kde.org/plasma-workspace/51c98e60a1e28218e86faa12284845a228264b04) 
+ Export a new helper function that read Xft.dpi value. [Commit.](http://commits.kde.org/plasma-workspace/355c6a306c3c9dfdb7fd7777a48e5ce583c706f8) 
+ Libtaskmanager: Fix appmenu caching. [Commit.](http://commits.kde.org/plasma-workspace/c09da5100aad95a49ba274729b144f6cb89e4706) Fixes bug [#422786](https://bugs.kde.org/422786)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Fix ddcutil for laptop user. [Commit.](http://commits.kde.org/powerdevil/ddcabaf93712b953e3ec327416e0332d7e44fc40) Fixes bug [#469895](https://bugs.kde.org/469895)
+ Take property update frequency into account for smoothed remaining time. [Commit.](http://commits.kde.org/powerdevil/ee95711d58ed862da4a1dbccd797e7aab83bc04d) 
+ Delay Solid::Battery instatiation until actually required. [Commit.](http://commits.kde.org/powerdevil/acaa7fb52ce0a326949f8eedb2e7611f039da266) 
+ Avoid repeated Solid::Battery instantiations on charge level change. [Commit.](http://commits.kde.org/powerdevil/3c32d284e83081f075f1e142d42d47cc36df8f51) 
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Fix showing error messages. [Commit.](http://commits.kde.org/sddm-kcm/d8ca15e88a15df26859a79a376056a222f7d63bd) Fixes bug [#471040](https://bugs.kde.org/471040)
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Improve keyboard navigation in sidebar. [Commit.](http://commits.kde.org/systemsettings/0caf03f596ca6a9d5effdbf0672a32bd54102e31) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Return correct user info. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/d95f420cecb70890502f3152e3eb4429456a5ce9) Fixes bug [#469697](https://bugs.kde.org/469697)
{{< /details >}}

