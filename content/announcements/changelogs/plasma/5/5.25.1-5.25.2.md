---
title: Plasma 5.25.2 complete changelog
version: 5.25.2
hidden: true
plasma: true
type: fulllog
---
{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/kscreen/0da1aa74f8135163d8b9d5f903b985cc39b63002) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/kscreen/6528d1e57a9f71c366ec895c788e4e87aeef923d) 
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/kscreenlocker/4f8299bd63709b443689ef2d7adcb79180b573ad) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/kscreenlocker/8ab36849ca016b25abe6c29071b400757b4ea20b) 
+ Greeter: fix undefined wallpaper by initializing wallpaper plugin before lock screen. [Commit.](http://commits.kde.org/kscreenlocker/c12e170903092a4edfdb32403657494ee6a2471c) 
+ Initialise pam response in all converse conditions. [Commit.](http://commits.kde.org/kscreenlocker/13dfae3fbc7e4d7e8b6692aed65baa272252ec36) Fixes bug [#455608](https://bugs.kde.org/455608)
{{< /details >}}

{{< details title="KSSHAskPass" href="https://commits.kde.org/ksshaskpass" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/ksshaskpass/e555b5c5da61b66c436f279645cd90030b7220de) 
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/ksystemstats/3cd38738dfafc30970715576af4dc8beaeed7cf1) 
{{< /details >}}

{{< details title="kwayland-integration" href="https://commits.kde.org/kwayland-integration" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/kwayland-integration/45284dd0097cbcfa31b6f50df94a44bb1f80299c) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Backends/drm: fix flicker with rotation on mobile. [Commit.](http://commits.kde.org/kwin/d40d3493ddc336e1bee8b3399db05049b602149d) 
+ Tablet: Leave the surface we were previously on, not the one we are going to. [Commit.](http://commits.kde.org/kwin/98eb866418799ffccd501e15fcfd50bf834d1b15) 
+ Backends/drm: ensure modeset properties are reset properly. [Commit.](http://commits.kde.org/kwin/3bac0cd07f0f4e7964056afaa08a32dba147859f) 
+ Effects/slide: Save correct current position. [Commit.](http://commits.kde.org/kwin/680fd236ee7eeb27ded309f8398a15815d37051c) 
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/kwin/72ca518407be9c38ad32680a42ed624449b9f929) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/kwin/2e8e04c6c31a3204ecf6e6d4f249942b957685d7) 
+ Effects: Set timestamp for input events. [Commit.](http://commits.kde.org/kwin/e6ae8fb6755398d3512863c7de6493360962118e) Fixes bug [#454275](https://bugs.kde.org/454275). Fixes bug [#449907](https://bugs.kde.org/449907)
+ Internal tracking for quick effect item focus. [Commit.](http://commits.kde.org/kwin/ea5bc72dc27562b99b81f2b3ddb855468353896e) Fixes bug [#455807](https://bugs.kde.org/455807). Fixes bug [#455783](https://bugs.kde.org/455783). See bug [#455633](https://bugs.kde.org/455633)
+ Move WindowHeap delegate to own file. [Commit.](http://commits.kde.org/kwin/cd02c60f4608baed3533fe824a1f15c6b43d30ea) 
+ Delegate updateShadow to event loop. [Commit.](http://commits.kde.org/kwin/d24df924448490530e4e4ccd64641b15704cd140) 
+ Fix flaky testTextInputV3Interface. [Commit.](http://commits.kde.org/kwin/ad25bf05bba0df7df22ef6ed58e72382d28e7379) 
+ Windowview: Fix broken keyboard navigation while filtering. [Commit.](http://commits.kde.org/kwin/dcc77bfa8fbca8032047fbbb5ab65382293488ac) Fixes bug [#455633](https://bugs.kde.org/455633). Fixes bug [#455764](https://bugs.kde.org/455764). Fixes bug [#455099](https://bugs.kde.org/455099). Fixes bug [#455586](https://bugs.kde.org/455586). Fixes bug [#455753](https://bugs.kde.org/455753)
+ Revert "Move WindowHeap delegate to own file". [Commit.](http://commits.kde.org/kwin/35273ae5ba34f9a7842cb91c12264bb2aa0c4e2b) 
+ Move WindowHeap delegate to own file. [Commit.](http://commits.kde.org/kwin/1fa5b3f9164bcf9a51ed46597255fc36444fbeaa) 
+ Screencasting: No need to adjust the scale on the cursor. [Commit.](http://commits.kde.org/kwin/6bd9025bc763dcd7f05058cf5b77385ac9e96694) 
+ Screencasting: Properly disable the cursor when it exits the viewport. [Commit.](http://commits.kde.org/kwin/cebdc1cc7e413aeade6bce5e915dd55040ac903f) 
+ Backends/drm: fix enabled+inactive check. [Commit.](http://commits.kde.org/kwin/ea8b0d962be15fbf5c4c29ac4dd657ff34344e17) 
{{< /details >}}

{{< details title="kwrited" href="https://commits.kde.org/kwrited" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/kwrited/fdb99613fcaee65155294c7002a90fcb7c71f432) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/kwrited/745072fc01361165d62b0d59a67ac46134c71806) 
{{< /details >}}

{{< details title="layer-shell-qt" href="https://commits.kde.org/layer-shell-qt" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/layer-shell-qt/6e27a6ab42ca203fce90527b89f2fa19ef1ebbe1) 
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/libkscreen/6f727024437f5628349bf9fc35a45dfbc613a76a) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/libksysguard/6ce175e47c36d5aa6919534ec693401d1590ffae) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/libksysguard/8ac4d53d2a6210054318f8ff2cbbc5e87b8bc293) 
{{< /details >}}

{{< details title="Milou" href="https://commits.kde.org/milou" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/milou/8f1a888185261add92af4037f33066b732672706) 
{{< /details >}}

{{< details title="Oxygen" href="https://commits.kde.org/oxygen" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/oxygen/9d2e900098aa814a73812b482fb923d3c337b431) 
{{< /details >}}

{{< details title="oxygen-sounds" href="https://commits.kde.org/oxygen-sounds" >}}
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/oxygen-sounds/a8786bf95003bde03df699e016a9ce510f86c502) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Desktoppackage: enable Apply button only after the wallpaper plugin is changed. [Commit.](http://commits.kde.org/plasma-desktop/158d093c859512f7299f8e9e791081a155503b27) 
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-desktop/474a000aafe9ed70328d9c72793cb46b5b952bf0) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/plasma-desktop/5e366442c8cc95858f93080d7ef1e2ebe719dad6) 
+ Fix includes when building the kglobalaccel dbus interface. [Commit.](http://commits.kde.org/plasma-desktop/a5e1f2c023b98b130bccee1ebd1ee184c8a2f495) 
+ [applets/kicker] Fix missing highlight on Dashboard grids & list. [Commit.](http://commits.kde.org/plasma-desktop/8cd587800990e5ebe3987834822f1c1356e8c43d) Fixes bug [#453980](https://bugs.kde.org/453980)
+ Desktoppackage: avoid loading wallpaper settings again when the plugin is changed. [Commit.](http://commits.kde.org/plasma-desktop/f3867eb3d24e3cc4d03faf1d2ce2e8e8e0ab578c) See bug [#407619](https://bugs.kde.org/407619)
+ Break Toolbox into two rows when screen space is limited. [Commit.](http://commits.kde.org/plasma-desktop/123a4493cf75ba540269e9a38d0ffbdfe7a2becd) 
{{< /details >}}

{{< details title="Plasma Disks" href="https://commits.kde.org/plasma-disks" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-disks/98772af21c7937caf0bc2e7e23f770245b04f029) 
{{< /details >}}

{{< details title="Plasma Firewall" href="https://commits.kde.org/plasma-firewall" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-firewall/6f1db510b6160d42dc3763da07e39d3b3c700339) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/plasma-firewall/e560e0610e67cbb97f03a0ae60910a0fe888d9f0) 
+ Fix creating advanced rules. [Commit.](http://commits.kde.org/plasma-firewall/6cbaf60965a79fca0bb809b17505f0869a2d39cb) 
{{< /details >}}

{{< details title="plasma-integration" href="https://commits.kde.org/plasma-integration" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-integration/4654b036bed11c6ea8b93160bd2a8f921dd9916a) 
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Quicksettings: Fix brightness slider alignment, and forced row count. [Commit.](http://commits.kde.org/plasma-mobile/19f5a4e83129af3c25a6ffa3eac9f8e617b5f9da) 
+ Panel & taskpanel: Only opaque if windows are maximized. [Commit.](http://commits.kde.org/plasma-mobile/9c2777c61e10a1f7a8898bfc9a6f48af01d0e667) 
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-mobile/add5012f05e3d9a187870b81ccb904e3a03ba3d1) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/plasma-mobile/0dc0b348dfd57fb9f68ed7c1802a398168c6c238) 
+ Lockscreen: Improve physical keyboard input. [Commit.](http://commits.kde.org/plasma-mobile/7fd7f1ebaf6e518359770dd49a847722ed71b2b4) 
{{< /details >}}

{{< details title="Plasma Nano" href="https://commits.kde.org/plasma-nano" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-nano/32f3e2a152092f081f7086fa74daeda39bb356fd) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Applet: add keyboard navigation in QR window. [Commit.](http://commits.kde.org/plasma-nm/920a469b93ecfa4465539002e2a1bb9c2abc418f) See bug [#455950](https://bugs.kde.org/455950)
+ Applet" Fix showing network QR code button only for WiFi networks. [Commit.](http://commits.kde.org/plasma-nm/b34338bd897581b3c65fb9bc910b3a10f14a9cfd) Fixes bug [#454643](https://bugs.kde.org/454643). Fixes bug [#453806](https://bugs.kde.org/453806)
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-nm/5ad2b721e9727da250978f9361a62559715b4564) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/plasma-nm/bd49f27d3051a03c2fb96ab7f7c978dd654ab8af) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-pa/ab563ef74ceef246c63759118376622f056a182e) 
{{< /details >}}

{{< details title="Plasma SDK" href="https://commits.kde.org/plasma-sdk" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-sdk/01d44be7df91fdb760442b259c4198bd3bae0db3) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-systemmonitor/95438816b0702572beeb86c0695a418c0ee7c2f6) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/plasma-systemmonitor/818f1351dcaad26a3ef3a72a8230126ea68fe1bc) 
{{< /details >}}

{{< details title="plasma-tests" href="https://commits.kde.org/plasma-tests" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-tests/f95c79a15528b4152ca7cd95d1f5194d0388e936) 
{{< /details >}}

{{< details title="plasma-vault" href="https://commits.kde.org/plasma-vault" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-vault/020d88c232f6e1a600fe852420233752229b67f2) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/plasma-vault/47707c1bc45418beec2518404bba0a34ad109ca9) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plasma-workspace/64a5a4fa39dfddece4130518ee8c089c479f1399) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/plasma-workspace/e3d588999a62d8b39d8b643753c28fcfcb44ccee) 
+ Revert "Prevent panel going out of screen boundaries". [Commit.](http://commits.kde.org/plasma-workspace/68ed1677ac334dafb8a319bda419105c9d88adbb) See bug [#438114](https://bugs.kde.org/438114)
+ Shell: refresh geometries of all `DesktopView` and `PanelView` when receiving `logicalDotsPerInchChanged`. [Commit.](http://commits.kde.org/plasma-workspace/41fb821545aac4df273fb45d0e4de9ab49143d9c) Fixes bug [#450443](https://bugs.kde.org/450443). Fixes bug [#438114](https://bugs.kde.org/438114)
+ Avoid crash in plasma-session teardown. [Commit.](http://commits.kde.org/plasma-workspace/d159d05baab01b01c3524304b11265ebeb665ad1) Fixes bug [#454159](https://bugs.kde.org/454159)
+ Kcms/colors: Fix window titlebar tinting in colorsapplicator. [Commit.](http://commits.kde.org/plasma-workspace/b6b34f14901d8e493be4caa7f54c2dd36a2f0b46) Fixes bug [#455395](https://bugs.kde.org/455395)
+ Kcms/feedback: Properly fit the items inside the layout. [Commit.](http://commits.kde.org/plasma-workspace/9fa48bc540059a36853f8fd7036493edc3a99252) Fixes bug [#455713](https://bugs.kde.org/455713)
+ Fix session restore + kwin interaction race. [Commit.](http://commits.kde.org/plasma-workspace/93416bb280493d8173ab289903c12ab77c67c4ae) Fixes bug [#442380](https://bugs.kde.org/442380)
{{< /details >}}

{{< details title="plasma-workspace-wallpapers" href="https://commits.kde.org/plasma-workspace-wallpapers" >}}
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/plasma-workspace-wallpapers/339fb48420a7c6f6b8246622ab93f4b0d614fb78) 
{{< /details >}}

{{< details title="Plymouth KControl Module" href="https://commits.kde.org/plymouth-kcm" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/plymouth-kcm/64cc57bb804e4b36d8c9e11bc6d3b26f811839ce) 
{{< /details >}}

{{< details title="polkit-kde-agent-1" href="https://commits.kde.org/polkit-kde-agent-1" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/polkit-kde-agent-1/80c74a35489af7092a8425e735d72a336a4bb2dc) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/powerdevil/56ebeabbc2bb697850fb5d555986465098ce931a) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/powerdevil/e641ddf05e270d52e21425921b5ff9294e1b3059) 
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/sddm-kcm/a738a90dd6f15edd8944eac975710d9f14d8b7d2) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/systemsettings/b396d9ba5796b37bd5e95eb62b9fcecc4925389e) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/systemsettings/a08c877fb3b5f0c3c66ca3da103d02504a66321f) 
+ Make sidebar tooltips respect the "Display informational tooltips" global setting. [Commit.](http://commits.kde.org/systemsettings/aa827bd682b33b54828b8520b458a5b4a13c27c8) Fixes bug [#455073](https://bugs.kde.org/455073)
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Update qt5 version requirement to 5.15.2. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/1a3c3656752674f36df4a9866d33c6ff82906760) 
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/27877c1c3865deb7a37fba40edd194655f7bc22f) 
+ RemoteDesktopDialog: fix "withTouch" property mismatch. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/58c3a71fd212330de1d826b5c58e4efa4e2154bc) 
{{< /details >}}

