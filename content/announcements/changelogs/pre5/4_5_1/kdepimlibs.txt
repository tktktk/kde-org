------------------------------------------------------------------------
r1168698 | mueller | 2010-08-27 09:09:49 +0100 (dv, 27 ago 2010) | 2 lines

bump version

------------------------------------------------------------------------
r1164942 | scripty | 2010-08-18 03:21:03 +0100 (dc, 18 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1164715 | skelly | 2010-08-17 15:19:24 +0100 (dt, 17 ago 2010) | 1 line

Make sure to finish one beginInsert/endInsert pair before processing a new one.
------------------------------------------------------------------------
r1164583 | tmcguire | 2010-08-17 09:16:13 +0100 (dt, 17 ago 2010) | 11 lines

SVN_MERGE:
Merged revisions 1154892 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/trunk/KDE/kdepimlibs

........
  r1154892 | ervin | 2010-07-26 15:21:04 +0200 (Mon, 26 Jul 2010) | 3 lines
  
  Avoid deadlocking when the socket is closed from the session inactivity
  detection.
........

------------------------------------------------------------------------
r1164362 | skelly | 2010-08-16 17:09:25 +0100 (dl, 16 ago 2010) | 1 line

Fix off-by-not error
------------------------------------------------------------------------
r1164345 | skelly | 2010-08-16 16:17:33 +0100 (dl, 16 ago 2010) | 3 lines

Work around QSortFilterProxyModel bug.

Buggy reporting was causing asserts in KDescendantsProxyModel.
------------------------------------------------------------------------
r1164343 | skelly | 2010-08-16 16:15:36 +0100 (dl, 16 ago 2010) | 1 line

Add some more asserts.
------------------------------------------------------------------------
r1164037 | winterz | 2010-08-15 17:31:59 +0100 (dg, 15 ago 2010) | 7 lines

backport SVN commit 1162867 by winterz from e35:

if the attachment is a URI and the attachment doesn't have a label,
then give the attachment anchor the name of the URI.
kolab/issue4005


------------------------------------------------------------------------
r1163120 | tokoe | 2010-08-13 11:46:02 +0100 (dv, 13 ago 2010) | 11 lines

Merged revisions 1163117 via svnmerge from 
svn+ssh://tokoe@svn.kde.org/home/kde/trunk/KDE/kdepimlibs

........
  r1163117 | bcooksley | 2010-08-13 12:34:43 +0200 (Fri, 13 Aug 2010) | 3 lines
  
  No need to constantly attempt to reconnect to the Akonadi Server.
  Reviewed-by: tokoe
  BUG: 247482
........

------------------------------------------------------------------------
r1162864 | smartins | 2010-08-12 22:50:36 +0100 (dj, 12 ago 2010) | 1 line

Don't leak memory.
------------------------------------------------------------------------
r1161661 | vkrause | 2010-08-10 15:47:12 +0100 (dt, 10 ago 2010) | 10 lines

Merged revisions 1161656 via svnmerge from 
svn+ssh://vkrause@svn.kde.org/home/kde/trunk/KDE/kdepimlibs

........
  r1161656 | vkrause | 2010-08-10 16:37:19 +0200 (Tue, 10 Aug 2010) | 3 lines
  
  Cancel subjobs instead of silently dropping them from our queue, there
  might be someone else waiting for result signals (such as ItemSync).
........

------------------------------------------------------------------------
r1161583 | vkrause | 2010-08-10 12:54:10 +0100 (dt, 10 ago 2010) | 11 lines

Merged revisions 1149395 via svnmerge from 
svn+ssh://vkrause@svn.kde.org/home/kde/trunk/KDE/kdepimlibs

........
  r1149395 | vkrause | 2010-07-13 12:16:39 +0200 (Tue, 13 Jul 2010) | 4 lines
  
  Prevent double emission of ItemSync::result(), which could cause all kinds
  of fancy side-effects. Adapt the unit test to notice this scenario
  correctly.
........

------------------------------------------------------------------------
