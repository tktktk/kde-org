2007-05-15 17:40 +0000 [r665053]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/uml.lsm: update

2007-05-21 19:59 +0000 [r667075]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/VERSION,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlview.cpp: Revert the
	  part of r663014 that introduced
	  m_{Widget,Message}List.setAutoDelete(true). The Undo mechanism
	  cannot deal with it. BUG:145762

2007-05-22 04:11 +0000 [r667185]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umlobject.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: resolveRef(): When
	  m_SecondaryId cannot be resolved and m_SecondaryFallback is empty
	  then set m_pSecondary to the artificial datatype "undef". This is
	  somewhat less friendly on loading older Umbrello files but avoids
	  the creation of bogus objects. The better way to solve this bug
	  would be to warn the user when he attempts to delete model
	  objects that are still referenced somewhere in the model but I'm
	  afraid I cannot implement that right now. CCBUG:141279 BUG:145709

2007-05-25 05:43 +0000 [r668117]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pythonwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: Use
	  CodeGenerator::formatDoc() for formatting the string returned by
	  UMLObject::getDoc() BUG:145918

2007-05-25 21:54 +0000 [r668297]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pythonwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pythonwriter.h:
	  writeAttributes(): New. Write attribute documentation, name, and
	  visibility in a Python comment section. BUG:145916

2007-05-26 18:30 +0000 [r668552]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/docgenerators/xhtmlgenerator.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/docgenerators/xmi2docbook.xsl,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: Add selector for
	  comment in template matching <UML:Attribute>. BUG:145972

2007-05-27 05:54 +0000 [r668653]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/cppimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: Apply attchment 20700
	  from Antoine Dopffer > The old code tried not to parse a file if
	  the file was already parsed but this > detection was done too
	  late (after the parsing of the dependences). > Modified
	  function-> CppImport::feedTheModel Thanks for the fix Antoine.
	  BUG:119125

2007-05-27 12:50 +0000 [r668736]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umldoc.cpp: apply
	  r667113 from trunk

2007-05-27 12:58 +0000 [r668740]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.cpp: sync
	  umllistview.cpp with trunk (commits 665608,665854,666779)

2007-05-28 21:39 +0000 [r669259]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: loadFromXMI(): Before
	  version 1.5.7, we forgot to write the "type" attribute of the
	  AssociationWidget in saveToXMI(). As a workaround on loading pre-
	  1.5.7 XMI files, use the type from the UMLAssociation. BUG:146064

2007-05-29 19:26 +0000 [r669599]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/entity.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifier.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/enum.cpp: Emit removal
	  signal before calling UMLObject::emitModified().

2007-05-30 23:18 +0000 [r669986]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/floatingtextwidgetcontroller.h:
	  typo fix, (C) 2007

2007-05-30 23:22 +0000 [r669987]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/floatingtextwidgetcontroller.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog:
	  FloatingTextWidgetController constructor: Initialize member
	  variables. This fixes the valgrind warnings exposed in attachment
	  20722. I have not been getting the reported crashes using r669259
	  of SVN branches/KDE/3.5/kdesdk/umbrello. Could someone please
	  verify that the bug is really fixed, thanks. BUG:146058

2007-06-05 05:53 +0000 [r671591]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/Makefile.am,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlnamespace.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/codegenfactory.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/model_utils.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/headings/heading.d
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/attribute.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/dwriter.cpp
	  (added), branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/THANKS,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/dwriter.h
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/headings/Makefile.am:
	  Partial backport of r670997 from trunk. Does not include the
	  advanced code generator as that is blocked by bug 126262.
	  BUG:124805

2007-06-05 22:21 +0000 [r671969]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/classifier.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: Emit addition signal
	  before calling UMLObject::emitModified() (cf. r669599) BUG:146367

2007-06-07 09:49 +0000 [r672498-672494]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/object_factory.cpp:
	  createUMLObject(ot_Datatype): Set parentPkg to Datatypes folder
	  when not given.

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.cpp:
	  setFloatingText(): Call setTextPosition() on newly constructed
	  FloatingTextWidget.

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidget.cpp:
	  activate(): Fix condition for entering into paste handling code.

2007-06-07 09:57 +0000 [r672500]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/umloperationdialog.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/operation.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/operation.h:
	  UMLOperation::addParm(type,name,initialValue,doc,kind): Unused,
	  remove.

2007-06-07 10:03 +0000 [r672504]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umldoc.cpp:
	  signalUMLObjectCreated(): This is the wrong place to call
	  setModified(). That should be done at the callers when object
	  creation and all its side effects (e.g. new widget in view, new
	  list view item, etc.) is finished.

2007-06-07 10:10 +0000 [r672510]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/entity.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entityattribute.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entity.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/attribute.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifier.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entityattribute.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/attribute.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifier.h:
	  UMLAttribute(parent,name,id,s,type,iv): Change "type" arg to
	  UMLObject*. UMLClassifier::createAttribute(): Add more parameters
	  to avoid calling separate setters which result in futile
	  modified() signals.

2007-06-07 18:21 +0000 [r672632]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umlview.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlview.cpp:
	  findAssocWidget(UMLWidget*,UMLWidget*): Add a further arg, the
	  role B name. This fixes association label duplications when
	  adding multiple attributes of the same type at a classifier, when
	  both that classifier and the attribute type classifier are
	  present on a class diagram. CCBUG:130172

2007-06-07 21:11 +0000 [r672693]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.cpp:
	  setName(): Patch by Danny Beullens <dbeullens_at_tiscalinet.be>
	  makes the name visible. Thanks Danny. (Too bad this didn't make
	  it into 1.5.71, released a few moments earlier)

2007-06-10 18:44 +0000 [r673643]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/classifierlistitem.h:
	  setType(): Make virtual.

2007-06-10 18:48 +0000 [r673645]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/operation.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/operation.h:
	  m_returnId: New. Store the xmi.id of the <UML:Parameter
	  kind="return"> to avoid gratuitous thrashing of the ID value.

2007-06-11 18:31 +0000 [r674094]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pascalwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/THANKS: writeClass(): Use the
	  classname instead of the fixed name "TObject". Thanks to Dmitry
	  N. Kurashkin for the patch. BUG:146676

2007-06-11 19:07 +0000 [r674103]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/VERSION,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: increase version
	  number

2007-06-13 20:44 +0000 [r675229]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: loadChildrenFromXMI,
	  (parent != item->parent()): Prohibit reparenting
	  m_datatypeFolder. The file loads now but it is very possible that
	  there are more weak spots. In that case please provide further
	  attachments and reopen. BUG:146748

2007-06-14 20:46 +0000 [r675723]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.cpp:
	  moveObject(): Don't physically delete listview items while
	  reparenting during load from XMI. CCBUG:146748

2007-06-18 11:21 +0000 [r677039]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/widget_factory.cpp:
	  makeWidgetFromXMI(): Restore backward compatibility tags. Due to
	  popular request ;)

2007-06-18 17:36 +0000 [r677219]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidget.cpp:
	  updateComponentSize(): Remove special casing code, precondition
	  for fixing CCBUG:146925

2007-06-18 17:47 +0000 [r677223-677222]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/activitywidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/activitywidget.h:
	  draw(): Do not UMLWidget::setPen(p) because that uses the
	  WidgetBase::m_LineWidth. This is the actual fix for the bug at
	  hand. But while at it, I made further fixes:
	  UMLWidget::m_bResizable: Set true unconditionally. constrain():
	  New. Override virtual method of UMLWidget for better resize
	  behavior. Activity_Type, Fork_DEPRECATED: Remove. BUG:146925

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/assocrules.cpp:
	  ActivityWidget::Activity_Type Fork_DEPRECATED was removed.

2007-06-19 05:55 +0000 [r677410]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/floatingtextwidgetcontroller.cpp:
	  moveWidgetBy(): Remove unwarranted return. CCBUG:137041

2007-06-19 06:01 +0000 [r677414]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidgetcontroller.cpp:
	  getPositionDifference(): Consider m_old{X,Y} on computing
	  new{X,Y}. Update m_old{X,Y} with the newly computed values.
	  BUG:137041

2007-06-20 03:59 +0000 [r677847]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/make-umbrello-release.sh: Use
	  svn2dist option --svn-root

2007-06-21 12:30 +0000 [r678447]  binner

	* branches/KDE/3.5/kdesdk/cervisia/cervisia.desktop,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umbrello.desktop: fix
	  invalid .desktop files

2007-06-22 06:14 +0000 [r678729]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog:
	  updateComponentSize(): Apparently this is invoked while loading.
	  That is bad because it spoils the loaded dimensions. BUG:147069

2007-06-25 20:42 +0000 [r680294]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerationpolicy.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codedocument.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubycodegenerationpolicypage.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubycodegenerationpolicy.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/dwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/xmlcodecomment.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeblock.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifierwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenobjectwithtextblocks.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/exportallviewsdialog.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/clipboard/umldrag.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/configurable.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/refactoring/refactoringassistant.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubycodecomment.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/enum.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entity.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/floatingtextwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlcanvasobject.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/listpopupmenu.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlobject.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/codegenerationpolicypage.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/adawriter.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppcodegenerator.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/artifact.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/docwindow.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/simplecodegenerator.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/activitywidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/component.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/codeeditor.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/worktoolbar.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppheaderclassdeclarationblock.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/defaultcodegenpolicypage.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifiercodedocument.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/assocrolepage.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javacodedocumentation.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/folder.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppcodegenerator.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifier.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/node.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/usecase.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/nodewidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javaclassdeclarationblock.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codecomment.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/textblock.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/codegenfactory.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/stereotype.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppcodecomment.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pascalwriter.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlviewimageexportermodel.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/actor.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/enumwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/kplayerslideraction.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javacodegenerationpolicypage.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/notewidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubycodedocumentation.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppcodegenerationpolicypage.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppcodedocumentation.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javacodegenerationpolicy.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/association.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppcodegenerationpolicy.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/assocgenpage.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppcodegenerationform.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerator.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/nativeimportbase.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/hierarchicalcodeblock.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/statewidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/forkjoinwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeblockwithcomments.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/package.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/boxwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubyclassdeclarationblock.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javacodecomment.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/linepath.h: apply trunk
	  commits 679186 (APIDOX) and 680247 (explicit constructors), EBN
	  fixes by Sharan

2007-06-26 05:53 +0000 [r680408]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerationpolicy.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/notewidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/association.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifiercodedocument.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerator.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/adawriter.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifierwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/package.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entityattribute.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistviewitem.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/linepath.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pascalwriter.h:
	  further ApiDox fixes

2007-06-26 19:17 +0000 [r680636]  weidendo

	* branches/KDE/3.5/kdesdk/kcachegrind/kcachegrind/treemap.cpp,
	  branches/KDE/3.5/kdesdk/kcachegrind/kcachegrind/treemap.h: Take
	  over fix for bug 145454 from fsview

2007-06-27 04:40 +0000 [r680734]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlview.cpp:
	  AssociationWidget constructor: Change the UMLAssociation*
	  umlassoc arg to UMLObject*.

2007-06-27 04:49 +0000 [r680735]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: mouseReleaseEvent():
	  Temporarily disallow ListPopupMenu for m_pObject types other than
	  Uml::ot_Association until somebody finds time to implement that.
	  Implementation would require certain controls in AssocPropDlg to
	  be made read-only for non-ot_Association types of m_pObject.
	  BUG:147202

2007-06-30 18:09 +0000 [r681900]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/docgenerators/docbookgenerator.cpp:
	  compile with KDE 3.3

2007-06-30 23:17 +0000 [r681952-681951]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.cpp:
	  popupMenuSel(), case mt_Externalize_Folder: Fix default extension
	  (.xml)

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/entity.cpp: Emit
	  UMLObject::emitModified() only AFTER all else is done, cf.
	  r671969

2007-07-12 21:33 +0000 [r687101]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerationpolicy.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/componentwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/association.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codedocument.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/datatypewidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/hierarchicalcodeblock.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/assocpage.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlview.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifierwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenobjectwithtextblocks.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/usecasewidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifierlistitem.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeclassfield.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/refactoring/refactoringassistant.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerationpolicy.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/attribute.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/widget_utils.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/classifierinfo.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/objectwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entity.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/floatingtextwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/actorwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlrole.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlcanvasobject.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlview.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/listpopupmenu.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlobject.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/classifierlistpage.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/refactoring/refactoringassistant.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/import_rose.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/toolbarstate.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/activitywidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entityattribute.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlviewimageexportermodel.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/artifactwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/floatingtextwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/folder.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umldoc.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppheadercodeaccessormethod.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/listpopupmenu.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlobject.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeblockwithcomments.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifier.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/messagewidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/nodewidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entityattribute.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/import_utils.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/toolbarstate.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistviewitem.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codedocument.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlviewimageexportermodel.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codedocumentlist.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/enumwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/notewidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/association.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/assocrules.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifier.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/petaltree2uml.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entitywidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/xmlschemawriter.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umldoc.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/hierarchicalcodeblock.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/statewidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/forkjoinwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeblockwithcomments.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifierlistitem.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/boxwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/widget_utils.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/attribute.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/packagewidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeclassfield.h: sync
	  with trunk (681948:686570)

2007-07-14 02:45 +0000 [r687613]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/linepath.cpp:
	  setPoint(): Ignore request for (0,0).

2007-07-14 02:51 +0000 [r687614]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidgetcontroller.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidgetcontroller.h:
	  mouseMoveEvent(): UMLWidget::adjustUnselectedAssocs() expects
	  absolute coordinate. getPosition(): New method, factored from
	  getPositionDifference(), returns the absolute coordinate.
	  getPositionDifference(): Use getPosition(). CCBUG:147810

2007-07-15 09:39 +0000 [r688175]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidgetcontroller.cpp:
	  mouseMoveEvent(): Use raw pos of m_widget at call to
	  UMLWidget::adjustUnselectedAssocs() BUG:147810

2007-07-15 10:54 +0000 [r688198]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/clipboard/umlclipboard.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/kdevcppparser/cpptree2uml.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/association.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/clipboard/umlclipboard.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifier.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifierwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codedocument.cpp: minor
	  sync with trunk

2007-07-16 20:54 +0000 [r688793]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/attribute.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/attribute.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlobject.h:
	  UMLObject::set{Name,Visibility}: Make virtual.
	  UMLAttribute::attributeChanged(): New signal.
	  UMLAttribute::set{Name,Visibility}: New. Reimplement methods from
	  UMLObject in order to emit extra signal attributeChanged().
	  AssociationWidget::slotAttributeChanged(): New slot. Connects to
	  UMLAttribute:: attributeChanged() when the AssociationWidget
	  represents an UMLAttribute. The extra signal is necessary because
	  the attribute name must already have been updated at the
	  AssociationWidget by the time UMLObject::emitModified() is called
	  else UMLView::createAutoAttributeAssociations() will think it
	  needs to create a new attribute association. BUG:147919

2007-07-18 20:25 +0000 [r689658-689657]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umldoc.cpp:
	  currentRoot(): m_pCurrentRoot being NULL is a hard error.

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umlrole.cpp: add
	  usefulness to error messages

2007-07-18 22:29 +0000 [r689706]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/association.cpp:
	  init(): Do not call UMLDoc::currentRoot() while loading.

2007-07-20 20:37 +0000 [r690369]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistviewitem.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/listpopupmenu.cpp:
	  apply r690331 from trunk with slight modification

2007-07-20 20:47 +0000 [r690372]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/enum.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/listpopupmenu.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/enum.h: apply r690368
	  from trunk

2007-07-21 17:36 +0000 [r690647]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umlobject.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: loadFromXMI(): If the
	  xmi.id of an UMLRole is already in use then generate a new one.
	  CCBUG:147988

2007-07-24 22:10 +0000 [r692032]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/model_utils.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umldoc.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlobject.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlobject.h:
	  UMLObject::loadStereotype(): New, factored from loadFromXMI().
	  Further adjustments for loading attachment 21243 (Netbeans XMI)
	  CCBUG:56184

2007-07-30 19:32 +0000 [r694452]  weidendo

	* branches/KDE/3.5/kdesdk/kcachegrind/kcachegrind/instrview.cpp:
	  kcachegrind: backport fix from 694392 to 3.5 branch

2007-08-03 05:54 +0000 [r695857]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/VERSION,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: go with KDE 3.5.8

2007-08-11 23:36 +0000 [r699077-699076]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppheadercodeaccessormethod.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppsourcecodeaccessormethod.cpp:
	  remove superfluous initializations

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/floatingtextwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/floatingtextwidgetcontroller.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/model_utils.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/csharpwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/cppimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/widgetbase.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifier.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlcanvasobject.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifierwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/node.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/clipboard/umldrag.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codemethodblock.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenobjectwithtextblocks.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifierlistitem.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/activitywidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/artifact.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/import_rose.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistviewitem.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/simplecodegenerator.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/import_utils.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/component.h: move
	  closer to trunk

2007-08-11 23:40 +0000 [r699079]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javawriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubyclassifiercodedocument.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javaclassifiercodedocument.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/dwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javawriter.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javacodegenerator.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubycodegenerator.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppwriter.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/codegen_utils.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubyclassifiercodedocument.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javaclassifiercodedocument.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/dwriter.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubycodegenerator.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javacodegenerator.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/codegen_utils.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javacodeaccessormethod.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubycodeaccessormethod.cpp:
	  Codegen_Utils::capitalizeFirstLetter(): New. Replace similar
	  methods replicated in various codegen classes.

2007-08-12 00:04 +0000 [r699091]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/dwriter.cpp:
	  remove gratuitous diff to trunk

2007-08-12 00:57 +0000 [r699102]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: slotFileSaveAs():
	  Apply commit 695283 from trunk which works fine, thanks Sharan.
	  BUG:146061

2007-08-23 14:49 +0000 [r703890]  mueller

	* branches/KDE/3.5/kdesdk/kcachegrind/kcachegrind/hi48-app-kcachegrind.png:
	  fix app icon size

2007-09-11 19:03 +0000 [r711210]  mkoller

	* branches/KDE/3.5/kdesdk/cervisia/updateview_items.cpp: BUG:
	  148162 Qt-3.3.8 changed the parsing in QDateTime::fromString()
	  but introduced a bug which leads to the problem that days with 1
	  digit will incorrectly being parsed as day 0 - which is invalid.
	  workaround with the implementation from Qt-3.3.6

2007-09-12 11:25 +0000 [r711591]  tpatzig

	* branches/KDE/3.5/kdesdk/scripts/svn2dist: -changed l10n to
	  l10n-kde3

2007-09-18 18:52 +0000 [r714100]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/ChangeLog: add forgotten fix

2007-09-23 19:18 +0000 [r716029]  woebbe

	* branches/KDE/3.5/kdesdk/cervisia/ChangeLog: - mention Martin's
	  fix for BR 148162 - change my mail address - convert my name to
	  utf-8

2007-09-23 19:30 +0000 [r716035-716034]  woebbe

	* branches/KDE/3.5/kdesdk/cervisia/entry_status.h,
	  branches/KDE/3.5/kdesdk/cervisia/annotateview.h,
	  branches/KDE/3.5/kdesdk/cervisia/stringmatcher.cpp,
	  branches/KDE/3.5/kdesdk/cervisia/updateview.cpp,
	  branches/KDE/3.5/kdesdk/cervisia/entry_status_change.h,
	  branches/KDE/3.5/kdesdk/cervisia/updateview_items.cpp,
	  branches/KDE/3.5/kdesdk/cervisia/stringmatcher.h,
	  branches/KDE/3.5/kdesdk/cervisia/updateview_visitors.cpp,
	  branches/KDE/3.5/kdesdk/cervisia/updateview.h,
	  branches/KDE/3.5/kdesdk/cervisia/updateview_items.h,
	  branches/KDE/3.5/kdesdk/cervisia/updateview_visitors.h,
	  branches/KDE/3.5/kdesdk/cervisia/main.cpp,
	  branches/KDE/3.5/kdesdk/cervisia/tooltip.cpp,
	  branches/KDE/3.5/kdesdk/cervisia/loginfo.cpp,
	  branches/KDE/3.5/kdesdk/cervisia/tooltip.h,
	  branches/KDE/3.5/kdesdk/cervisia/entry.cpp,
	  branches/KDE/3.5/kdesdk/cervisia/loginfo.h,
	  branches/KDE/3.5/kdesdk/cervisia/annotateview.cpp,
	  branches/KDE/3.5/kdesdk/cervisia/entry_status.cpp,
	  branches/KDE/3.5/kdesdk/cervisia/entry.h: - change my mail
	  address - convert my name to utf-8 - update copyright

	* branches/KDE/3.5/kdesdk/cervisia/Makefile.am,
	  branches/KDE/3.5/kdesdk/cervisia/version.h: bump version number,
	  just in case we'll have KDE 3.5.8

2007-09-30 20:25 +0000 [r719287]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/entitywidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entitywidget.h: Apply
	  r719259 from trunk. Thanks to Sharan for spotting this.

2007-10-06 21:40 +0000 [r722199]  okellogg

	* branches/KDE/3.5/kdesdk/kbabel/kbabel/kbabelview.cpp: compile
	  with KDE 3.4

2007-10-07 14:08 +0000 [r722514]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppheadercodedocument.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/codegenfactory.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubywriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/tclwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/classifierinfo.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pascalwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/classifierinfo.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppheadercodedocument.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javaclassifiercodedocument.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubyclassifiercodedocument.h:
	  merge r721197:722186 from trunk

2007-10-08 11:05 +0000 [r722971]  coolo

	* branches/KDE/3.5/kdesdk/kdesdk.lsm: updating lsm

