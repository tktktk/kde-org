2008-01-06 15:40 +0000 [r757945]  weilbach

	* branches/KDE/4.0/kdepimlibs/kblog/gdata.cpp: Readd fixes from
	  trunk for check if the job as deleted itself before starting the
	  slots. Fixed some debugging messages,too.

2008-01-06 19:05 +0000 [r758021]  djarvie

	* branches/KDE/4.0/kdepimlibs/kcal/resourcecached.h: apidox

2008-01-06 23:08 +0000 [r758125]  weilbach

	* branches/KDE/4.0/kdepimlibs/kblog/gdata.cpp: Add links to the
	  post object from the syndication feed object. This does not add
	  permlinks for GData.

2008-01-19 19:35 +0000 [r763575]  weilbach

	* branches/KDE/4.0/kdepimlibs/kblog/metaweblog.cpp,
	  branches/KDE/4.0/kdepimlibs/kblog/blogger1.cpp: Krazy fixes:
	  QString::null becomes QString()

2008-01-21 12:48 +0000 [r764318]  weilbach

	* branches/KDE/4.0/kdepimlibs/kblog/gdata.cpp: Fix kDebug calls in
	  GData.

2008-01-22 17:11 +0000 [r764826]  winterz

	* branches/KDE/4.0/kdepimlibs/kcal/kresult.cpp,
	  branches/KDE/4.0/kdepimlibs/kcal/libical/src/libical/icalderivedproperty.c.in,
	  branches/KDE/4.0/kdepimlibs/kcal/tests/testkresult.cpp (added),
	  branches/KDE/4.0/kdepimlibs/syndication/dataretriever.cpp,
	  branches/KDE/4.0/kdepimlibs/kcal/scheduler.cpp,
	  branches/KDE/4.0/kdepimlibs/kcal/tests/CMakeLists.txt,
	  branches/KDE/4.0/kdepimlibs/kcal/vcalformat.cpp,
	  branches/KDE/4.0/kdepimlibs/kcal/tests/testkresult.h (added),
	  branches/KDE/4.0/kdepimlibs/kldap/ldapmodel_p.cpp,
	  branches/KDE/4.0/kdepimlibs/kabc/scripts/field.src.cpp,
	  branches/KDE/4.0/kdepimlibs/kcal/libical/src/libical/sspm.c,
	  branches/KDE/4.0/kdepimlibs/kioslave/imap4/imap4.cpp,
	  branches/KDE/4.0/kdepimlibs/kcal/CHECKLIST,
	  branches/KDE/4.0/kdepimlibs/kcal/libical/src/libical/icalrestriction.c.in:
	  backport all the fixes from the Christoph Bartoschek report that
	  tokoe and I committed yesterday. CCMAIL: tokoe@kde.org

2008-01-26 13:49 +0000 [r766714]  kkofler

	* branches/KDE/4.0/kdepimlibs/mailtransport/CMakeLists.txt: Disable
	  mailtransports kconf_update script for 4.0.x, as there's no
	  kdepim in 4.0 and this script breaks kdepim (at least KMail) from
	  KDE 3. See also
	  http://cvs.fedoraproject.org/viewcvs/rpms/kdepimlibs/devel/kdepimlibs.spec?r1=1.7&r2=1.8
	  CCMAIL: kde-pim@kde.org

2008-01-26 15:28 +0000 [r766765]  kkofler

	* branches/KDE/4.0/kdepimlibs/mailtransport/CMakeLists.txt:
	  Reenable the kconf_update scripts at the request of Tom Albers,
	  until a better solution is found. Leave the warning in because
	  it's still true. CCMAIL: kde-pim@kde.org CCMAIL: tomalbers@kde.nl
	  CCMAIL: kevin.krammer@gmx.at

2008-01-26 15:51 +0000 [r766770]  kloecker

	* branches/KDE/4.0/kdepimlibs/mailtransport/kconf_update/migrate-transports.pl:
	  Merged revision 766768 from trunk Do not delete the mail
	  transport settings from kmailrc or knoderc, so that the KDE 3
	  versions of those apps still find the settings.

2008-01-26 15:57 +0000 [r766773]  toma

	* branches/KDE/4.0/kdepimlibs/mailtransport/CMakeLists.txt: Not
	  true, after Ingo's commit.

