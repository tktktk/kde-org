Dir: kdebase
----------------------------
new version numbers
----------------------------
new version number



Dir: kdebase/kate/app
----------------------------
increased version
----------------------------
Backport for Cullmann
CCBUG: 85215
----------------------------
compile
----------------------------
Revert. sorry.



Dir: kdebase/kcontrol/access
----------------------------
Backport r1.37.



Dir: kdebase/kcontrol/background
----------------------------
Backport it++ -> ++it



Dir: kdebase/kcontrol/clock
----------------------------
it++ -> ++it



Dir: kdebase/kcontrol/colors
----------------------------
Backport it++ -> ++it
(it's not possible to cvsbackport all files :( it's necessary to change file after file in kde_3_3_branch)



Dir: kdebase/kcontrol/crypto
----------------------------
For Aegypten 2, we need the new functionality in the KDE 3.3 version of this module on KDE 3.2, so restore source compat w/ 3_2_branch again.



Dir: kdebase/kcontrol/ebrowsing/plugins/localdomain
----------------------------
Backport fix for path does not have to start with a word (RFC 2396)



Dir: kdebase/kcontrol/ebrowsing/plugins/shorturi
----------------------------
it++ -> ++it
----------------------------
- Backport the email address filtering fix.

BUG: 91049



Dir: kdebase/kcontrol/icons
----------------------------
Backport it++ -> ++it



Dir: kdebase/kcontrol/info
----------------------------
backport fix by Christopher Layne for swap memory statistics reporting



Dir: kdebase/kcontrol/input
----------------------------
Backport part of #42609 and the following fix for it.



Dir: kdebase/kcontrol/ioslaveinfo
----------------------------
it++ -> ++it



Dir: kdebase/kcontrol/kcontrol
----------------------------
it++ -> ++it



Dir: kdebase/kcontrol/kded
----------------------------
it++ -> ++it



Dir: kdebase/kcontrol/keys
----------------------------
it++ -> ++it



Dir: kdebase/kcontrol/kfontinst/lib/Attic
----------------------------
it++ -> ++it



Dir: kdebase/kcontrol/kicker
----------------------------
it++ -> ++it
----------------------------
Fixes Bug 42278 - Strange layout with Kicker and custom size (normal).
Now buttons are never wider (for horizontal) or taller (for vertical) than 58
pixels.
Added some symbolic constants for some kicker values (max size, min size,
default custom size), that were previously hard-coded3.
A few debug lines have been left in for now.



Dir: kdebase/kcontrol/kio
----------------------------
Backport: properly restore settings.



Dir: kdebase/kcontrol/knotify
----------------------------
i18n fix



Dir: kdebase/kcontrol/krdb
----------------------------
it++ -> ++it



Dir: kdebase/kcontrol/kthememanager
----------------------------
Backport typo fix



Dir: kdebase/kcontrol/privacy
----------------------------
Backport this changes into kde3.3 branch



Dir: kdebase/kcontrol/randr
----------------------------
it+ -> ++it



Dir: kdebase/kcontrol/style
----------------------------
it++ -> ++it



Dir: kdebase/kcontrol/usbview
----------------------------
- backport: revision numbers are in hex notation - print them as such!
- update usb.ids file to latest available version



Dir: kdebase/kdepasswd
----------------------------
Backport qstring cache



Dir: kdebase/kdesktop
----------------------------
KIOSK: Ignore IconPosition file when editable_desktop_icons=false
Patch by Aur�lien G�teau
----------------------------
Backport r1.184.



Dir: kdebase/kdesu/kdesu
----------------------------
Backport r1.41.



Dir: kdebase/kdm/backend
----------------------------
fix i18n of "you're root, go away". ;)
----------------------------
make failure to create ipv6 socket non-fatal



Dir: kdebase/kdm/kfrontend
----------------------------
Xsession: use 'export' instead of 'set | grep -v'



Dir: kdebase/khotkeys/shared
----------------------------
Backport workaround for #84434.



Dir: kdebase/kicker
----------------------------
Fixes Bug 42278 - Strange layout with Kicker and custom size (normal).
Now buttons are never wider (for horizontal) or taller (for vertical) than 58
pixels.
Added some symbolic constants for some kicker values (max size, min size,
default custom size), that were previously hard-coded3.
A few debug lines have been left in for now.



Dir: kdebase/kicker/buttons
----------------------------
branches:  1.8.6;
Fixes Bug 42278 - Strange layout with Kicker and custom size (normal).
Now buttons are never wider (for horizontal) or taller (for vertical) than 58
pixels.
Added some symbolic constants for some kicker values (max size, min size,
default custom size), that were previously hard-coded3.
A few debug lines have been left in for now.
----------------------------
branches:  1.11.4;
Fixes Bug 42278 - Strange layout with Kicker and custom size (normal).
Now buttons are never wider (for horizontal) or taller (for vertical) than 58
pixels.
Added some symbolic constants for some kicker values (max size, min size,
default custom size), that were previously hard-coded3.
A few debug lines have been left in for now.



Dir: kdebase/kicker/core
----------------------------
Backport #90733.
----------------------------
Fixes Bug 42278 - Strange layout with Kicker and custom size (normal).
Now buttons are never wider (for horizontal) or taller (for vertical) than 58
pixels.
Added some symbolic constants for some kicker values (max size, min size,
default custom size), that were previously hard-coded3.
A few debug lines have been left in for now.



Dir: kdebase/kicker/proxy
----------------------------
delete the applet when the proxy goes away so that applet dtors get run!
tested by alexeijh; thanks! =) hopefully this will fix some anomolies seen
in earlier versions of kicker when things Went Bad(tm) and then came back
unGood(r), esp w/regard to the system tray



Dir: kdebase/kicker/taskbar
----------------------------
Backport r1.64.



Dir: kdebase/kicker/ui/Attic
----------------------------
Bug 92653: Custom Size configuration dialog has inconsistent behaviour with Configure Panel dialog
----------------------------
Fixes Bug 42278 - Strange layout with Kicker and custom size (normal).
Now buttons are never wider (for horizontal) or taller (for vertical) than 58
pixels.
Added some symbolic constants for some kicker values (max size, min size,
default custom size), that were previously hard-coded3.
A few debug lines have been left in for now.



Dir: kdebase/kioslave/fish
----------------------------
Backport of r1.61
----------------------------
Backport of r1.17
----------------------------
Only use capabilities for "append", the other ones where not initialized at
the location where they were checked.



Dir: kdebase/kioslave/smb
----------------------------
don't keep the password as part of the URL.



Dir: kdebase/kioslave/smtp
----------------------------
Fix #89118 (Kmail does not respond correctly to "550" from smtp server)
CCBUG: 89118
----------------------------
New debugging aid: an interactive SMTP server (well, just a graphical line-based protocol server, really). Might be made more convenient over time...



Dir: kdebase/klipper
----------------------------
Backport #68173 - workaround for acroread.
----------------------------
Backport #80302 + #85198.



Dir: kdebase/konqueror
----------------------------
Backport fix for empty profile (and dnd)
----------------------------
new version number
----------------------------
Backport r1.1364 - better detection of used memory.
----------------------------
Use translated name in save profile dialog.



Dir: kdebase/konqueror/keditbookmarks
----------------------------
fix i18n
BUG: 92013
----------------------------
backport:
fix #84331 (don't crash when deleting while checking)



Dir: kdebase/konqueror/sidebar/trees
----------------------------
Don't crash
BUG:92892
----------------------------
Fix detailed tooltips
BUG:92728



Dir: kdebase/konqueror/sidebar/trees/bookmark_module
----------------------------
Insert separators but hide them
BUG:79941



Dir: kdebase/konsole/konsole
----------------------------
Removes checking of sound system for Bell->System-Notification (BR87664)
----------------------------
increase version for KDE 3.3.2



Dir: kdebase/kstart
----------------------------
Backport r1.62.



Dir: kdebase/kwin
----------------------------
Backport shading fixes.
----------------------------
Backport #92583.
----------------------------
Backport #89813.
BUG: 89813
----------------------------
Backport #89849.
----------------------------
Backport #93832.
----------------------------
Backport r2.43 (#91766).
----------------------------
Backport #90733.
----------------------------
Backport r1.43 (#91194).



Dir: kdebase/kwin/clients/b2
----------------------------
Backport shading fixes.



Dir: kdebase/kwin/clients/default
----------------------------
Backport shading fixes.



Dir: kdebase/kwin/clients/modernsystem
----------------------------
Backport shading fixes.



Dir: kdebase/kwin/clients/quartz
----------------------------
Backport shading fixes.



Dir: kdebase/kwin/kcmkwin/kwinrules
----------------------------
Backport, make sure values are prefilled.
----------------------------
Oops, forgot this change.



Dir: kdebase/kwin/lib
----------------------------
Backport shading fixes.



Dir: kdebase/kxkb
----------------------------
- fix finding X11 dir (Bug #80970)
----------------------------
- recompile layouts on errors (Bug #88882)
- lazy layouts compilation



Dir: kdebase/l10n/cz
----------------------------
- added AddressFormats
- fixed date formats



Dir: kdebase/nsplugins
----------------------------
Fixed increasing memory usage when reloading a fullpage nsplugin.
BUG: 81833
----------------------------
fixing plugins that got a colon in their name
