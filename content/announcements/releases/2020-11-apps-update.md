---
layout: page
publishDate: '2020-11-05T12:00:00+00:00'
title: KDE's November 2020 Apps Update
type: announcement
---

Although you may only notice the big releases, KDE contributors are at work all the time. KDE app developers are no exception and every month we compile the progress they make into release announcements like the one you are reading right now.

To avoid making these announcements too long, we pick a handful of changes to talk about, but there are many more and all are important. Every change, big and small, fixes bugs, improves usability, adds features and just makes using KDE apps better in general for you.

Read on to discover what is coming you way this November:

# New releases

## Krita 4.4

{{< img class="text-center img-fluid" src="mypaint_selector_diagonal.png" caption="MyPaint color selector in Krita" style="width: 600px">}}

KDE's painting app [Krita got a major new release versioned 4.4](https://krita.org/en/item/krita-4-4-0-released/).

Some of the nifty features that come with this version include: brushes which can use a combination of the new lightness parameter with the mix parameter and  use the texture strength parameter to mix gradient mapped brush tips and textures; the SeExpr language that lets you code your own fill layers; and new fill layer methods, such as a multigrid fill layer that generates (among other things) Penrose tilings, Quasicrystal structures, transformations for the pattern fill and a new fill layer option for filling the whole screen with dots, squares, lines, waves and more.

Krita is available in your Linux distro repositories, as a Snap or Flatpak package, as an AppImage download, and also in the Windows and Steam stores.

## Partition Manager

{{< img class="text-center img-fluid" src="partition-manager.png" caption="Partition Manager" style="width: 600px">}}

KDE's disk partitioning tool [Partition Manager released version 4.2](https://mail.kde.org/pipermail/kde-announce-apps/2020-October/005615.html). This version provides improved support for partitions with unknown file systems and mount points, and /etc/fstab handling. 

Partition Manager is available for your Linux distro. It is also the basis for the partition manager in the popular Calamares installer used by many Linux distros.

## RKWard 0.7.2

{{< img class="text-center img-fluid" src="rkward.png" caption="RKWard" style="width: 600px">}}

RKWard is an easy-to-use and extensible IDE/GUI for R, the programming language and free software environment for statistical computing and graphics. [This month's 0.7.2 release](https://rkward.kde.org/News.html) adds a bunch of new features to our IDE:

* Script preview keeps vertical scroll position when updating
* Add function rk.home() for retrieving application paths, similar to R.home()
* Add menu option to switch application language
* Remove direct dependency on libintl
* Add "Check installation" wizard to test for several common basic installation issues all in one place
* Add rkward internal package location to end of library search path, to make it accessible to help.search()
* Add menu action to open any supported file type directly
* < text > elements in plugins may now also contain clickable links, including rkward://-scheme links
* The new code hinting features from version 0.7.1 are now also available in the console
* On unix-systems, RKWard can now be run without installation
* Kate addons are now supported within RKWard. Initially, search-in-files, snippets, and projects are loaded by default
* Python development scripts have been ported to python3.

You can install RKWard on [Mac](https://rkward.kde.org/RKWard_on_Mac.html), [Windows](https://rkward.kde.org/RKWard_on_Windows.html) and for your Linux distro.

# Bugfixes

## Konversation 1.7.7

[Konversation](https://mail.kde.org/pipermail/kde-announce-apps/2020-October/005618.html), KDE's app for chatting on IRC, had a bugfix release. A new feature also adds support for SVG nick icons and, for the goths out there, adds a dark variant of the default nick icon theme.

## KRename 5.0.1

KRename is a tool that lets you rename multiples files at the same time. KRename is releasing a [bugfix version this month](https://mail.kde.org/pipermail/kde-announce-apps/2020-October/005613.html) and developers fixed escapes in filenames with special characters, squashed a bug that closed the app if the progress dialog was closed and made icons look sharp on HiDPI monitors.

# Incoming

Some new apps arrived in playground where we work on them before they are ready for release:

* **Arkade** is a collection of Arcade games developed in Kirigami that will work both on your desktop and on mobile phones.
* **KGeoTag** is a photo geotagging program.
* **Neochat** is a glossy cross-platform instant messaging application for the Matrix chat protocol. It is a fork of Spectral implemented with Kirigami and currently developers are working on ports for Windows, Android and Linux.

# Announcing apps.kde.org

{{< img class="text-center img-fluid" src="apps.kde.org.png" caption="KDE Apps website" style="width: 600px">}}

[apps.kde.org](https://apps.kde.org/) is the new home for listing and browsing KDE's apps. The code behind the site has been updated and now makes it easier to browse in other languages.

# Releases 20.08.3

Some of our projects release on their own timescale and some get released en-masse. The 20.08.3 bundle of projects was released today with dozens of bugfixes and will be available through app stores and distros soon. See the [20.08.3 releases page](https://www.kde.org/info/releases-20.08.3) for details.

Some of the fixes in today's releases:

 * Gwenview no longer accidentally shows the thumbnail view as a separate window with newer Qt versions
 * Sending SMS with KDEConnect has been restored
 * Fixed a possible Okular crash when selecting text in annotations

[20.08 release notes](https://community.kde.org/Releases/20.08_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull; [20.08.3 source info page](https://kde.org/info/releases-20.08.3) &bull; [20.08.3 full changelog](https://kde.org/announcements/fulllog_releases-20.08.3/)

# Contributing

If you would like to help, [get involved](https://community.kde.org/Get_Involved)! We are a friendly and accepting community and we need developers, writers, translators, artists, documenters, testers and evangelists. You can also visit our [welcome chat channel](https://go.kde.org/matrix/#/#kde-welcome:kde.org) and talk live to active KDE contributors.

Another way to help KDE is by [donating to KDE](https://kde.org/community/donations/) and helping the Community keep things running.
