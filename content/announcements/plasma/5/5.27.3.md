---
date: 2023-03-14
changelog: 5.27.2-5.27.3
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Dr Konqi: Add emoji picker to mappings. [Commit.](http://commits.kde.org/drkonqi/105fac2d53ca03dae451922acce42e1f3cb04623) 
+ Klipper: remove duplicate items when loading from history. [Commit.](http://commits.kde.org/plasma-workspace/b34c60956fe858f123dcdde7ee6322b986a795f6) Fixes bug [#466236](https://bugs.kde.org/466236). See bug [#465225](https://bugs.kde.org/465225)
+ Powerdevil: Suspend by default on AC profile. [Commit.](http://commits.kde.org/powerdevil/734c0753e502245372db8b1eec4645acc0138764) 
