---
title: Support Good People
layout: yearend2024_hw
scssFiles:
- /scss/yearend2024-hw.scss
jsFiles:
- /js/yearend2024.js
draft: false
---

<a name="top"></a>We're kicking off our 2024 end-of-year fundraiser just in time for Halloween!

Even if the spine-tingling horrors of the long dark night of Walpurgis are mostly imaginary, the sinister threats of predatory proprietary software providers remain all too real.

Fear not! We, the KDE community, will help you, your friends, family, company, and community banish all the creepy and insidious proprietary software that haunts your computers, phones, and household appliances.

But we can't do it alone! We need you to help us fight the good fight against the tech-ghouls from beyond. Use the form to donate any amount to our fundraiser (or become a regular donor to our community) and help us keep the dark forces of proprietary software at bay.

### <span class="white">Donated (update daily)</span>
<div>
{{< yearend2024/progress >}}
</div>

<p><sup>*</sup>More stretch goals coming soon.</p>

## Proprietary <span class="red">Nightmares</span>

### <span class="red">INVASION</span> <span class="white">OF THE PROJECT</span> <span class="red">SNATCHERS</span>
After months of intrusive pop-ups badgering me to update my software, this morning I did it. Now I can't open my older projects.

<button type="button" class="btn btn-danger btn-lg" onclick="show_conclusion('invasion')"><span class="horror">Click me</span></button> *to read the spine-chilling conclusion*</p>

<div class="d-none" id="invasion">

This never happened with my [Kdenlive](https://kdenlive.org) projects. KDE's video editor preserved backwards-compatibility and you can even parse the source of each project with your naked eyeballs. It's only XML, after all. So even decades from now, anyone could write a new app capable of opening the files. Digital archiving is a reality with Free Software!

I guess I'll just have to remake all those older projects with Kdenlive. At least then I know I won't be screwed again.

Reminds me of the time my hard disk conked out and I had no backup. [Turns out KDE has a solution for that too](https://apps.kde.org/kup/).

Live and learn.

[KDE's software is built mostly by unpaid volunteers. Help support their contributions with a donation.](#top)
</div>

![](/fundraisers/yearend2024/skull_philigrgran.png)

### <span class="white">THE</span> <span class="red">SILENCE</span> <span class="white">OF THE</span> <span class="red">LANS</span>
I decided to stop paying the upgrades for the software since it already worked fine for me, had all the features I needed, and the support license was getting ridiculously expensive. When I logged in this morning, the software had been remotely disabled and I couldn't access any of my work.

<button type="button" class="btn btn-danger btn-lg" onclick="show_conclusion('silence')"><span class="horror">Click me</span></button> *to read the spine-chilling conclusion*</p>

<div class="d-none" id="silence">

I had no idea they could do that! I'd assumed that, having paid the company, the software was mine. Turns out I was just renting it, and you can only be an owner of Free Software. Everything else is subject to the whims of the provider, regardless of the license you originally accepted.

My mate is helping me install a Linux system with something called ["KDE"](https://kde.org). It's true-blue Free Open Source Software. So far it has all the features I need, and, whatever isn't pre-installed, I can grab from something called "software repos" with the easy-to-use "Discover" software center app. Looking good, no drama.

[KDE relies on grassroots donations to maintain its independence. You too can become a donor.](#top)
</div>

![](/fundraisers/yearend2024/skull_philigrgran.png)

### <span class="white">THE BYTE</span> <span class="red">WITCH</span> <span class="white">PROJECT</span>
A client ghosted me. When I phoned them to find out what was up, they confessed they'd found an AI service that provided them with art in my exact same style at a fraction of the price.

A bit of digging revealed that Adobe has been training their AI with all the work I've created using their software.

<button type="button" class="btn btn-danger btn-lg" onclick="show_conclusion('witch')"><span class="horror">Click me</span></button> *to read the spine-chilling conclusion*</p>

<div class="d-none" id="witch">

Screw Photoshop and its scummy cloud-AI scam. Now I've been drawing and painting with [Krita](https://krita.org) for the last few months, and it's an artist's dream app. It's not only free, but it has literally hundreds of brushes, and it works on Linux, Windows, macOS, and Android. Best of all, I know it's not spying on me and I get to choose what happens with the art I create using it.

[KDE is not a company and has no interest in monetizing your work. We will not only never share your data, we will also work to stop it from being stolen by others.](#top)
</div>

![](/fundraisers/yearend2024/skull_philigrgran.png)

### <span class="white">THE RICHMOND DESKTOP</span> <span class="red">MASSACRE</span>
I'd been putting off Windows updates for a few weeks while I was working on a big project, but this morning it decided to force me to… For 40 minutes… During a presentation… In front of a live audience of 500 people… Who'd paid $100 a pop to be there…

<button type="button" class="btn btn-danger btn-lg" onclick="show_conclusion('massacre')"><span class="horror">Click me</span></button> *to read the spine-chilling conclusion*</p>

<div class="d-none" id="massacre">

I learned my lesson. I can make presentations with half a dozen free software applications (including something called [Calligra Stage](https://calligra.org/components/stage/)), and show them on KDE's Plasma desktop. Plasma is smart and modern, and automatically enters "do not disturb" mode when I connect to an overhead projector.

And when there are updates, _I_ can decide when they get installed and applied, if at all. I'm in control. My computer feels like mine again.

[KDE's mission is to provide you with the tools that allow you to control your digital life. Help us keep doing that by donating to our fundraiser.](#top)
</div>

![](/fundraisers/yearend2024/skull_philigrgran.png)

### <span class="white">TOTAL (WINDOWS)</span> <span class="red">RECALL</span>
I'm in the middle of an acrimonious separation dispute with my ex, Phil. This morning, his lawyers subpoenaed the data on my computer. I thought "Sure, I have nothing to hide". Then they mentioned _Windows Recall_. Turns out this thing records a screenshot of my desktop every 3 minutes. It's been doing this for over a year and I had no idea. 

I am pretty sure I never sent anything damning from that machine, but I know that any angry message about Phil I thought I had sent in confidence to my friends and family will be used against me in court. It could represent the difference between keeping a roof over my head and the custody of my child.

I am so scared.


{{< video src-webm="https://cdn.kde.org/promo/Fundraisers/EndofYear/2024/WindowsRecall.webm" src="https://cdn.kde.org/promo/Fundraisers/EndofYear/2024/WindowsRecall.mp4" poster="https://cdn.kde.org/promo/Fundraisers/EndofYear/2024/WindowsRecall.png" fig_class="mx-auto max-width-800" >}}

<button type="button" class="btn btn-danger btn-lg" onclick="show_conclusion('recall')"><span class="horror">Click me</span></button> *to read the spine-chilling conclusion*</p>

<div class="d-none" id="recall">

Things turned out okay in the end. The judge was understanding and threw out most of the "evidence" Phil's lawyers tried to use against me.

But never again. I erased Windows for good and learned how to install Linux (much easier than I thought) using a distro with [KDE's Plasma desktop](https://kde.org/plasma-desktop/). It does what I want it to do and _only_ what I want it to do. No mysterious connections to the Internet uploading god-knows-what to god-knows-where. No shady processes recording my every keystroke and activity.

Hah! Subpoena _this_, Phil, you abusive bastard!

[Help KDE continue to produce data protecting software with a donation.](#top)
</div>

![](/fundraisers/yearend2024/skull_philigrgran.png)

### <span class="red">PEEPING</span> <span class="white">ROM</span>
As soon as we started talking about having another baby, our smart TV began bombarding us with ads for diapers, formula, and other baby-related stuff. The damn thing listens to our every word, and the manufacturer sells everything we say to advertisers. Who else are they selling it to? Insurance companies? Our first child was born with a minor heart condition. She's thankfully okay, but we've often discussed her health in what we thought was the privacy of our own home. Will our health insurance company kick her off the policy or raise our rates if this information is on a database somewhere?

<button type="button" class="btn btn-danger btn-lg" onclick="show_conclusion('peeping')"><span class="horror">Click me</span></button> *to read the spine-chilling conclusion*</p>

<div class="d-none" id="peeping">

Fuck that. I disabled all the so-called smart recognition features, grabbed a Raspberry Pi and installed [Plasma Bigscreen](https://plasma-bigscreen.org/), plugged it into the back of the TV, and hey presto!: Free Software's answer to those spying "smart" TVs.  We can still enjoy all our favorite content, and it even has a voice recognition feature, but it does all the recognizing locally, without even connecting to the Internet.

[KDE creates software for all kinds of devices, so you can enjoy free and safe software everywhere. Please donate so we can keep all our projects running and up to date.](#top)
</div>

![](/fundraisers/yearend2024/skull_philigrgran.png)

### <span class="white">STRANGER</span> <span class="red">PINGS</span>
As I approached the ethereal figure crouching huddled in the corner, cold sweat pearled on my brow and my heart started to race.

I reached out and touched her icy cold shoulder.

"Are you okay?" I managed, my voice squeaky with trepidation.

She turned to me, her pallid face floating in the shadows, eyes dark pools of dread.

"It's our school..." she muttered haltingly "They're still running Windows XP…"

<button type="button" class="btn btn-danger btn-lg" onclick="show_conclusion('stranger')"><span class="horror">Click me</span></button> *to read the spine-chilling conclusion*</p>

<div class="d-none" id="stranger">

"Bu— but" I stammered "why?"

"Our computers are too old to update!!!" wailed the tortured soul.

"Oh!"

"And we have no money to buy new machines!"

"Just install Linux with Plasma, then."

"What?"

"Linux and Plasma from KDE. Download an ISO off the Internet… Say… I don't know… [Neon, Fedora, openSUSE, whatever](https://kde.org/distributions/). Install it and you'll have a modern operating system with tons of software, including [educational apps](https://apps.kde.org/categories/education/) with loads of [activities](https://gcompris.net). It will happily run on computers that are even [decades old](https://eco.kde.org/be-green/)."

"Really?"

"Yeah, really. Now get away from me, will you? You're leaking ectoplasm everywhere!"

[KDE helps underfunded schools all over the world. Help us and give children the educational technology they deserve with their privacy secure, via a donation to our fundraiser.](#top)
</div>

![](/fundraisers/yearend2024/skull_philigrgran.png)

### <span class="red">TELL</span> <span class="white">US YOUR</span> <span class="red">STORY</span>

Want to share your own proprietary horror story? Ping us on [Mastodon](https://floss.social/@kde), [Lemmy](https://lemmy.kde.social/), [Reddit](https://www.reddit.com/r/kde/), or [Discuss](https://discuss.kde.org) and tell us what happened to you! 💀
